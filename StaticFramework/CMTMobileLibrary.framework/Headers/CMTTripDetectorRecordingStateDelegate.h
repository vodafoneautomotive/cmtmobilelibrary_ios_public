//
// CMTTripDetectorRecordingStateDelegate.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@class CMTCurrentlyRecordingDriveInfo;
@class CMTTripDetector;

/*!
 *  Allows protocol implementing object to receive updates about drives starting or stopping or uploading
 *  @since Available since 1.0.0
 */
@protocol CMTTripDetectorRecordingStateDelegate <NSObject>

@optional

/**
 Called when a drive has started (with the drive's start time)

 @param tripDetector Reference to the CMTTripDetector object that initiated the trip
 @param driveInfo    Information about the just started trip
 @param startDate    Time at which the trip started
 
 @since Available since 1.0.0
 */
-(void) tripDetector:(CMTTripDetector *)tripDetector tripWithCurrentDriveInfo:(CMTCurrentlyRecordingDriveInfo *)driveInfo didStartAtTime:(NSDate *)startDate;

/**
 Called when a drive is prepared for upload

 @param tripDetector Reference to the CMTTripDetector object that initiated the trip
 @param driveInfo    Information about the trip about to be uploaded
 @param uploadDate   Time at which trip was enqueued for upload
 
 @since Available since 1.0.0
 */
-(void) tripDetector:(CMTTripDetector *)tripDetector tripWithCurrentDriveInfo:(CMTCurrentlyRecordingDriveInfo *)driveInfo didEnqueueForUploadAtTime:(NSDate *)uploadDate;

/**
 Called when a drive has stopped

 @param tripDetector Reference to the CMTTripDetector object that initiated the trip
 @param driveInfo    Information about the trip that just stopped
 @param stopDate     Time at which trip was stopped
 
 @since Available since 1.4.1
 
 @discussion This method will become required when the tripDetector:tripWithCurrentDriveInfo:didStopAtTime:waitingToFinish method is removed
 */
-(void) tripDetector:(CMTTripDetector *)tripDetector tripWithCurrentDriveInfo:(CMTCurrentlyRecordingDriveInfo *)driveInfo didStopAtTime:(NSDate *)stopDate;


@end
NS_ASSUME_NONNULL_END
