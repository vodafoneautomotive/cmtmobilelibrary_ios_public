//
// NSObject+CMTMTLComparisonAdditions.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//


#import <Foundation/Foundation.h>

/**
 Returns whether both objects are identical or equal via -isEqual:
 @param obj1 First object
 @param obj2 Second object
*/
BOOL CMTMTLEqualObjects(id obj1, id obj2);
