//
// CMTUserSummaryDataProvider.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTUserBaseDataProvider.h"
#import "CMTUserSummaryModel.h"

/*! Notification indicating that `CMTUserSummary` has changed.
 * @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTUserSummaryDataProviderDidChangeNotification;

/*!
 * Key used in the userInfo dictionary object passed in the NSNotification object.
 * access the userInfo dictionary. The value of the key is the CMTUserSummaryModel.
 * @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTUserSummaryDataProviderUserSummaryModelKey;

/*!
 * Class to get the latest user summary for the user
 * @since Available since 1.0.0
 * @deprecated Deprecated since 9.1.0. To fetch the user summary, use CMTURLSessionManager to fcontact the CMT endpoint: `/mobile/v3/get_user_summary`.
 * @discussion <b>Deprecated since 9.1.0</b>. To fetch the user summary, use CMTURLSessionManager to contact the CMT endpoint: `/mobile/v3/get_user_summary`.
 */
__attribute__((deprecated("Deprecated since 9.1.0. To fetch the user summary, use CMTURLSessionManager to fetch user summary information from the CMT backend.")))
@interface CMTUserSummaryDataProvider : CMTUserBaseDataProvider
/*!
 * Returns a reference to the user summary
 * @since Available since 1.0.0
 * @deprecated Deprecated since 9.1.0. Don't use this property because this class has been deprecated.
 */
@property (readonly,strong,nullable) CMTUserSummaryModel *userSummary
__attribute__((deprecated("Deprecated since 9.1.0. Don't use this property because this class has been deprecated.")));

/**
 App developers should not attempt to create an instance of `CMTUserSummaryDataProvider` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Fetch user summary
 *
 *  @param handler Handler that will be called at completion of fetch. The handler will be passed the
 *  updated user summary, whether the fetch succeeded. If it failed, the error parameter will be set appropriately.
 *  @return NSOperation used to perform the background task
 *  @since Available since 1.0.4
 *  @deprecated Deprecated since 9.1.0. To fetch the user summary, use the CMTURLSessionManager to contact the CMT endpoint: `/mobile/v3/get_user_summary`.
 */
-(nonnull NSOperation *)loadUserSummaryInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTUserSummaryModel * _Nullable value,
                                                                           BOOL success,
                                                                           NSError * _Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.1.0. To fetch the user summary, use CMTURLSessionManager to contact the CMT endpoint: /mobile/v3/get_user_summary.")));

/*!
 *  Fetch user summary
 *
 *  @param handler Handler that will be called at completion of fetch. The handler will be passed the
 *  updated user summary, whether the fetch succeeded. If it failed, the error parameter will be set appropriately.
 *
 *  @since Available since 9.0.0
 *  @deprecated Deprecated since 9.1.0. To fetch the user summary, use the CMTURLSessionManager to contact the CMT endpoint: `/mobile/v3/get_user_summary`.
 */
-(void)loadUserSummaryWithHandler:(void (^_Nullable)(CMTUserSummaryModel * _Nullable value,
                                                     BOOL success,
                                                     NSError * _Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.1.0. To fetch the user summary, use the CMTURLSessionManager to contact the CMT endpoint: /mobile/v3/get_user_summary.")));

@end
