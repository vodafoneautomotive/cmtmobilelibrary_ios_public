//
// CMTTripMetricsModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"


/**
 Data model for simple trip metrics
 @since Available since 1.7.0
 */
@interface CMTTripMetricsModel : CMTMTLModel<CMTMTLJSONSerializing>

/*!
 * The sum of the trip distance for all trips included in the user score, km (float)
 * @since Available since 1.7.0
 */
@property (nonatomic,nullable) NSNumber *scoredKilometers;

/*!
 * Number of trips used in computing user score (integer)
 * @since Available since 1.7.0
 */
@property (nonatomic,nullable) NSNumber *scoredTrips;

@end
