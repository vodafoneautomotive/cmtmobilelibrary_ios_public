//
// NSDictionary+CMTMTLMappingAdditions.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/// CMTMTLMappingAdditions category for NSDictionary
@interface NSDictionary (CMTMTLMappingAdditions)

/**
 Creates an identity mapping for serialization.

 @param modelClass A subclass of CMTMTLModel.
 @return a dictionary that maps all properties of the given class to
 themselves.
 */
+ (NSDictionary *)cmt_mtl_identityPropertyMapWithModel:(Class)modelClass;

@end
