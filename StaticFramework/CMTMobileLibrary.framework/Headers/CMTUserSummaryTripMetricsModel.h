//
// CMTUserSummaryTripMetricsModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTTripMetricsModel.h"

/*!
 *  Metrics about the user
 *  @since Available since 1.0.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.")))
@interface CMTUserSummaryTripMetricsModel : CMTTripMetricsModel <CMTMTLJSONSerializing>

/*!
 *  Total number of driving trips user has ever taken (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *totalTrips;

/*!
 *  Total number of non-driving trips user has ever taken (integer)
 *  @since Available since 1.4.0
 */
@property (nullable) NSNumber *totalNonCarTrips;

/*!
 *  Number of trips >= 4 stars overall
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *goodTrips;

/*!
 *  Number of trips >= 4 stars for acceleration
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *goodAccelTrips;

/*!
 *  Number of trips >= 4 stars for braking
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *goodBrakeTrips;

/*!
 *  Number of trips >= 4 stars for turning
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *goodTurnTrips;

/*!
 *  Number of trips >= 4 stars for speeding
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *goodSpeedingTrips;

/*!
 *  Current consecutive number of trips with no distractions (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *noDistractionCurrentStreak;

/*!
 *  Best consecutive number of trips with no distractions (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *noDistractionBestStreak;

/*!
 *  User subscore for acceleration (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreAccel;

/*!
 *  User subscore for braking (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreBrake;

/*!
 *  User subscore for turning (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreTurn;

/*!
 *  User subscore for speeding (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreSpeeding;

/*!
 *  User subscore for phone motion (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScorePhoneMotion;

/*!
 *  User subscore for distance (float)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreDistance;

/*!
 *  User subscore pertaining to when user drives  (float)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreTimeOfDay;

/*!
 *  User subscore for smoothness of driving  (float)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreSmoothness;

/*!
 *  User subscore for road types (float)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScoreRoads;

/*!
 *  The sum of the trip distance for all trips included in the user score, km (float)
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 1.6.0, use scoredKilometers instead.
 */
@property (nullable) NSNumber *userScoreKilometers __attribute__((deprecated("Deprecated since 1.6.0, use scoredKilometers instead")));

/*!
 *  Number of trips used in computing user score (integer)
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 1.6.0, use scoredTrips instead.
 */
@property (nullable) NSNumber *userScoreTrips __attribute__((deprecated("Deprecated since 1.6.0, use scoredTrips instead")));

@end
