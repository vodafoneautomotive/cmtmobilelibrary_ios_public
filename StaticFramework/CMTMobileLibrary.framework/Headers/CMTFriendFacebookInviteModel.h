//
// CMTFriendFacebookInviteModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 *  Used to send an invite via Facebook
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendFacebookInviteModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! The name of the person on Facebook you want to invite
 *  @since Available since 0.1.0
 *  @deprecated No longer used by Facebook SDK > 4.0.
 */
@property (nonatomic,nullable) NSString *name __attribute__((deprecated("No longer used by Facebook SDK > 4.0")));

/*! Description to send to invitee via Facebook
 *  @since Available since 0.1.0
 *  @deprecated No longer used by Facebook SDK > 4.0.
 */
@property (nonatomic,nullable) NSString *fbDescription __attribute__((deprecated("No longer used by Facebook SDK > 4.0")));

/*! Picture to send to invitee via Facebook
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSURL *fbPicture;

/*! Caption to send to invitee via Facebook
 *  @since Available since 0.1.0
 *  @deprecated No longer used by Facebook SDK > 4.0.
 */
@property (nonatomic,nullable) NSString *fbCaption __attribute__((deprecated("No longer used by Facebook SDK > 4.0")));

/*! URL to include in invitation
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSURL *fbLink;


@end
