//
// CMTLeaderboardDataManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTLeaderboardModel.h"
#import "CMTUserBaseDataProvider.h"

/*!
 * Name of errors that come from the CMTLeaderboardDataManager
 * @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTLeaderboardDataManagerErrorDomain;

/**
 Errors that can arise from using the CMTLeaderboardDataManager
 @discussion Deprecated since 9.0.0
 @since Available since 1.0.0
 */
typedef NS_ENUM(NSInteger, CMTLeaderboardDataManagerError) {
    /// Indicates when the fetched data from the backend is unable to be serialized (Available since 1.0.0)
    CMTLeaderboardDataManagerErrorSerializationFailed = -1
};

/*!
 *  The `CMTLeaderboardDataManager` API will be deprecated in a future release;
 *  use the `CMTCustomLeaderboardDataManager` for all leaderboard-related information.
 *
 *  @since Available since 1.0.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTLeaderboardDataManager : CMTUserBaseDataProvider

/*!
 *  Returns the leaderboard object
 *  @since Available since 1.0.0
 */
@property (readonly,atomic,nullable) CMTLeaderboardModel *leaderboard;

/**
 App developers should not attempt to create an instance of `CMTLeaderboardDataManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Synchronously load leaderboard
 *
 *  @param facebookToken If the facebookToken was just updated, you can pass this to the server, so that the response will include
 *                       the list of facebook friends that have authorized the app.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return The object corresponding to the list of friends
 *
 *  @throws (Swift only) NSError
 *  
 *  @since Available since 1.3.15
 */
-(nullable CMTLeaderboardModel *)loadLeaderboardWithFacebookToken:(NSString *_Nullable)facebookToken error:(NSError * _Nullable __autoreleasing * _Nullable)error;

/*!
 * Forces an asynchronous fetch of the user's list of friends.
 *
 * @param handler If not nil, at the completion of the asynchronous network operation, the handler will be called. 
 *      The handler should return the `CMTLeaderboardModel` object corresponding to the list of friends, whether
 *      the network call succeeded or failed. If the network call failed, an appropriate error will be indicated to the user
 *
 * @return The `NSOperation` assigned to the operation performing the fetch, can be used to cancel
 * @since Available since 1.0.4
 */
-(nonnull NSOperation *)loadLeaderboardInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTLeaderboardModel * _Nullable leaderboard, BOOL success, NSError * _Nullable error))handler;

/*!
 * Forces an asynchronous fetch of the user's list of friends.
 *
 * @param facebookToken If the facebookToken was just updated, you can pass this to the server, so that the response will include
 *                      the list of facebook friends that have authorized the app.
 * @param handler If not nil, at the completion of the asynchronous network operation, the handler will be called.
 *      The handler should return the `CMTLeaderboardModel` object corresponding to the list of friends, whether
 *      the network call succeeded or failed. If the network call failed, an appropriate error will be indicated to the user
 *
 * @return The `NSOperation` assigned to the operation performing the fetch, can be used to cancel
 * @since Available since 1.0.4
 */
-(nonnull NSOperation *)loadLeaderboardInBackgroundWithFacebookToken:(NSString *_Nullable)facebookToken
                                                   completionHandler:(void (^_Nullable)(CMTLeaderboardModel * _Nullable leaderboard, BOOL success, NSError * _Nullable error))handler;
/*!
 *  Removes friends in the friend array
 *
 *  @param friendsArray An array of `CMTFriendModel` objects with the `friendId` property set.
 *  @param handler      If not nil, at the completion of the asynchronous network operation, the handler will be called. The handler should return the `CMTLeaderboardModel` object corresponding to the list of friends, whether the network call succeeded or failed. If the network call failed, an appropriate error will be indicated to the user
 *
 *  @return The `NSOperation` assigned to the operation performing the fetch
 *  @since Available since 1.0.4
 */
-(nonnull NSOperation *)removeFriendsInBackground:(NSArray <CMTFriendModel *> *_Nonnull)friendsArray
                                completionHandler:(void (^_Nullable)(CMTLeaderboardModel* _Nullable leaderboard, BOOL success, NSError * _Nullable error))handler;

@end
