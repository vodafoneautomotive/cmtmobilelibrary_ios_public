//
// CMTSVRAlertModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

@class CLLocation;


/**
 CMTSVRAlertModel contains information about discovered SVR tags that can
 be used by app developers
 
 The CMTSVRDetectorDelegate callbacks will pass information to the delegate
 using objects of this type
 
 @since Available since 1.2.1
 */
@interface CMTSVRAlertModel : CMTMTLModel<CMTMTLJSONSerializing>

/**
 The location of the tag
 
 @since Available since 1.2.1
 */
@property (nonatomic, nullable) NSArray<CLLocation *> *locations;

/**
 The mac address of the SVR tag as a string
 
 @since Available since 1.2.1
 */
@property (nonatomic, nullable) NSString *tagIdentifier;

/**
 The date that the tag was discovered
 
 @since Available since 1.2.1
 */
@property (nonatomic, nullable) NSDate *phoneDate;

/**
 The type of beacon issuing the alert
 
 @since Available since 7.0.0
 */
@property (nonatomic, nullable) NSString *beaconType;

@end
