//
// CMTVersionState.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

extern NSString * const CMTVersionStateDefaultBundleVersion;
extern NSString * const CMTVersionStateDefaultShortVersionString;

/**
 * Data model to describe version of various components of SDK
 * @since Available since 1.4.1
 */
@interface CMTVersionState : NSObject


/** CMT Mobile Library version
 @discussion Combines currentCMTMobileLibraryVersion and currentCMTMobileLibraryCommitHash into a single string
 @since Available since 1.4.1
 */
@property (readonly) NSString *currentCMTLibraryVersion;

/** CMT Mobile Library version
 @since Available since 8.0.0
 */
@property (readonly) NSString *currentCMTMobileLibraryVersion;

/** CMT Mobile Library Commit Hash
 @since Available since 8.0.0
 */
@property (readonly) NSString *currentCMTMobileLibraryCommitHash;

/** CMT Filter Engine version
 * @since Available since 1.4.1
 */
@property (readonly) NSString *currentFilterEngineVersion;

/** If not set, getter will return "CFBundleShortVersion.CFBundleVersion" by default
 * @since Available since 1.4.1
 */
@property NSString *currentAppVersion;

/** Returns YES when SDK is first installed. NO otherwise.
 * @since Available since 1.4.1
 */
@property (readonly) BOOL created;

/** Returns YES when SDK is upgraded. No otherwise.
 * @since Available since 1.4.1
 */
@property (readonly) BOOL upgraded;

@end
NS_ASSUME_NONNULL_END
