//
// CMTFriendInviteModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 *  Model used to send an invitation to a friend. Only one of email, mobile or facebook 
 *  values need to be provided.
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendInviteModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Name to use if sending an email invitation
 *  @since Available since 0.1.0
 */
@property (nullable, nonatomic) NSString *name;

/*! Email address of invitee
 *  @since Available since 0.1.0
 */
@property (nullable, nonatomic) NSString *email;

/*! Mobile number of invitee
 *  @since Available since 0.1.0
 */
@property (nullable, nonatomic) NSString *mobile;

/*! Boolean property indicating invite via Facebook
 *  @since Available since 0.1.0
 */
@property (nullable, nonatomic) NSNumber *viaFacebook;

/*! Username to use if send an in-app friend request
 *  @since Available since 1.4.1
 */
@property (nullable, nonatomic) NSString *username;

@end
