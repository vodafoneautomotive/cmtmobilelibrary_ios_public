//
// CMTVehicleManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTVehicle.h"
#import "CMTVehicleDescription.h"

@class CMTUser;

/**
 Name of errors that come from the `CMTVehicleManager`.
 @since Available since 4.0.0
 */
extern NSString * _Nonnull const CMTVehicleManagerErrorDomain;

/**
 Error types that could be returned while using the CMTVehicleManager

 @since Available since 5.0.3
 */
typedef NS_ENUM(NSInteger, CMTVehicleManagerError) {
    /// No error, default (Available since 5.0.3)
    CMTVehicleManagerErrorNone = 0,
    /// A vehicle with a missing short vehicle identifier was provided (Available since 5.0.3)
    CMTVehicleManagerErrorInvalidVehicleIdentifierProvided = -1,
    ///  An update to the vehicle includes a field which conflicts with a field used by a CMTVehicle property (Available since 5.0.3)
    CMTVehicleManagerErrorCustomDictionarySpecifiesConflictingField = -2,
};

/**
 Use the `CMTVehicleManager` class to manage the vehicles associated with this user.

 @since Available since 4.0.0
 */
@interface CMTVehicleManager : NSObject

/**
 A list of vehicles associated with this user. This value
 is generated from an on-disk cache and may not be updated.
 @since Available since 5.0.0
 */
@property (nullable, readonly) NSArray<CMTVehicle *> *vehicleList;

/**
 Dictionary that maps a tag's MAC address (as a String) to the linked CMTVehicle with that MAC address
 @since Available since 5.0.3
 */
@property (nullable, readonly) NSDictionary<NSString *, CMTVehicle *> *macAddressToLinkedVehicleDictionary;

/**
 App developers should not attempt to create an instance of `CMTVehicleManager`. Use the `[CMTUser currentUser].vehicleManager` to get a vehicle list.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Fetches a list of vehicles associated with this user from the CMT backend.
 If there is an error, nil will be returned, otherwise a list of vehicles will be returned.
 This is a change from previous versions of the SDK.

 @method loadListOfVehiclesWithError:

 @param error (Objective-C only) Set on failure.
 @return Array of `CMTVehicle` objects (could be nil)
 @throws (Swift only) NSError
 @since Available since 4.0.0
 */
-(nullable NSArray <CMTVehicle *> *)loadListOfVehiclesWithError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Fetches a list of vehicles associated with this user with the matching vehicle identification and registration provided from the CMT backend.
 If there is an error, nil will be returned, otherwise a list of vehicles will be returned.
 This is a change from previous versions of the SDK.

 @method loadVehiclesWithVehicleId:andRegistration:error

 @param vehicleDescription an object describing the vehicle.
 @param error (Objective-C only) Set on failure.
 @return Array of `CMTVehicle` objects (could be nil)

 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
-(nullable NSArray <CMTVehicle *> *)loadVehiclesWithDescription:(CMTVehicleDescription * _Nullable)vehicleDescription
                                                          error:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Asynchronously fetches a list of vehicles associated with this user from the CMT backend.

 @method loadListOfVehiclesInBackgroundWithCompletionHandler:
 @param handler If not nil, at the completion of the asynchronous network operation, the handler will be called.
                The handler should return an `NSArray` object corresponding to the user's list of `CMTVehicle` objects
                and indicate whether the network call succeeded or failed. If there is a failure, an appropriate error
                will be indicated to the caller.
 @since Available since 4.0.0
 */
-(void)loadListOfVehiclesInBackgroundWithCompletionHandler:(void (^_Nullable)(BOOL success, NSArray <CMTVehicle *> * _Nullable vehicleList, NSError * _Nullable error))handler;


/**
 Asynchronously fetches a list of vehicles associated with this user with the matching vehicle identification and registration provided from the CMT backend

 @param vehicleDescription an object describing the vehicle.
 @param handler If not nil, at the completion of the asynchronous network operation, the handler will be called.
                The handler should return an `NSArray` object corresponding to the user's list of `CMTVehicle` objects that have the vehicle id
                and registration vales provided. The handler will also indicate whether the network call succeeded or failed. If there is a
                failure, an appropriate error will be indicated to the caller.
 @since Available since 8.0.0
 */
-(void)loadVehiclesInBackgroundWithDescription:(CMTVehicleDescription * _Nullable)vehicleDescription
                             completionHandler:(void (^_Nullable)(BOOL, NSArray<CMTVehicle *> * _Nullable, NSError * _Nullable))handler;

/**
 Synchronously add vehicle with vehicle description

 @param description         A description of the vehicle you want to add
 @param customDictionary    A custom dictionary that can be used to provide additional data for custom fields
 @param error               (Objective-C only) Set on failure.

 @return Array of `CMTVehicle` objects (could be nil).
 @throws (Swift only) NSError if add fails
 @since Available since 5.0.3
 */
-(nullable NSArray <CMTVehicle *> *)addVehicleWithDescription:(CMTVehicleDescription * _Nonnull)description
                                          andCustomDictionary:(NSDictionary * _Nullable)customDictionary
                                                        error:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Asynchronously add vehicle with vehicle description

 @param description         A description of the vehicle you want to add
 @param customDictionary    A custom dictionary that can be used to provide additional data for custom fields
 @param handler             If not nil, at the completion of the asynchronous network operation, the handler will be called.
                            If adding the vehicle is successful, The handler should return an `NSArray` object corresponding
                            to the user's list of `CMTVehicle` objects and whether the network call succeeded or failed.
                            If there is a failure, an appropriate error, will be indicated to the caller.
 @since Available since 5.0.3
 */
-(void)addVehicleInBackgroundWithDescription:(CMTVehicleDescription * _Nonnull)description
                         andCustomDictionary:(NSDictionary * _Nullable)customDictionary
                           completionHandler:(void (^_Nullable)(BOOL success, NSArray <CMTVehicle *> * _Nullable vehicleList, NSError * _Nullable error))handler;

/**
 Synchronously update vehicle with vehicle description

 @param vehicle             Reference to a CMTVehicle object (must have shortVehicleIdentifier defined)
 @param customDictionary    A custom dictionary that can be used to provide additional data for custom fields
 @param description         The updated vehicle description
 @param error               (Objective-C only) Set on failure.

 @return Array of `CMTVehicle` objects (could be nil)
 @throws (Swift only) NSError if update failes
 @since Available since 5.0.3
 */
-(nullable NSArray <CMTVehicle *> *)updateVehicle:(CMTVehicle * _Nonnull)vehicle
                                  withDescription:(CMTVehicleDescription * _Nonnull)description
                              andCustomDictionary:(NSDictionary * _Nullable)customDictionary
                                            error:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Asynchronously update vehicle with vehicle description

 @param vehicle             Reference to a CMTVehicle object (must have shortVehicleIdentifier defined)
 @param description         An updated description for the vehicle
 @param customDictionary    A custom dictionary that can be used to provide additional data for custom fields
 @param handler             If not nil, at the completion of the asynchronous network operation, the handler will be called.
                            If updating the vehicle is successful, The handler should return an `NSArray` object corresponding
                            to the user's list of `CMTVehicle` objects and whether the network call succeeded or failed.
                            If there is a failure, an appropriate error, will be indicated to the caller.
 @since Available since 5.0.3
 */
-(void)updateVehicleInBackground:(CMTVehicle * _Nonnull)vehicle
                 WithDescription:(CMTVehicleDescription * _Nonnull)description
             andCustomDictionary:(NSDictionary * _Nullable)customDictionary
               completionHandler:(void (^_Nullable)(BOOL success, NSArray <CMTVehicle *> * _Nullable vehicleList, NSError * _Nullable error))handler;


@end
