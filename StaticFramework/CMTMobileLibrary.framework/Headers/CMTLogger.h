//
// CMTLogger.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Different log levels available.
 
 @since Available since 1.5.0
 
 @note The log message you're able to see in the persistent log is related to the log level
 of the SDK and the type of message you log. The default log level of the SDK is INFO level, 
 which means if you log a message with type CMTLoggerMessageTypeDebug or using
 logDebugMessage:component: method, you won't be able to see it in the persistent log.
 */
typedef NS_ENUM(NSInteger, CMTLoggerMessageType) {
    /// Type to use when logging errors (Available since 1.5.0)
    CMTLoggerMessageTypeError = 1,
    /// Type to use when logging warnings (Available since 1.5.0)
    CMTLoggerMessageTypeWarn,
    /// Type to use when logging useful information (Available since 1.5.0)
    CMTLoggerMessageTypeInfo,
    /// Type to use when logging debug messages (Available since 1.5.0)
    CMTLoggerMessageTypeDebug,
    /// Type to use when logging detailed log messages (Available since 1.5.0)
    CMTLoggerMessageTypeVerbose,
    /// Type to use when logging as much info as possible (Available since 1.5.0)
    CMTLoggerMessageTypeVerbose2,
};

/**
 CMTLogger provides the ability to log messages to a local file that is
 periodically sent to CMT servers for debugging purposes.
 @since Available since 1.0.0
 */
@interface CMTLogger : NSObject

/** Log a VERBOSE message to the persistent log
 *
 @param message The message to log
 @param component The component the message is logged for
 @since Available since 3.0.0
 */
+(void) logVerboseMessage:(NSString *_Nonnull)message component:(NSString *_Nonnull)component;

/**
 Log a VERBOSE message to the persistent log
 
 @param component The component the message is logged for
 @param format A message format string and variable list of arguments
 @param ... Argument list
 @since Available since 3.0.0
 */
+(void)logVerboseMessageFromComponent:(NSString *_Nonnull)component messageWithFormat:(NSString *_Nonnull)format, ... NS_FORMAT_FUNCTION(2, 3);
#define CMTLOG_VERBOSE(...) [CMTLogger logVerboseMessageFromComponent:NSStringFromClass([self class]) messageWithFormat: __VA_ARGS__]

/** Log an INFO message to the persistent log
 
 @param message The message to log
 @param component The component the message is logged for
 @since Available since 1.0.0
 */
+(void)logInfoMessage:(NSString *_Nonnull)message component:(NSString *_Nonnull)component;

/**
 Log an INFO message to the persistent log
 
 @param component The component the message is logged for
 @param format A message format string and variable list of arguments
 @param ... Argument list
 @since Available since 1.5.0
 */
+(void)logInfoMessageFromComponent:(NSString *_Nonnull)component messageWithFormat:(NSString *_Nonnull)format, ... NS_FORMAT_FUNCTION(2, 3);
#define CMTLOG_INFO(...) [CMTLogger logInfoMessageFromComponent:NSStringFromClass([self class]) messageWithFormat: __VA_ARGS__]

/** Log a DEBUG message to the persistent log
 
 @param message The message to log
 @param component The component the message is logged for
 @since Available since 1.0.0
 */
+(void)logDebugMessage:(NSString *_Nonnull)message component:(NSString *_Nonnull)component;

/**
 Log a DEBUG message to the persistent log
 
 @param component The component the message is logged for
 @param format A message format string and variable list of arguments
 @param ... Argument list
 @since Available since 1.5.0
 */
+(void)logDebugMessageFromComponent:(NSString *_Nonnull)component messageWithFormat:(NSString *_Nonnull)format, ... NS_FORMAT_FUNCTION(2, 3);
#define CMTLOG_DEBUG(...) [CMTLogger logDebugMessageFromComponent:NSStringFromClass([self class]) messageWithFormat: __VA_ARGS__]

/*! Log a WARNING message to the persistent log
 
 @param message The message to log
 @param component The component the message is logged for
 @since Available since 1.0.0
 */
+(void)logWarningMessage:(NSString *_Nonnull)message component:(NSString *_Nonnull)component;

/**
 Log a WARNING message to the persistent log
 
 @param component The component the message is logged for
 @param format A message format string and variable list of arguments
 @param ... Argument list
 @since Available since 1.5.0
 */
+(void)logWarningMessageFromComponent:(NSString *_Nonnull)component messageWithFormat:(NSString *_Nonnull)format, ... NS_FORMAT_FUNCTION(2, 3);
#define CMTLOG_WARNING(...) [CMTLogger logWarningMessageFromComponent:NSStringFromClass([self class]) messageWithFormat: __VA_ARGS__]

/** Log an ERROR message to the persistent log
 
 @param message The message to log
 @param component The component the message is logged for
 @since Available since 1.0.0
 */
+(void)logErrorMessage:(NSString *_Nonnull)message component:(NSString *_Nonnull)component;

/**
 Log an ERROR message to the persistent log
 
 @param component The component the message is logged for
 @param format A message format string and variable list of arguments
 @param ... Argument list
 @since Available since 1.5.0
 */
+(void)logErrorMessageFromComponent:(NSString *_Nonnull)component messageWithFormat:(NSString *_Nonnull)format, ... NS_FORMAT_FUNCTION(2, 3);
#define CMTLOG_ERROR(...) [CMTLogger logErrorMessageFromComponent:NSStringFromClass([self class]) messageWithFormat: __VA_ARGS__]

/**
 Log a message using format strings and a variable list of arguments

 @param logMessageType The type of the message
 @param component A label to distinguish this log message, typically the name of the class
 @param format Format string with argument list
 @param ... Argument list
 @since Available since 1.5.0
 */
+(void)logMessageOfType:(CMTLoggerMessageType)logMessageType
              component:(NSString *_Nonnull)component
      messageWithFormat:(NSString *_Nonnull)format, ... NS_FORMAT_FUNCTION(3, 4);

/**
 Force the log file to rotate and then upload the file immediately (BETA)

 @discussion This feature is in beta, so we advise that app developers avoid using this method
 @since Available since 7.0.0
 */
+(void) forceLogUpload;

@end
