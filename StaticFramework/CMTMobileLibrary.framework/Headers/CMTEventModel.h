//
// CMTEventModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTDataTypes.h"
#import <CoreLocation/CoreLocation.h>

/*!
 * De-serializes the events in the CMTMobileResultsModel JSON file. CMTEventModel contains
 * the properties that describe an event, such as braking, cornering, acceleration, speeding, and so on.
 *  @since Available since 0.1.0
 */
@interface CMTEventModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Type of event
 *  @since Available since 0.1.0
 *  @discussion eventType depends on the value of rawEventId. If rawEventId is not set before eventType is read,
 *  then eventType will have the wrong value. Note that when a CMTEventModel is serialized from a JSON object, it will take on the correct value.
 */
@property (readonly) CMTEventType eventType;

/*! Location at which event occurred
 *  @since Available since 0.2.51
 */
@property (nullable) CLLocation *location;

/*! UTC Time at which the event occurred
 *  @since Available since 0.1.0
 */
@property (nullable) NSDate *eventTime;

/*! Speed in km/h at which event occurred
 *  @since Available since 0.1.0
 */
@property (nullable) NSNumber * speedKmh; // speed

/*! Speed limit in km/h at which event occurred
 *  @since Available since 8.0.0
 */
@property (nullable) NSNumber * speedLimitKmh;

/*! Magnitude of event in m/s/s (floating point number)
 *  @since Available since 0.1.0
 */
@property (nullable) NSNumber * value;

/*! TRUE if event was a severe event (Boolean)
 *  @since Available since 0.1.0
 */
@property (nullable) NSNumber * severe;

/*! Duration when the relevant metric exceeds the threshold (floating point number)
 *  @since Available since 1.7.0
 */
@property (nullable) NSNumber * durationSec;

/*! Speed change due to the event (accel or brake) (floating point number)
 *  @since Available since 1.7.0
 */
@property (nullable) NSNumber * speedDeltaKmh;

/*! Matching duration for the speed change, usually ~4 sec longer than duration_sec (floating point number)
 *  @since Available since 1.7.0
 */
@property (nullable) NSNumber * durationForSpeedDeltaSec;

/*! Degree/sec for cornering events (floating point number)
 *  @since Available since 1.7.0
 */
@property (nullable) NSNumber * turnDps;

/*! TRUE if event was considered minor event (Boolean)
 *  @since Available since 0.1.0
 */
@property (nullable) NSNumber * minor;

/*! JSON representation of event
 *  @since Available since 8.0.0
 *  @discussion beta feature
 */
@property (nullable) NSData *jsonData;

/*! Raw value of event type
 *  @since Available since 8.0.0
 *  @discussion This is the integer value of the event type and will match eventType. If eventType = CMTEventTypeUnknown, this value may be different.
 *  Be sure that if you create a CMTEventModel object, that you set rawEventId before reading eventType.
 */
@property NSInteger rawEventId;

@end
