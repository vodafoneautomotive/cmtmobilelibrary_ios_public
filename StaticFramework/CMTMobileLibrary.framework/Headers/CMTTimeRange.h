//
// CMTTimeRange.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"


/**
 Data model to hold a time range
 @since Available since 1.4.0
 */
@interface CMTTimeRange : CMTMTLModel<CMTMTLJSONSerializing>

/**
 The starting date of the time range
 @since Available since 1.4.0
 */
@property (readonly,nullable) NSDate *startTime;

/**
 The ending date of the time range
 @since Available since 1.4.0
 */
@property (readonly,nullable) NSDate *endTime;

@end
