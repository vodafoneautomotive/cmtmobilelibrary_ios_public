//
// CMTAppAnalyticsScreenCategory.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Predefined Screen Categories for using App Analytics
 @since Available since 8.0.0
 */
typedef NS_ENUM(NSInteger, CMTAppAnalyticsScreenCategory) {
    /// Screen that provides a user an overview of their app performance, past trips, or other details. Often the first screen a user views when opening the app. (Available since 8.0.0)
    CMTAppAnalyticsScreenCategoryOverview,
    /// Screen that provides an overview of past trips. (Available since 8.0.0)
    CMTAppAnalyticsScreenCategoryTripList,
    /// Screen that provides detailed information about individual trip results. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryTripDetails,
    /// Screen that shows comparative user rankings based on driving score. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryLeaderboards,
    /// Screen that shows the number of times the user has exhibited positive behavior, such as 10 trips without distraction in a row. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryStreaks,
    /// Screen that shows the individual user’s behavioral improvement over time. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryTrends,
    /// Screen that shows what a user can achieve with positive behavior, such as earned or unearned badges. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryAchievements,
    /// Screen that shows the scores and trips of family or friends. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryFamily,
    /// Screen that shows the discount the driver has earned in the program to date. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryDiscount,
    /// Screen that shows earned rewards and enables the user to redeem. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryRewards,
    /// Screen that offers helpful information about the app and/or information to contact support. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryHelp,
    /// Screen that enables the user to adjust app settings. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategorySettings,
    /// Screen that offers driving tips based on user’s behavior. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryTips,
    /// Screen that requires a user to accept terms and conditions before moving on. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryTerms,
    /// Screen that enables user to register. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryRegistration,
    /// Screen that explains/requests motion permissions. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryMotionPermission,
    /// Screen that explains/requests location permissions. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryLocationPermission,
    /// Screen that explains/requests notification permissions. (Available since 8.0.0).
    CMTAppAnalyticsScreenCategoryNotificationPermission

};
