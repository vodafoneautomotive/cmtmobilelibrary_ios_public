//
// CMTSynchronousDataProviderProtocol.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Protocol used for periodical fetching
 @since Available since 1.0.0
 */
@protocol CMTSynchronousDataProviderProtocol <NSObject>

/**
 Boolean indicating the delegate needs to be updated or not
 @since Available since 1.0.0
 */
@property (readonly) BOOL needsUpdating;

/**
 Method to call on delegate if an update is needed.

 @param error If the update failed, an error will be returned, otherwise nil.
 @return YES If the update succeeds, otherwise NO
 @since Available since 1.0.0
 */
-(BOOL)synchronousReloadWithError:(NSError *_Nullable*_Nullable)error;

@end
