//
// CMTTeamSummaryTripMetricsModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTTripMetricsModel.h"

/**
 Team trip metrics data model
 @since Available since 1.7.0
 @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.")))
@interface CMTTeamSummaryTripMetricsModel : CMTTripMetricsModel<CMTMTLJSONSerializing>

@end
