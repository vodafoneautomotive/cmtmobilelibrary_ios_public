//
// CMTTagScanner.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMTTagScanner;
@class CMTUser;

/**
 *  Delegate to indicate when tags are discovered. Conforming objects need to implement this delegate to get notified.
 *
 *  @discussion Deprecated since 9.0.0; 'CMTTTagScannerDelegate' should no longer be used to link and activate tags. Use 'CMTTagActivationManager' instead.
 *  @since Available since 1.3.2
 */
__attribute__((deprecated("Deprecated since 9.0.0; 'CMTTTagScannerDelegate' should no longer be used to link and activate tags. Use 'CMTTagActivationManager' instead.")))
@protocol CMTTagScannerDelegate <NSObject>

/// Callback to class that conforms to CMTTagScannerDelegate to indicate found tag mac address.
/// @param tagScanner reference to the tag scanner object
/// @param macAddr mac address of the discovered tag
-(void) tagScanner:(CMTTagScanner *_Nonnull)tagScanner didDiscoverTagWithMacAddress:(NSString *_Nonnull)macAddr;

@end

/**
 *  Class to scan for nearby tags.
 *  Will began scanning when initWithUser:andDelegate: is called, calling the delegate didDiscoverTagWithMacAddress method
 *  for each newly discovered tag.
 *
 *  @discussion Deprecated since 9.0.0; 'CMTTTagScanner' should no longer be used to link and activate tags. Use 'CMTTagActivationManager' instead.
 *  @since Available since 1.3.2
 */
__attribute__((deprecated("Deprecated since 9.0.0; 'CMTTTagScanner' should no longer be used to link and activate tags. Use 'CMTTagActivationManager' instead.")))
@interface CMTTagScanner : NSObject

/** The list of tags discovered since this object was created. It will also include any tag that might be connected.
 *  @since Available since 1.3.2
 */
@property (readonly,atomic,nullable) NSArray <NSString *> *tagList;

/**
 App developers should not attempt to create an instance of `CMTTagScanner` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Create CMTTagScanner object
 
 @param delegate The object to receive callbacks about discovered tags on;  the delegate will be notified once of each discovered tag.
 @since Available since 1.3.2
 */
-(nonnull instancetype) initWithDelegate:(NSObject<CMTTagScannerDelegate> *_Nonnull)delegate __attribute__((deprecated("Deprecated since 3.0.3, use initWithUser:andDelegate: defined below")));


/** Constructor;  start scanning immediately for nearby tags
 @param user The current user
 @param delegate The object to receive callbacks about discovered tags on;  the delegate will be notified once of each discovered tag.
 @since Available since 3.0.3
 */
-(nonnull instancetype) initWithUser:(CMTUser *_Nonnull)user andDelegate:(NSObject<CMTTagScannerDelegate> *_Nonnull)delegate;

/**
 *  Start scanning; can be used to restart scanning after stopping
 *  @since Available since 1.4.1
 */
-(void) startScan;

/**
 *  Stop scanning; tagList will continue to return all discovered tags.
 *  @since Available since 1.3.2
 */
-(void) stopScan;

@end

