//
// CMTBadgesDataManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTUserBaseDataProvider.h"
#import "CMTBadgesModel.h"

/*!
 * Notification indicating that `badges` has changed.
 * @since Available since 0.2.5
 */
extern NSString *const _Nonnull CMTBadgesDataProviderDidChangeNotification;

/*!
 * Class to fetch the latest badges for the user
 * @since Available since 0.2.5
 * @deprecated Deprecated since 9.0.0. To get/update badge information, use the CMTURLSessionManager to communicate with the backend directly.
 * @discussion <b>Deprecated since 9.0.0</b>. To get/update badge information, use the CMTURLSessionManager to communicate with the backend directly.
 */
__attribute__((deprecated("Deprecated since 9.0.0. To get/update badge information, use the CMTURLSessionManager to communicate with the backend directly.")))
@interface CMTBadgesDataManager : CMTUserBaseDataProvider

/*!
 * Returns a reference to a CMTBadgesModel object containing all the user's badges
 * @since Available since 0.2.5
 * @deprecated Deprecated since 9.0.0. This property should not be used because this class has been deprecated.
 */
@property (readonly,atomic,nullable) CMTBadgesModel *badges
__attribute__((deprecated("Deprecated since 9.0.0. This property should not be used because this class has been deprecated.")));
/**
 App developers should not attempt to create an instance of `CMTBadgesDataManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Fetch latest badges information
 *  @since Available since 0.3.2
 *  @param error (Objective-C only) Set on failure.
 *  @return Returns a CMTBadgesModel object containing all the user's badges
 *  @throws (Swift only) NSError
 *  @deprecated Deprecated since 9.0.0. To fetch badge information, use the CMTURLSessionManager to contact the CMT endpoint: `/mobile/v3/get_badges`.
 */
-(nullable CMTBadgesModel *)loadBadgesWithError:(NSError * _Nullable __autoreleasing* _Nullable)error
__attribute__((deprecated("Deprecated since 9.0.0. To fetch badge information, use CMTURLSessionManager to contact the CMT endpoint: /mobile/v3/get_badges.")));
/*!
 *  Fetch status of badge achievement for all badges
 *  @since Available since 0.3.2
 *  @param handler Handler that will be called at completion of fetch. The handler will be passed the
 *  badge response, whether the fetch succeeded. If it failed, the error parameter will be set appropriately.
 *  @return NSOperation used to perform the background task
 *  @deprecated Deprecated since 9.0.0. To fetch badge information, use the CMTURLSessionManager to contact the CMT endpoint: `/mobile/v3/get_badges`.
 */
-(nonnull NSOperation *)loadBadgesInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTBadgesModel * _Nullable badges, BOOL success, NSError * _Nullable err))handler
__attribute__((deprecated("Deprecated since 9.0.0. To fetch badge information, use CMTURLSessionManager to contact the CMT endpoint: /mobile/v3/get_badges.")));

/*!
 *  <i>Synchronously</i> claim an array of badges
 *  @since Available since 0.2.5
 *  @param badges An array of strings corresponding to badge handle names (see CMTBadgeModel.badgeHandle)
 *  @param error (Objective-C only) Set on failure.
 *  @return (Objective-C only) YES if successful, NO otherwise
 *  @throws (Swift only) NSError
 *  @deprecated Deprecated since 9.0.0. To claim badges, use the CMTURLSessionManager to contact the CMT endpoint: `/mobile/v3/claim_badges`.
 */
-(BOOL) claimBadges:(NSArray <NSString *> *_Nonnull)badges error:(NSError * _Nullable __autoreleasing*_Nullable) error
__attribute__((deprecated("Deprecated since 9.0.0. To claim badges, use the CMTURLSessionManager to contact the CMT endpoint: /mobile/v3/claim_badges.")));

/*!
 *  <i>Asynchronously</i> claim an array of badges
 *  @since Available since 0.2.5
 *  @param badges  An array of strings corresponding to badge handle names that user wants to claim
 *  @param handler Handler that will be called at completion of server call. The handler will be passed the
 *  badge response, whether the fetch succeeded. If it failed, the error parameter will be set appropriately.
 *  @return NSOperation used to perform the background task
 *  @deprecated Deprecated since 9.0.0. To claim badges, use the CMTURLSessionManager to contact the CMT endpoint: `/mobile/v3/claim_badges`.
 */
-(nonnull NSOperation *) claimBadgesInBackground:(NSArray <NSString *> *_Nonnull)badges
                               completionHandler:(void (^_Nullable)(CMTBadgesModel *_Nullable, BOOL, NSError *_Nullable))handler
__attribute__((deprecated("Deprecated since 9.0.0. To claim badges, use the CMTURLSessionManager to contact the CMT endpoint: /mobile/v3/claim_badges.")));
@end
