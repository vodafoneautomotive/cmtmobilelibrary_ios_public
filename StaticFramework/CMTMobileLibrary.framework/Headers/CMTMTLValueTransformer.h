//
// CMTMTLValueTransformer.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CMTMTLTransformerErrorHandling.h"

NS_ASSUME_NONNULL_BEGIN
/**
 A block that represents a transformation.

 @param value The value to transform.
 @param success The block must set this parameter to indicate whether the transformation was successful. CMTMTLValueTransformer will always call this block with *success initialized to YES.
 @param error If not NULL, this may be set to an error that occurs during transforming the value.
 @return the result of the transformation, which may be nil.
 */
typedef id _Nullable (^CMTMTLValueTransformerBlock)(id value, BOOL *success, NSError **error);


/// A value transformer supporting block-based transformation.
@interface CMTMTLValueTransformer : NSValueTransformer <CMTMTLTransformerErrorHandling>

/**
 App developers should not attempt to create an instance of `CMTMTLValueTransformer` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Create a transformer which transforms values using the given block
 @param transformation block to use to forward transform values
 @return a transformer which transforms values using the given block. Reverse transformations will not be allowed.
 */
+ (instancetype)transformerUsingForwardBlock:(CMTMTLValueTransformerBlock)transformation;

/**
 Create a transformer which transforms values using the given block
 @param transformation block to use to reverse transform values
 @return a transformer which transforms values using the given block. Forward transformations will not be allowed
 */
+ (instancetype)transformerUsingReversibleBlock:(CMTMTLValueTransformerBlock)transformation;

/**
 Create a transformer which transforms values using the given blocks
 @param forwardTransformation block to use to forward transform values
 @param reverseTransformation block to use to reverse transform values
 @return a transformer which transforms values using the given blocks (both forward and reverse)
 */
+ (instancetype)transformerUsingForwardBlock:(CMTMTLValueTransformerBlock)forwardTransformation reverseBlock:(CMTMTLValueTransformerBlock)reverseTransformation;

@end

@interface CMTMTLValueTransformer (Deprecated)

+ (NSValueTransformer *)transformerWithBlock:(id (^)(id))transformationBlock __attribute__((deprecated("Replaced by +transformerUsingForwardBlock:")));

+ (NSValueTransformer *)reversibleTransformerWithBlock:(id (^)(id))transformationBlock __attribute__((deprecated("Replaced by +transformerUsingReversibleBlock:")));

+ (NSValueTransformer *)reversibleTransformerWithForwardBlock:(id (^)(id))forwardBlock reverseBlock:(id (^)(id))reverseBlock __attribute__((deprecated("Replaced by +transformerUsingForwardBlock:reverseBlock:")));

@end
NS_ASSUME_NONNULL_END
