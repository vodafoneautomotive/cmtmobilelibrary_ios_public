//
// CMTSQLiteInterface.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//



#import <Foundation/Foundation.h>

/*! Utility methods to make manipulating a SQLite database easier 
 
 @since Available since 1.2.13
 */
@interface CMTSQLiteInterface : NSObject {
}

/*! Check if the table exists 
 
 @since Available since 1.2.13
 */
- (BOOL) tableExists:(NSString * _Nonnull)table;

/*! Test if a column exists 
 
 @since Available since 1.2.13
 */
- (BOOL) columnExistsInTable:(NSString *_Nonnull)table named:(NSString *_Nonnull)col;

/*! Run the specified SQL query on the database, returning an array of results.  
 @return Each result is a
 single row that is returned, separated by vertical bar ('|') characters.
 
 @since Available since 1.2.13
 */
-(nonnull NSArray<NSString*> *)runQuery:(NSString *_Nonnull)query;

/*! Runs the specified query
 @return An array of dictionaries with the specified field names. 
 
 @since Available since 1.2.13
 */
-(nonnull NSArray<NSDictionary<NSString*,NSString*>*> *)runQuery:(NSString *_Nonnull)query
                                                withResultFields:(NSArray *_Nonnull)fields;
    
/*! Run a query for a set of fields, with an optional (may be NULL) predicate
 @return an array of dictionaries with the specified field names. 
 
 @since Available since 1.2.13
 */
-(nonnull NSArray<NSDictionary<NSString*,NSString*>*> *)queryForFields:(NSArray *_Nonnull)fields
                                                             fromTable:(NSString *_Nonnull)table
                                                   withFilterPredicate:(NSString *_Nullable)pred;

/*! Run a query for a set of fields, with an optional (may be NULL) predicate,
with specified names as output keys in returned dictionaries in outKeys
 @return an array of dictionaries with the specified field names. 
 
 @since Available since 1.2.13
 */
-(nonnull NSArray<NSDictionary<NSString*,NSString*>*> *)queryForFields:(NSArray *_Nonnull)fields
                                                             fromTable:(NSString *_Nonnull)table
                                                   withFilterPredicate:(NSString *_Nullable)pred
                                                         andOutputKeys:(NSArray *_Nonnull)outKeys;

/*! Create a table from a NSArray of 3-entry NSArrays.
 @param tableName The name of the table to create
 @param fields
 An array of 3-entry arrays.  Each array should contain a field name, a field type (as a string), and a default field value.  The default value should be an NSNumber or NSString depending on the supplied type.
    Legal types include int, long, bool, float, double, number, varchar, varchar(n), and text.
 @param createMissing If set to true,  and the specified table already exists, any missing fields will be added with the specified value as a default.
 @param initialTuple If specified, an initial tuple will be created with the default values.
 @param name If specified, an autoincrementing integer with the name provided will be set as primary key
 
 @since Available since 1.2.13
 */
-(void)    createTable:(NSString *_Nonnull)tableName
        fromFieldArray:(NSArray<NSArray<NSObject*>*> *_Nonnull)fields
andCreateMissingFields:(BOOL)createMissing
    andAddInitialTuple:(BOOL)initialTuple
   withAutoIncrementId:(NSString *_Nullable)name;

-(void)    createTable:(NSString *_Nonnull)tableName
        fromFieldArray:(NSArray<NSArray<NSObject*>*> *_Nonnull)fields
andCreateMissingFields:(BOOL)createMissing
    andAddInitialTuple:(BOOL)initialTuple;

/**
 App developers should not attempt to create an instance of `CMTSQLiteInterface` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

 /*! Open the database with the specified name 
  
  @since Available since 1.2.13
  */
-(nonnull instancetype)initWithName:(NSString *_Nonnull)name;

/*! Open the database at the specified path 
 
 @since Available since 1.2.13
 */
-(nonnull instancetype)initWithFullPath:(NSString *_Nonnull)fullPath;

/* Convenience methods */
-(nullable NSString *) stringForKey:(NSString *_Nonnull)key inTable:(NSString *_Nonnull)tableName;
-(void) setString:(NSString *_Nonnull)token forKey:(NSString *_Nonnull)key inTable:(NSString *_Nonnull)tableName;
-(BOOL) boolForKey:(NSString *_Nonnull)key inTable:(NSString *_Nonnull)tableName;
-(void) setBool:(BOOL)value forKey:(NSString *_Nonnull)key inTable:(NSString *_Nonnull)tableName;
-(int) intForKey:(NSString *_Nonnull)key inTable:(NSString *_Nonnull)tableName;
-(void) setInt:(int)val forKey:(NSString *_Nonnull)key inTable:(NSString *_Nonnull)tableName;
-(void) dropTable:(NSString *_Nonnull)tableName;
-(void) clearTable:(NSString *_Nonnull)tableName;
-(NSInteger) numberOfRows:(NSString *_Nonnull)tableName;

@end
