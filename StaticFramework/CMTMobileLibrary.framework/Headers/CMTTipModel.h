//
// CMTTipModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTDataTypes.h"

/**
 * Tip information
 * @since AVailable since 1.0.0
 * @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.")))
@interface CMTTipModel : CMTMTLModel <CMTMTLJSONSerializing>

/**
 * Tip text to show
 * @since AVailable since 1.0.0
 */
@property (nullable) NSString *text;

/**
 * Type of tip
 * @since Available since 1.0.0
 */
@property CMTTipType tipType;

@end
