//
// CMTFriendMobileInviteModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 *  If this model is present in the response, the app can use the contents to send this invitation
 *  via a SMS message.
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendMobileInviteModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! The phone number of the person you need to send the SMS invitation to
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *mobile;

/*! The message you want to send to the invitee
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *body;

@end
