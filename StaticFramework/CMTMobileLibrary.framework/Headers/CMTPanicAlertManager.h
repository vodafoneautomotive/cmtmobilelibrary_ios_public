//
// CMTPanicAlertManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMTUser;

/**
 The states after there's a panic alert detected.
 @since Available since 1.9.6
 */
typedef NS_ENUM(NSInteger, CMTPanicAlertResult) {
    /// The state that a panic alert has been triggered and sent to server. (Available since 1.9.6)
    CMTPanicAlertTriggeredAndSent,
    /// The state that a panic alert has been triggered but won't be sent to server. This is used for testing purpose. (Available since 1.9.6)
    CMTPanicAlertTriggeredButNotSent,
    /// The state that a panic alert has been triggered but suppressed by user. (Available since 1.9.6)
    CMTPanicAlertTriggeredButSuppressedByUser
};

/*!
 *  Executed by delegate on behalf of CMTPanicAlertManager *
 *  @param suppressAlert YES if Panic Alert should not be sent, No otherwise *
 *  @since Available since 1.9.6
 */
typedef void (^CMTPanicAlertConfirmationCallback)(BOOL suppressAlert);

/**
 Panic alert manager protocols
 */
@protocol CMTPanicAlertNotificationDelegate <NSObject>

@optional

/**
 Called by CMTPanicAlertManager to notify that sending of a Panic Alert has been successfully processed.

 @param result Result could be one of these below:
 
 - CMTPanicAlertTriggeredAndSent -> Panic Alert was sent to server
 - CMTPanicAlertTriggeredButNotSent -> Panic Alert was not sent to server because panicAlertTestMode was set to YES
 - CMTPanicAlertTriggeredButSuppressedByUser -> Panic Alert was not to server because it was suppressed by `confirmPanicAlertWithCallback:`
 
 @discussion If the Panic Alert was supppressed by the user, a single Panic Alert message is sent to the server, indicating that it has been suppressed.
 @since Available since 1.9.6
 */
-(void)panicAlertProcessedWithResult:(CMTPanicAlertResult)result;

/*!
 *  Called by CMTPanicAlertManager to notify that sending of a Panic Alert has failed.
 *  @since Available since 1.9.6
 */
-(void)panicAlertSendingFailed;

/*!
 *  Called by CMTPanicAlertManager to allow the delegate to confirm/suppress a pending Panic Alert.
 *  @param callback code to be executed by delegate *
 *  @since Available since 1.9.6
 */
-(void)confirmPanicAlertWithCallback:(CMTPanicAlertConfirmationCallback _Nonnull)callback;
@end


/*!
 *  This class manages the sending of user-generated Panic Alerts to the server. 
 *  @since Available since 1.9.6
 */
@interface CMTPanicAlertManager: NSObject

/*!
 *  Enable/disable Panic Alert Function has been enabled by user (as opposed to enabled in configuration).
 *  Default value is YES.
 *  @since Available since 1.9.6
 */
@property (nonatomic) BOOL panicEnabledByUser;

/*!
 *  Enable/disable Panic Alert Test mode.
 *  Default value is NO. YES indicates test mode. In test mode, Panic Alerts are not actually sent to server.
 *  @since Available since 1.9.6
 */
@property BOOL panicAlertTestMode;

/**
 App developers should not attempt to create an instance of `CMTPanicAlertManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE; 

/*!
 *  Initialize instance CMTPanicAlertManager
 *  @param user Authenticated CMTUser
 *  @param panicAlertNotificationDelegate object to receive CMTPanicAlertNotificationDelegate calls
 *  @return nil if Panic Alert not configured at server
 *  @since Available since 1.9.6
 */
-(nullable instancetype)initWithUser:(CMTUser * _Nonnull)user delegate:(NSObject<CMTPanicAlertNotificationDelegate>*_Nullable)panicAlertNotificationDelegate;

/**
 Cause panic alerts to be sent

 @discussion Intended to be called when a user presses a button or performs some action to indicate
 they are in a panic situation
 @since Available since 3.0.2
 */
-(void)sendUserInitiatedPanicAlert;

@end
