//
// CMTTagConnectionModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTDataTypes.h"

/**
 `CMTTagConnectionModel` class presents the connection info for a certain tag.
 @since Available since 1.0.0
 */
@interface CMTTagConnectionModel : CMTMTLModel

/**
 Tag identifier in 64-bit unsigned integer
 @since Available since 1.0.0
 */
@property (nonatomic) uint64_t  tagId;

/**
 Tag connection state
 @since Available since 1.0.7
 */
@property (nonatomic) CMTTagConnectedState tagState;

/**
 NSInteger indicating tag battery level
 @since Available since 1.0.0
 */
@property (nonatomic) NSInteger  tagBatteryLevel;

/**
 Interval since the last tag connection
 @since Available since 1.0.0
 */
@property (nonatomic) NSTimeInterval tagConnectionSecondsSince1970;

/**
 Phone seconds
 @since Available since 1.0.0
 */
@property (nonatomic) NSTimeInterval phoneSecondsSince1970;

/**
 Total number of connections a certain tag has
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger tagConnectionCount;

/**
 Total number of trips recorded by a certain tag
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger tagTripCount;

/**
 Tag next byte index
 @since Available since 1.0.0
 */
@property (nonatomic) uint64_t tagNextByteIndex;

/**
 Major hardware version
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger hardwareVersionMajor;

/**
 Minor hardware version
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger hardwareVersionMinor;

/**
 Major firmware version
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger firmwareVersionMajor;

/**
 Minor firmware version
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger firmwareVersionMinor;

/**
 Firmware beta version number
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger firmwareVersionBeta;


/**
 Firmware app identifier
 @since Available since 1.0.0
 */
@property (nonatomic) NSUInteger firmwareAppId;

/**
 Boolean indicating Tag is visible to user or not
 @since Available since 1.2.11
 */
@property (nonatomic, getter=isVisibleToUser) BOOL visibleToUser;

@end
