//
// CMTTripDetector.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CMTEventLogger.h"
#import "CMTDataTypes.h"

#import "CMTUser.h"
#import "CMTTripDetectorTagNotificationDelegate.h"
#import "CMTCurrentlyRecordingDriveInfo.h"
#import "CMTTripDetectorRecordingStateDelegate.h"

/**
 @since Available since 1.9.0
 */
extern NSString * _Nonnull const CMTTripDetectorErrorDomain;

/**
 @since Available since 1.9.0
 */
extern NSString * _Nonnull const CMTTripDetectorTripReadyForUpload;

/**
 @since Available since 1.9.0
 */
extern NSString * _Nonnull const CMTTripDetectorTripReadyForUploadDriveIdKey;

/**
 Different start modes when starting a trip for testing

 @since Available since 1.9.0
 */
typedef NS_ENUM(NSInteger, CMTTripDetectorForceTripStartStopReason) {
    /// TESTING ONLY: Start recording a trip with reason MANUAL (Available since 1.9.0)
    CMTTripDetectorForceTripStartStopReasonManual = 0,
    /// TESTING ONLY: Start recording a trip with reason AUTOMATIC (Available since 1.9.0)
    CMTTripDetectorForceTripStartStopReasonAutomatic = 1,
    /// TESTING ONLY: Start recording a trip with reason TAG (Available since 1.9.0)
    CMTTripDetectorForceTripStartStopReasonTag = 2
};

/**
 Error codes that could be returned when force starting a trip for testing
 
 @since Available since 1.9.0
 */
typedef NS_ENUM(NSInteger, CMTTripDetectorErrorCode) {
    /// Error code returned when trying to force start / stop a trip and developer has no permission to do so (Available since 1.9.0)
    CMTTripDetectorErrorPermissionDenied = -1,
    /// Error code returned when trying to force stop a trip and there is no recording trip (Available since 1.9.0)
    CMTTripDetectorErrorNoCurrentlyRecordingTrip = -2,
    /// Error code returned when trying to start a trip, but a trip is already started (Available since 1.9.0)
    CMTTripDetectorErrorRecordingAlreadyStarted = -3,
    /// Error code returned when trying to start an external trip, but provided externalDriveId is invalid (Available since 7.0.0)
    CMTTripDetectorErrorInvalidExternalDriveIdProvided = -4,
    /// Error code returned when trying to start an external trip, but provided used of externalDriveId is not allowed by configuration (Available since 7.0.0)
    CMTTripDetectorErrorExternalDriveIdNotAllowed = -5,
    /// Error code returned when trying to start/stop an external trip, but provided automatic drive detection is enabled (Available since 7.0.0)
    CMTTripDetectorErrorExternalTripNotAllowedWithDriveDetectionEnabled = -6,
    /// Error code returned when trying to start/stop an external trip, but request has timed out (Available since 7.0.0)
    CMTTripDetectorErrorExternalTripRequestTimedOut = -7,
    /// Error code returned when trying to start/stop a trip, but an internal error occurred (Available since 7.0.0)
    CMTTripDetectorErrorInternalError = -8
};

/*!
 *  If this key is present in the `tripDetectorSuppressionState` variable, trip recording is suppressed
 *  because the battery is too low
 *  @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTTripDetectorSuppressedLowBatteryKey;

/*!
 *  If this key is present in the `tripDetectorSuppressionState` variable, trip recording is suppressed
 *  because the user is not authorized
 *  @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTTripDetectorSuppressedUserNotAuthorizedKey;

/*!
 *  If this key is present in the `tripDetectorSuppressionState` variable, trip recording is suppressed
 *  because standby mode is activated
 *  @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTTripDetectorSuppressedStandbyModeActiveKey;

/*!
 *  If this key is present in the `tripDetectorSuppressionState` variable, trip recording is suppressed
 *  because trial has expired 
 *  @since Available since 2.0.0
 */
extern NSString * _Nonnull const CMTTripDetectorSuppressedTrialExpiredKey;

/*!
 *  If this key is present in the `tripDetectorSuppressionState` variable, trip recording is suppressed
 *  because driver is off-duty
 *  @since Available since 2.0.0
 */
extern NSString * _Nonnull const CMTTripDetectorSuppressedDriverOffDutyKey;

/*!
 *  If this key is present in the `tripDetectorSuppressionState` variable, trip recording is suppressed
 *  because the start date hasn't been reached yet.
 *  @since Available since 6.0.0
 */
extern NSString * _Nonnull const CMTTripDetectorSuppressedStartDateNotReachedKey;

/**
 *  @typedef CMTTripDetectorLocationCallback is a function that can be used to stop the current recording trip
 *
 *  @param continueRecording If continueRecording is false, the trip will be stopped. Otherwise, continue recording
 *  @param stopReason        If continueRecording is false, the trip will stop with reason `stopReason`. Otherwise, it is nil or undefined.
 *
 *  @since Available since 1.0.0
 */
typedef void (^CMTTripDetectorLocationCallback)(BOOL continueRecording, CMTStopReasonType * _Nullable stopReason);

/**
 *  @typedef CMTTripDetectorLocationFilter is a function that is used to process location points examined by the Drive Detector
 *
 *  @param location A location object that can be processed by the function
 *  @param callback A callback function to pass with the type as described above
 *
 *  @since Available since 1.0.0
 */
typedef void (^CMTTripDetectorLocationFilter)(CLLocation * _Nullable location, CMTTripDetectorLocationCallback _Nonnull callback);


/**
 Class used to detect the start of drives and the current trip recording status. Set the enabled property of the CMTTripDetector object associated 
 with the CMTUser object to enable / disable the trip detector.
 @since Available since 1.0.0
 */
@interface CMTTripDetector : NSObject

/**
 App developers should not attempt to create an instance of `CMTTripDetector` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 *  @warn TESTING ONLY: Start a trip with a given reason. This code should never be available to end users. It is intended for QA only.
 *        Also, the trip detector must be enabled in order for these APIs to start recording.
 *
 *  @param startReason Start reason for this trip
 *  @param error (Objective-C only) Set on failure.  Currently, the only possible error codes are
 *                                  CMTTripDetectorErrorPermissionDenied, CMTTripDetectorErrorRecordingAlreadyStarted
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError (see error codes above)
*/
-(BOOL)startRecordingTestTripWithReason:(CMTTripDetectorForceTripStartStopReason) startReason error:(NSError * _Nullable __autoreleasing * _Nullable)error;

/**
 *  @warn TESTING ONLY: Start a trip with a given reason. This code should never be available to end users. It is intended for QA only.
 *        Also, the trip detector must be enabled in order for these APIs to start recording.
 *
 *  @param cannedTripIdentifier Identifier for canned trip data to substitute when processing this drive on the server
 *  @param externalTripIdentifier  User supplied trip identifier if non-nil will be used instead of auto-generated UUID (Added in 7.0.0)
 *  @param error (Objective-C only) Set on failure.  Currently, the only possible error codes are
 *                                  CMTTripDetectorErrorPermissionDenied, CMTTripDetectorErrorRecordingAlreadyStarted
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError (see error codes above)
 *
 *  @since Available since 1.9.0
 *  @deprecated Deprecated since 9.0.0, use the portal to create canned test trips.
 */
-(BOOL)startRecordingTestTripWithCannedTripIdentifier:(NSString * _Nonnull)cannedTripIdentifier
                               externalTripIdentifier:(NSString* _Nullable)externalTripIdentifier
                                                error:(NSError * _Nullable __autoreleasing * _Nullable)error DEPRECATED_MSG_ATTRIBUTE("Deprecated since 9.0.0, use the portal to create canned test trips.");

/**
 *  @warn TESTING ONLY: Stop a trip with a given reason. This code should never be available to end users. It is intended for QA only.
 *
 *  @param stopReason  Stop reason for this trip
 *  @param error (Objective-C only) Set on failure.  Currently, the only possible error codes are
 *                                  CMTTripDetectorErrorPermissionDenied, CMTTripDetectorErrorNoCurrentlyRecordingTrip
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError (see error codes above)
 *
 *  @since Available since 1.9.0
 */
-(BOOL)stopRecordingTestTripWithReason:(CMTTripDetectorForceTripStartStopReason) stopReason error:(NSError * _Nullable __autoreleasing * _Nullable)error;

/**
 *  This method allows you to set when a trip starts based on the requirements of your app, rather than using the SDK's automatic trip detection.
 *  @param externalTripIdentifier Optional, user-supplied trip identifier to be used instead of the CMT auto-generated UUID.
 *                                externalTripIdentifier requirements:
 *                                 (1) globally unique
 *                                 (2) allowed characters: 'A'-'Z', 'a'-'z', '0'-'9', and '-'.
 *                                This method will fail immediately if (2) is violated. If (1) is violated, the method will succeed, but the trip
 *                                may not be processed correctly due to a naming collision with an existing drive.
 *  @param handler to be executed on completion of attempted trip start (successful or otherwise)
 *  @discussion This method will stop a currently recording trip, if any, before starting new trip.
 *  @discussion This method should only be called when 'enabled' == NO (i.e. automatic trip detection is disabled). It will fail immediately if
 *              'enabled' == YES.
 *  @since Available since 7.0.0
 *
 */
-(void)startRecordingExternalTripWithExternalTripIdentifier:(nullable NSString*)externalTripIdentifier
                                          completionHandler:(nullable void (^)(CMTCurrentlyRecordingDriveInfo * _Nullable driveInfo, NSDate * _Nullable startDate,
                                                                               BOOL success, NSError * _Nullable error) ) handler;

/**
 *  This method allows you to set when a trip ends based on the requirements of your app, rather than using the SDK's automatic trip detection.
 *  @param handler to be executed on completion of attempted trip stop (successful or otherwise)
 *  @discussion This method should only be called when 'enabled' == NO (i.e. automatic trip detection is disabled). It will fail immediately if
 *              'enabled' == YES.
 *  @since Available since 7.0.0
 */
-(void)stopRecordingExternalTripWithCompletionHandler:(nullable void (^)(CMTCurrentlyRecordingDriveInfo * _Nullable driveInfo, NSDate * _Nullable stopDate,
                                                                         BOOL success, NSError * _Nullable error)) handler;

/*!
 *  Stop the trip detector
 *  @since Available since 1.0.0
 */
-(void)resetTripDetector;

/*!
 *  Write a user-defined value into the trip recording. One value allowed per trip.
 *  @param note The value to be written. Must be a valid serialized JSON object,
 *         e.g. value returned by [NSJSONSerialization dataWithJSONObject:options:error:];
 *         Top level JSON object must be an NSDictionary or NSArray.
 *  @return NO if note is not valid
 *  @discussion The value written here is associated with the trip as a whole, not with any particular time or event within the trip. This method is intended
 *              to be called only once during a trip. It can be called multiple times within a trip, but only the last value will be persisted.
 *              If called outside of a trip recording, the value may not be persisted.
 *  @since Available since 7.0.0
 */
-(BOOL)writeTripNoteSingle:(nonnull NSData *)note;

/*!
 *  Write a user-defined value at the current point in the trip recording. Multiple values allowed per trip.
 *  @param note The value to be written. Must be a valid serialized JSON object,
 *         e.g. value returned by [NSJSONSerialization dataWithJSONObject:options:error:];
 *         Top level JSON object must be an NSDictionary or NSArray.
 *  @return NO if note is not valid
 *  @discussion This may be called multiple times within a trip. All values will be persisted.
 *              If called outside of a trip recording, the value may not be persisted.
 *  @since Available since 7.0.0
 */
-(BOOL)writeTripNoteMany:(nonnull NSData *)note;

/*!
 * The identifier of the connected tag. If the CMTTripDetector is in tag mode and there is a connected tag, this return a non-zero identifier.
 * Otherwise, it returns 0.
 * @since Available since 1.0.7
 */
@property (readonly) uint64_t connectedTagIdentifier;

/**
 *  Even though trip detection may be enabled, sometimes trips will not record.
 *  This dictionary provides information about why trip recording could be disabled.
 *
 *  @since Available since 1.0.0
 */
@property (readonly, nullable) NSDictionary <NSString*, NSNumber*> * tripDetectorSuppressionState;

/*!
 *  Set to YES if a trip is currently being recorded and NO otherwise.
 *  For non-tag and non-manual trips, will only be true if a valid gps point with high enough speed has been recorded.
 *  For tag trips, will be true as long as data is being recorded.
 *  For manual trips, will be true/false after manual start/stop.
 *
 *  @since Available since 1.0.0
 */
@property (readonly) BOOL recordingDrive;

/**
 Allow drive detector to record only after this date.
 
 @discussion A trip recording will be suppressed if
 
 - `standbyUntilDate` is not nil *AND*
 - `standbyUntilDate` > `[NSDate date]` (the current date)
 
 The value will be reset to nil when attempting to start a trip and `standbyUntilDate` is in the past. 
 If `standbyUntilDate` is set to a time before the current date, then standby will be canceled.
 
 @since Available since 1.0.0
 */
@property (atomic, copy, nullable) NSDate * standbyUntilDate;

/*!
 *  If set, this function will be called to filter location points
 *  @since Available since 1.0.0
 */
@property (atomic, strong, nullable) CMTTripDetectorLocationFilter locationFilter;

/**
 * If set to YES and [CMMotionActivityManager isActivityAvailable], motion activity will be used 
 * to enhance trip detection for the phone only trip detector.
 * @since Available since 3.0.0
 */
@property BOOL enableMotionActivitySensing;

/**
 *  True if trip detection is enabled, False otherwise
 *  @since Available since 1.0.0
 */
@property BOOL enabled;

/**
 *  Set this to an object to receive messages about trips starting / stopping to record
 *  @since Available since 1.0.0
 */
@property (weak, atomic, nullable) NSObject<CMTTripDetectorRecordingStateDelegate> * recordingStateDidChangeDelegate;

/**
 *  Set this to an object to receive messages about tag state changes. The delegate
 *  will only receive messages when the user is a tag user and the trip detector is enabled.
 *  @since Available since 1.0.0
 */
@property (weak, atomic, nullable) NSObject<CMTTripDetectorTagNotificationDelegate> * tagNotificationDelegate;

/**
 *  Information about the current drive
 *  @since Available since 1.0.0
 */
@property (readonly, nullable) CMTCurrentlyRecordingDriveInfo * currentDriveInfo;

/*!
 * If TRUE, then honor the white list when connecting to a tag. Default is FALSE.
 *
 * @discussion This value will persist between launches.
 *
 * @since Available since 1.9.0
 */
@property (nonatomic, assign) BOOL honorTagWhiteList;

/*!
 * The set of tags this user can connect to.
 *
 * @discussion Users of this API can provide a set containing NSString objects
 * that represent tag MAC addresses. MAC addresses are 6-byte (48-bits) in length, and are
 * written in MM:MM:MM:SS:SS:SS format. Typically, the first 3-bytes are the ID number of the
 * manufacturer, which is assigned by an Internet standards body. The second 3-bytes are
 * serial numbers assigned by the manufacturer.
 *
 * The whitelist is honored if honorTagWhiteList is set to YES. Otherwise, it is ignored.
 *
 * If honorTagWhiteList is YES and this value is set to nil or the empty set,
 * NO tags will be allowed to connect. If honorTagWhiteList is YES and
 * this is set to a non-empty set to only connect to tags in the set.
 *
 * If set to nil or the empty set, this property will return nil.
 *
 * Also, this value will persist between launches.
 *
 * @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSSet<NSString *> * tagWhiteListAddressSet;

@end
