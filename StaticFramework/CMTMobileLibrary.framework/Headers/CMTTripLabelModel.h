//
// CMTTripLabelModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTDataTypes.h"

/*!
 * Used to describe the mode of transport of a trip or whether a trip is a business or private trips
 * @since Available since 1.0.0
 */
@interface CMTTripLabelModel : CMTMTLModel<CMTMTLJSONSerializing>

/** 
 * User provided trip label
 * @since Available since 1.0.0
 */
@property CMTTransportModeType transportationMode;

/**
 * True if trip was marked as a business trip (Boolean), Default is false.
 * @since Available since 1.0.0
 */
@property (nullable) NSNumber *businessTrip;

/** 
 * Note provided by user, default is empty string.
 * @since Available since 1.0.0
 */
@property (nullable) NSString *businessNote;

/**
 * Label is applied to `driveId` trip
 * @since Available since 1.0.0
 */
@property (nullable) NSString *driveId;


@end
