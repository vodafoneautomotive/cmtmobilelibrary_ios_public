//
// CMTDriveScheduleManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTSynchronousDataProviderProtocol.h"
/*!
 *  Error Domain
 *  @since Available since 1.3.22
 */
extern NSString * _Nonnull const CMTDriveScheduleManagerErrorDomain;

/**
 Error types from the CMTDriveScheduleManager
 
 @since Available since 1.3.22
 */

typedef NS_ENUM(NSInteger, CMTDriveScheduleManagerErrorType) {
    /// Operation cancelled (Available since 1.3.22)
    CMTDriveScheduleManagerOperationCancelled = 1,
    /// Key not found (Available since 1.5.0)
    CMTDriveScheduleManagerUserProfileGetDriveScheduleKeyNotFound = 2
};

@class CMTUser, CMTRecordingScheduleModel;
@protocol CMTSynchronousDataProviderProtocol;

/*!
 *  Manages Driver Schedules
 *  @since Available since 1.3.22
 */
@interface CMTDriveScheduleManager : NSObject <CMTSynchronousDataProviderProtocol>

/** Intervals of times where trip recorder should suppress trip recordings
 * @since Available since 1.5.0
 */
@property (readonly,nullable) CMTRecordingScheduleModel *recordingSchedule;

/**
 App developers should not attempt to create an instance of `CMTDriveScheduleManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Create instance of CMTDriveScheduleManager class
 *  @since Available since 1.3.22
 *  @param user Authenticated CMTUser object
 *  @return An instance of a CMTDriveScheduleManager object
 */
- (nonnull instancetype) initWithUser:(CMTUser *_Nonnull)user NS_DESIGNATED_INITIALIZER;

/**
 * Fetch the latest trip recording schedule
 *  @since Available since 1.3.22
 *  @param error (Objective-C only) Set on failure.
 *  @return A reference to the recording schedule or nil if the operation failed
 *  @throws (Swift only) NSError
 */
- (nullable CMTRecordingScheduleModel *) loadScheduleWithError:(NSError * _Nullable __autoreleasing* _Nullable)error;

/**
 * <i>Asynchronously</i> fetch the latest trip recording schedule from the server
 * @since Available since 1.3.22
 * @param handler Handler which gets the following parameters: latest recording schedule,
 *                                                             Whether the fetch failed or succeeded,
 *                                                             Whether an error occurred or not
 * @return An NSOperation which can be used to cancel the fetch.
 * @deprecated Deprecated since 9.0.0, use loadScheduleWithHandler:/loadSchedule(handler:) instead.
 */
- (nonnull NSOperation*)loadScheduleInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTRecordingScheduleModel * _Nullable value,
                                                                                         BOOL success, NSError * _Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use loadScheduleWithHandler:/loadSchedule(handler:) instead ")));

/**
 * <i>Asynchronously</i> fetch the latest trip recording schedule from the server
 * @since Available since 9.0.0
 * @param handler Handler which gets the following parameters: latest recording schedule,
 *                                                             Whether the fetch failed or succeeded,
 *                                                             Whether an error occurred or not
 */
- (void)loadScheduleWithHandler:(void (^_Nullable)(CMTRecordingScheduleModel * _Nullable value, BOOL success, NSError * _Nullable error)) handler;

/**
 * Reset stored schedule
 *  @since Available since 1.3.22
 *  @param error (Objective-C only) Set on failure.
 *  @return (Objective-C only) YES if successful, NO otherwise
 *  @throws (Swift only) NSError
 */
- (BOOL)resetWithError:(NSError * _Nullable __autoreleasing*_Nullable)error;

/*!
 * Check on-duty or off-duty for a give time
 * @since Available since 1.5.0
 * If no recording schedule is found on this object, YES will be returned
 * @param date If nil, current time will be used
 * @return YES if on-duty, otherwise off-duty
 */
- (BOOL)onDutyAtDate:(NSDate *_Nullable)date;

/*!
 * Get the next on-duty time for a given time
 * @since Available since 1.5.0
 * @param date If nil, current time will be used
 * @return An NSdate object will be returned, note that if recordingSchedule is in the past,
 * distant past will be returned
 */
- (nonnull NSDate *)nextOnDutyDateSinceDate:(NSDate *_Nullable)date;

/*!
 * Get the next off-duty time for a given time
 * @since Available since 1.5.0
 * @param date If nil, current time will be used
 * @return An NSdate object will be returned, note that if recordingSchedule is in the past,
 * distant past will be returned
 */
- (nonnull NSDate *)nextOffDutyDateSinceDate:(NSDate *_Nullable)date;

@end
