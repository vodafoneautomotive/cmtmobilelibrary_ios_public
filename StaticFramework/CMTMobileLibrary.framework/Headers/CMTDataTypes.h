//
// CMTDataTypes.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Types of tips
 @since Available since 0.1.0
 @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.
 */
 __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.")))
typedef NS_ENUM(NSInteger, CMTTipType){
    /// Unknown tip type (Available since 0.1.0)
    CMTTipUnknown,
    /// Tip about using phone too much (Available since 0.1.0)
    CMTTipPhoneUse = 1,
    /// Tip about braking too harshly (Available since 0.1.0)
    CMTTipBraking = 2,
    /// Tip about turning too harshly (Available since 0.1.0)
    CMTTipTurning = 3,
    /// Tip about accelerating too harshly (Available since 0.1.0)
    CMTTipAccel = 4,
    /// Tip about too much speeding (Available since 0.1.0)
    CMTTipSpeeding = 5,
    /// Tip about driving hours (Available since 0.1.0)
    CMTTipNight = 6,
    /// Tip about distance (Available since 0.2.4)
    CMTTipDistance = 7,
    /// Tip about smoothness (Available since 0.2.4)
    CMTTipSmoothness = 8,
    /// Tip about time of day (Available since 0.2.4)
    CMTTipTimeOfDay = 9,
    /// Tip about roads used (Available since 0.2.4)
    CMTTipRoads = 10
};

/**
 Trip event types
 @since Available since 0.1.0
 */
typedef NS_ENUM(NSInteger, CMTEventType){
    ///Unknown type (Available since 0.1.0)
    CMTEventTypeUnknown = 0,
    ///Moving event, this type is here for documentation purposes only, it will never be sent.(Available since 0.1.0)
    CMTEventTypeMoving = 1,
    ///Braking event (Available since 0.1.0)
    CMTEventTypeBraking = 2,
    ///Turning event (Available since 0.1.0)
    CMTEventTypeTurning = 3,
    ///Acceleration event (Available since 0.1.0)
    CMTEventTypeAccelerating = 4,
    ///Speeding event (Available since 8.0.0)
    CMTEventTypeSpeeding = 5,
    ///Phone Motion event (Available since 8.0.0)
    CMTEventTypePhoneMotion = 6,
    ///Calling event (Available since 8.0.0)
    CMTEventTypeCalling = 7,
    ///Tapping event (Available since 8.0.0)
    CMTEventTypeTapping = 8,
};

/**
 Phone motion types
 @since Available since 0.1.0
 @brief Types of phone motion
 */
typedef NS_ENUM(NSInteger, CMTPhoneMotionType){
    /// Not present (Available since 0.1.0)
    CMTPhoneMotionDefault = 0,
    /// Some kind of phone motion exists (Available since 0.1.0)
    CMTPhoneMotionExists = 1
};

/**
 Primary segment types
 @since Available since 1.0.6
 */
typedef NS_ENUM(NSInteger, CMTPrimarySegmentType) {
    /// Prepended / estimated segment (Available since 1.0.6)
    CMTPrimarySegmentTypeEstimated = -1,
    /// Not speeding segment (Available since 1.0.6)
    CMTPrimarySegmentTypeNotSpeeding = 0,
    /// Speeding at level 1 (Available since 1.0.6)
    CMTPrimarySegmentTypeSpeedingOne,
    /// Speeding at level 2 (Available since 1.0.6)
    CMTPrimarySegmentTypeSpeedingTwo,
    /// Speeding at level 3 (Available since 1.0.6)
    CMTPrimarySegmentTypeSpeedingThree
};

/**
 Secondary segment types
 @since Available since 1.0.6
 */
typedef NS_ENUM(NSInteger, CMTSecondarySegmentType) {
    /// No notable events (Available since 1.0.6)
    CMTSecondarySegmentTypeDefault = 0,
    /// Phone motion present (Available since 1.0.6)
    CMTSecondarySegmentTypePhoneMotion = 1,
};

/**
 Trip transportation types
 @since Available since 0.1.0
 @brief Types of transportation modes
 */
typedef NS_ENUM(NSInteger, CMTTransportModeType) {
    /// Not yet labeled (Available since 1.0.0)
    CMTTransportModeTypeNotLabeled,
    /// Not a car (Available since 0.1.0)
    CMTTransportModeTypeNotCar,
    /// Other mode (Available since 0.1.0)
    CMTTransportModeTypeOther,
    /// Driver of a car (Available since 0.1.0)
    CMTTransportModeTypeDriver,
    /// Car passenger (Available since 0.1.0)
    CMTTransportModeTypePassenger,
    /// Bus (Available since 0.1.0)
    CMTTransportModeTypeBus,
    /// Train (Available since 0.1.0)
    CMTTransportModeTypeTrain,
    /// Motorcycle (Available since 0.1.1)
    CMTTransportModeTypeMotorcycle,
    /// Foot (Available since 0.1.0)
    CMTTransportModeTypeFoot,
    /// Boat (Available since 0.1.0)
    CMTTransportModeTypeBoat,
    /// Airplane (Available since 0.1.0)
    CMTTransportModeTypeAirplane,
    /// ATV (Available since 0.1.0)
    CMTTransportModeTypeATV,
    /// Bicycle (Available since 0.1.0)
    CMTTransportModeTypeBicycle,
    /// Driver but ignore distraction score (Available since 2.1.0)
    CMTTransportModeTypeDriverNoDistraction,
};

/**
 Possible trip stop reasons:
 @since Available since 0.1.0
 @brief Various ways that a trip can be stopped
 */
typedef NS_ENUM(NSInteger, CMTStopReasonType){
    /// Manually stopped (Available since 0.1.0)
    CMTStopReasonTypeManual = 0,
    /// Automatically stopped (Available since 0.1.0)
    CMTStopReasonTypeAutomatic,
    /// Tag stopped (Available since 0.3.0)
    CMTStopReasonTypeTag,
    /// Stopped due to app being quit (Available since 0.1.0)
    CMTStopReasonTypeForced,
    /// Stopped due to battery being too low (Available since 0.1.0)
    CMTStopReasonTypeLowBattery,
    /// Stopped due to entering a region for which we can't record (Available since 0.3.0)
    CMTStopReasonTypeInvalidLocation,
    /// Stopped because user activated standby (Available since 0.3.0)
    CMTStopReasonTypeStandbyActivated
};

/**
 Trip start reasons
 @since Available since 0.3.0
 @brief reasons for trip starting
 */
typedef NS_ENUM(NSInteger, CMTStartReasonType) {
    /// Reason uknown (Available since 0.3.0)
    CMTStartReasonTypeUnknown = 0,
    /// Manually started (Available since 0.3.0)
    CMTStartReasonTypeManual,
    /// Started because location change detected (Available since 0.3.0)
    CMTStartReasonTypeAutomatic,
    /// Tag started the trip (Available since 0.3.0)
    CMTStartReasonTypeTag
};

/**
 Trip processing status types
 @since Available since 0.1.0
 @brief Tracks the state of a trip that has not yet been processed.
 */
typedef NS_ENUM(NSInteger, CMTTripProcessingStatusType) {
    /// Trip recorded during invalid time (Available since 4.0.0)
    CMTTripProcessingStatusTypeRecordedDuringInvalidTime = -4,
    /// Trip is transitioning from recording to waiting to send (Available since 3.0.0)
    CMTTripProcessingStatusTypeRecordingCompleted = -3,
    /// Trip has been stopped by a force quit (Available since 3.0.0)
    CMTTripProcessingStatusTypeForcedQuit = -2,
    /// Trip is currently being recorded (Available since 0.3.2)
    CMTTripProcessingStatusTypeCurrentlyRecording = -1,
    /// Trip has not been uploaded, but is waiting for a valid network connection (Available since 0.1.0)
    CMTTripProcessingStatusTypeWaitingToSend = 0,
    /// Trip is in the process of being uploaded (Available since 0.1.0)
    CMTTripProcessingStatusTypeSending = 1,
    /// Trip has been successfully received by the server and is being processed (Available since 0.1.0)
    CMTTripProcessingStatusTypeSent = 2,
    /// Trip processing has failed (Available since 0.1.0)
    CMTTripProcessingStatusTypeFailed = 3,
    /// Trip processing has completed (Available since 0.1.0)
    CMTTripProcessingStatusTypeProcessed = 4,
};

/**
 Tag connection states
 @since Available since 1.1.0
 @brief connection state of tag
 */
typedef NS_ENUM(NSInteger, CMTTagConnectedState) {
    /// Tag is disconnected (Available since 0.1.0)
    CMTTagConnectedStateDisconnected = 0,
    /// Tag is connected (Available since 0.1.0)
    CMTTagConnectedStateConnected
};

/**
 Message types
 @since Available since 1.1.0
 */
typedef NS_ENUM(NSInteger, CMTMessageType) {
    /// Cheer type (Available since 0.1.0)
    CMTMessageTypeCheer = 0,
    /// Taunt type (Available since 0.1.0)
    CMTMessageTypeTaunt = 1
};

/**
 Error Codes that correspond to server issues
 @since Available since 0.1.0
 */
typedef NS_ENUM(NSInteger, CMTServerError) {
    /// An unknown error occured. Usually this means that no network connection is available (Available since 0.1.0).
    CMTServerErrorUnknownError = 0,
    /// HTTP 400 error, returned when trying to register a new user and some unique identifier associated with the user is already in CMT's database (Available since 0.1.0).
    CMTServerErrorAlreadyRegistered = 1,
    /// Profile management APIs (e.g., login/(pre)register/(pre)activate) are capped. Returned when a maximum number of requests is attempted (Available since 0.1.0).
    CMTServerErrorTooManyRequests = 2,
    /// HTTP 400, the account associated with the information submitted can not be found (Available since 0.1.0).
    CMTServerErrorAccountNotFound = 3,
    /// HTTP 400, request to sign in contained expired OTP (auth code) (Available since 0.1.0).
    CMTServerErrorAuthCodeExpired = 4,
    /// HTTP 400, request to login contained wrong OTP (auth code) for user (Available since 0.1.0).
    CMTServerErrorWrongAuthCode = 5,
    /// CMTServerErrorFacebookRequest400 description (Available since 0.1.0).
    CMTServerErrorFacebookRequest400 = 6,
    /// HTTP 400, request was made to change profile information, but the changes would cause in conflict with another user's information (Available since 0.1.0).
    CMTServerErrorUniquenessFailure = 7,
    /// HTTP 400 error, request was bad or malformed (Available since 0.1.0).
    CMTServerErrorBadRequest = 8,
    /// HTTP 500 error, corresponding API is not implemented at the server (Available since 0.1.0).
    CMTServerErrorUnimplemented = 9,
    /// HTTP 400, user is not authorized to use this server (Available since 0.1.0).
    CMTServerErrorNotAuthorized = 10,
    /// HTTP 400, called API is not supported for the calling app (Available since 0.1.0).
    CMTServerErrorNotSupported = 11,
    /// HTTP 500, server error (Available since 0.1.0).
    CMTServerErrorInternalError = 12,
    /// HTTP 400, Request triggered email to be sentd, but previous emails to address have bounced (Available since 0.1.0).
    CMTServerErrorEmailBounce = 13,
    /// HTTP 400, Request triggered email to be sentd, but previous emails to address have results in complaints  (Available since 0.1.0).
    CMTServerErrorEmailComplaint = 14,
    /// HTTP 400, a request was sent to a remote server to request a quote, but the receiving server returned a 400 error (Available since 0.1.0).
    CMTServerErrorRequestQuote400 = 15,
    /// HTTP 400, request didn’t make it to our handler code. This can happen by using the wrong HTTP verb (GET or POST) or otherwise failing to speak proper HTTP (Available since 0.1.0).
    CMTServerErrorTornadoError = 16,
    /// HTTP 500, a request was sent to a remote server to request profile information, but the receiving server returned a 500 error (server error) (Available since 0.1.0).
    CMTServerErrorFacebookRequest500 = 17,
    /// HTTP 500, a request was sent to a remote server to request a quote, but the receiving server returned a 500 error (server error) (Available since 0.1.0).
    CMTServerErrorRequestQuote500 = 18,
    /// HTTP 400, an invalid registration token was passed to the server (Available since 0.1.0).
    CMTServerErrorInvalidRegToken = 19,
    /// HTTP 500, a request was sent to a remote server to request profile information, but the receiving server returned a 500 error (server error) (Available since 0.1.0).
    CMTServerErrorGetProfile500 = 20,
    ///  HTTP 400, udpate profile not authorized (Available since 0.1.0).
    CMTServerErrorUpdateProfileNotAuthorized = 21,
    /// HTTP 400, request relies on a remote server, but the remote server returned an error (Available since 0.1.0).
    CMTServerErrorRemoteServerError = 22,
    /// HTTP 400, Impact not found (Available since 0.1.0).
    CMTServerErrorImpactNotFound = 23,
    /// HTTP 400, Already activated (Available since 0.1.0).
    CMTServerErrorAlreadyActivated = 24,
    /// HTTP 400, Invalid enrollment (Available since 0.1.0).
    CMTServerErrorInvalidEnrollment = 25,
    /// HTTP 400, Wrong app (Available since 0.1.0).
    CMTServerErrorWrongApp = 26,
    /// HTTP 400, Trial period has expired (Available since 0.1.0).
    CMTServerErrorTrialExpired = 27,
    /// HTTP 400, the app cannot link the tag because the tag does not belong to you or your tag provider (Available since 0.1.0).
    CMTServerErrorNotYourTag = 28,
    /// HTTP 400, the app cannot link the tag because the tag is not known in the CMT backend (Available since 0.1.0).
    CMTServerErrorUnknownTag = 29,
    /// HTTP 400, The tag you are trying to link with is already linked to another vehicle (Available since 0.1.0).
    CMTServerErrorTagAlreadyInUse = 30,
    /// HTTP 400, Vehicle is not in allow list (Available since 1.7.0).
    CMTServerErrorVehicleIdNotInAllowList = 51,
    /// HTTP 400, Cannot unlink tag (Available since 1.9.2).
    CMTServerErrorCannotUnlink = 52,
    /// HTTP 400, The request to set the profile photo failed (Available since 1.9.2).
    CMTServerErrorPhotoUploadError = 53,
    /// HTTP 400, The vehicle that you are trying to link a tag with is not allowed to be linked (Available since 1.9.2).
    CMTServerErrorVehicleNotEligibleForTagLinking = 54,
    /// HTTP 400, The vehicle you are trying to link with cannot be found in the CMT backend (Available since 1.7.0).
    CMTServerErrorVehicleIdNotFound = 55,
    /// HTTP 400, Drive not found (Available since 1.9.2).
    CMTServerErrorDriveNotFound = 56,
    /// HTTP 400, Unknown user (Available since 1.9.2).
    CMTServerErrorUnknownUser = 57,
    /// HTTP 400, Tag cannot be reused (Available since 1.9.2).
    CMTServerErrorCannotReuseTag = 58,
};

/*!
 *  Error domain for CMTServerError NS_ENUM
 */
extern NSString *const CMTServerErrorDomain;

/**
 Email formats
 @since Available since 0.1.0
 @brief Format of email content
 */
typedef NS_ENUM(NSInteger, CMTEmailFormat){
    /// Unknown email format (Available since 0.1.0)
    CMTEmailFormatUnknown = 0,
    /// Text format (Available since 0.1.0)
    CMTEmailFormatText = 1,
    /// HTML format (Available since 0.1.0)
    CMTEmailFormatHTML = 2
};

/*! 
 * Default tag identifier
 * @since Available since 3.0.0
 */
extern uint64_t const CMTTagDefaultIdentifier;

@interface CMTDataTypes : NSObject

@end
