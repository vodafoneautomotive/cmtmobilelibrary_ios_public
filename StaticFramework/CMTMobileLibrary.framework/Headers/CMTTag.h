//
// CMTTag.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"


/// Publicly accessible version 
@interface CMTTag : CMTMTLModel<CMTMTLJSONSerializing>

/**
 Tag Mac Address of the tag
 @since Available since 4.0.0
 */
@property (readonly, nonatomic, nullable) NSString *tagMacAddress;

/**
 App developers should not attempt to create an instance of `CMTTag` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

@end
