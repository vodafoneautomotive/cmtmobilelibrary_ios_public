//
// CMTSVRDetector.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CMTSVRDetectorDelegate.h"

@class CMTUser;
extern NSString * _Nonnull const CMTSVRDetectorMAC;
extern NSString * _Nonnull const CMTSVRDetectorAlerts;

/**

 CMTSVRDetector detects stolen vehicle tags that are in the proximity of the app
 and reports their presence to CMT servers.
 
 Once a CMTSVRDetector object has been created (via initWithUser), SVR detection must be activated by calling
 startMonitoringForSVRBeacons.  SVR detection relies on bluetooth region monitoring, and will cause the app to
 relaunch in the background when a SVR tag is detected, until stopMonitoring or logoutUser are called.
 
 Apps that need to use the SVR functionality will need to be able to "launch" in the background on iOS for regions
 or locations. The app must ensure the following in order for SVR detection to function:
 
 - the "App registers for location updates" field is present in the background modes key of the app plist
 - the "Privacy - Location Always Usage Description" key must be present in the app plist
 - the app must construct the CMTSVRDetector object from the appDidFinishLaunchWithOptions: method, so that
 the SVR detector can start tracking locations.
 
 @since Available since 1.1.0
 
 */
@interface CMTSVRDetector : NSObject<CLLocationManagerDelegate>

/**
 * The delegate object of the CMTSVRDetector. Set this to an object that you wish to get
 * updates when an SVR tag is discovered or disappears
 
 @since Available since 1.1.0
 */
@property (weak, nonatomic, nullable) NSObject<CMTSVRDetectorDelegate> *delegate;

/**
 *  The associated user
 *  @since Available since 1.2.1
 */
@property (readonly, nonatomic, nullable, weak) CMTUser *user;

/**
 *  Whether scanning is active or not
 *  @since Available since 1.3.11
 */
@property BOOL scanningForBeacons;

/**
 App developers should not attempt to create an instance of `CMTSVRDetector` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Create a new instance of CMTSVRDetector.  Monitoring must be started by calling startMonitoringForSVRBeacons.

 @param user A `CMTUser` object
 @return A `CMTSVRDetector` object
 @since Available since 1.1.0
 */
-(nonnull instancetype)initWithUser:(CMTUser * _Nonnull)user NS_DESIGNATED_INITIALIZER;

/** 
 * Start monitoring for SVR beacons;  the user supplied to initWithUser must be an authenticated user.
 @since Available since 1.3.10
 */
-(void) startMonitoringForSVRBeacons;
    
/** 
 * Stop SVR detection 
 
 @since Available since 1.3.10
 */
-(void)stopMonitoringForSVRBeacons;


@end
