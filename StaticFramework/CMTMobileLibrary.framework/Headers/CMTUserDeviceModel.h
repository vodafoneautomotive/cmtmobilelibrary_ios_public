//
// CMTUserDeviceModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*! Data model to describe device information stored in the server
 *  @since Available since 1.0.0
 */
@interface CMTUserDeviceModel : CMTMTLModel<CMTMTLJSONSerializing>

/*!
 * Globally unique device identifier
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSString *deviceId;

/*!
 *  Device nickname
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSString *deviceNickname;

/*!
 * Date when a server request was made form this device
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSDate *lastRequestDate;

/*!
 * Date when device was authorized
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSDate *authorizedDate;

/*!
 *  Whether device is authorized on the current backend (Boolean)
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *isAuthorized;

/*!
 *  Whether the push token has been sent to the server (Boolean)
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *hasPushToken;

/*!
 *  Whether server will send notification for friend invites (Boolean)
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *useNotificationsForInvites;

/*!
 *  Whether server will send notification for new results (Boolean)
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *useNotificationsForNewResults;

/*!
 *  Whether server will send notification for badges updates (Boolean)
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *useNotificationsForBadges;

/*!
 *  Whether server believes that app will upload telematics data using WiFi only (Boolean)
 *
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *uploadWiFiOnly;

@end

