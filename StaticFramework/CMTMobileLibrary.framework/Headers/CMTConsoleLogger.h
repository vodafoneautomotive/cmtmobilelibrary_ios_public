//
// CMTConsoleLogger.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CMTConsoleLogError(fmt, ...)    [[CMTConsoleLogger defaultLogger] log:CMTConsoleLogLevelError format:fmt, ##__VA_ARGS__]
#define CMTConsoleLogWarn(fmt, ...)     [[CMTConsoleLogger defaultLogger] log:CMTConsoleLogLevelWarn format:fmt, ##__VA_ARGS__]
#define CMTConsoleLogInfo(fmt, ...)     [[CMTConsoleLogger defaultLogger] log:CMTConsoleLogLevelInfo format:fmt, ##__VA_ARTS__]
#define CMTConsoleLogDebug(fmt, ...)    [[CMTConsoleLogger defaultLogger] log:CMTConsoleLogLevelDebug format:fmt, ##__VA_ARGS__]
#define CMTConsoleLogVerbose(fmt, ...)  [[CMTConsoleLogger defaultLogger] log:CMTConsoleLogLevelVerbose format:fmt, ##__VA_ARGS__]


/**
 Possible values to assign to the log level.
 
 @since Available since 1.9.2
 */
typedef NS_ENUM(NSInteger, CMTConsoleLogLevel) {
    /// Disable console logging (Available since 1.9.2)
    CMTConsoleLogLevelNone = 0,
    /// Only log errors to the console (Available since 1.9.2)
    CMTConsoleLogLevelError = 1,
    /// Log warnings and errors to the console (Available since 1.9.2)
    CMTConsoleLogLevelWarn = 2,
    /// Log info and above to the console (Available since 1.9.2)
    CMTConsoleLogLevelInfo = 3,
    /// Log debug level and above to the console (Available since 1.9.2)
    CMTConsoleLogLevelDebug = 4,
    /// Log as much as possible to the console (Available since 1.9.2)
    CMTConsoleLogLevelVerbose = 5
};

/**
 CMTConsoleLogger is a utility class that handles logging to the console. Changing log levels during
 development may make debugging easier. App developers must not refer to the defaultLogger until after the
 CMTMobileLibrary has been initialized.
 
 You can change the log level by calling:
 
    [CMTConsoleLogger defaultLogger].logLevel = CMTConsoleLogLevelNone;
    
 The following logging level options are available:
 
    CMTConsoleLogLevelNone (This is the default.)
    CMTConsoleLogLevelError
    CMTConsoleLogLevelWarn
    CMTConsoleLogLevelInfo
    CMTConsoleLogLevelDebug
    CMTConsoleLogLevelVerbose

    @since Available since 1.9.2
    @note Your must set the log level to `None` before publishing to the App Store.
 */
@interface CMTConsoleLogger : NSObject

/** @name Properties */
/**
 The log level setting. The default value is `None`.
 @since Available since 1.9.2
 */
@property (atomic, assign) CMTConsoleLogLevel logLevel;

/** @name Methods */
/**
 Returns the shared logger object.
 @since Available since 1.9.2
 @return The shared logger object.
 */
+ (nullable instancetype)defaultLogger;

/**
 Prints out the formatted logs to the console. You may also use the following predefined shorthand methods:
    CMTConsoleLogError(fmt, ...)
    CMTConsoleLogWarn(fmt, ...)
    CMTConsoleLogInfo(fmt, ...)
    CMTConsoleLogDebug(fmt, ...)
    CMTConsoleLogVerbose(fmt, ...)

 @since Available since 1.9.2
 @param logLevel The level of this log.
 @param fmt      The formatted string to log.
 @param ...      Argument list
 */
- (void)log:(CMTConsoleLogLevel)logLevel
     format:(NSString * _Nonnull)fmt, ... NS_FORMAT_FUNCTION(2, 3);

@end
