//
// NSDictionary+CMTMTLJSONKeyPath.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/// CMTMTLJSONKeyPath category for NSDIctionary
@interface NSDictionary (CMTMTLJSONKeyPath)

/**
 Looks up the value of a key path in the receiver.

 @param JSONKeyPath The key path that should be resolved. Every element along this key path needs to be an instance of NSDictionary for the resolving to be successful.
 @param success If not NULL, this will be set to a boolean indicating whether the key path was resolved successfully.
 @param error If not NULL, this may be set to an error that occurs during resolving the value.

 @return the value for the key path which may be nil. Clients should inspect the success parameter to decide how to proceed with the result.
*/
- (id)cmt_mtl_valueForJSONKeyPath:(NSString *)JSONKeyPath success:(BOOL *)success error:(NSError **)error;

@end
