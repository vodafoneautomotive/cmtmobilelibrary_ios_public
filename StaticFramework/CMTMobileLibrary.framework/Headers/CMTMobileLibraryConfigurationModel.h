//
// CMTMobileLibraryConfigurationModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/**
 Error domain for `CMTMobileLibraryConfigurationModel`
 @deprecated Deprecated in 3.0.0, app developers should use `CMTMobileLibraryCoreErrorDomain` instead.
 */
extern NSString * _Nonnull const CMTMobileLibraryConfigurationModelErrorDomain __attribute__((deprecated("Deprecated in 3.0.0, app developers should use CMTMobileLibraryCoreErrorDomain instead")));

/**
 `CMTMobileLibraryConfigurationModel` Class offers APIs to override default configuration for CMTMobileLibrary
 @since Available since 1.9.0
 */
@interface CMTMobileLibraryConfigurationModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Minimum battery percentage for which drives will start recording, default = @"10" 
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *minBatteryLevelStartDrive;

/*! A comma-separated list of iBeacon UUIDs strings that the library will listen for
 * @discussion This is only useful for tag users.
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSArray <NSString *> *bluetoothRegionUUIDList;

/*! Number of days to keep trip in local database, default = @"30"
 * @discussion It is possible to set this to a negative value, in which case
 * a trip will be kept forever. This is not recommended as this will negatively impact
 * trip list retrieval from the SDK
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *numberDaysToKeepTrip;

/*! Enable developers to force recording trips with specific start reasons, default is @"false". Set to @"true" to enable
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *allowDevelopersToStartTripsForTesting;

/*! Enable logging of phone telematics, default is @"true". Set to @"false" to disable. 
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *enablePhoneTelematicsLogging;

/*! Enable logging of phone telematics from tag, default is @"true". Set to @"false" to disable.
 * @discussion Only applies to tag_users
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *enableTagTelematicsLogging;

/*! Post tag's location, default is @"true". Set to @"false" to disable.
 *  @discussion Only applies to tag_users
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *enablePostTagLocation;

/*! Enable impact alerts, default is @"true".  Set to @"false" to disable. 
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *enableImpactAlert;

/*! Enable driver location posts, default is @true". Set to @"false" to disable.
 *  @since Available since 1.9.0
 */
@property (nonatomic, copy, nullable) NSString *enablePhoneOnlyTracking;


/*! Custom configuration that can passed from the server to the app
 * @discussion The value passed here is opaque to the SDK
 * @since Available since 1.9.0
 */
@property (readonly, copy, nullable) NSString *customConfiguration;

@end
