//
// CMTBadgeModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 * Simple class to provide information about a badge
 * @since Available since 0.1.0
 * @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTBadgeModel : CMTMTLModel <CMTMTLJSONSerializing>

/** @name Properties */
/*!
 * String to append to strings "BADGE_TITLE_", "BADGE_SUMMARY_", or "BADGE_ACHIEVED_ICON_" as keys into marketing materials
 * Use badgeTitle, badgeSummary, badgeAchievedIconUrl for convenience.
 * @since Available since 0.1.0
 */
@property (nullable) NSString *badgeHandle;

/*! Integer value between 0 and 100
 *  @since Available since 0.1.0
 */
@property (readonly,nullable) NSNumber *progress;

/*! Date when badge was awarded or nil if not awarded
 *  @since Available since 0.1.0
 */
@property (readonly,nullable) NSDate *dateCompleted;

/*! Date when badge was claimed
 *  @since Available since 0.2.4
 */
@property (readonly,nullable) NSDate *claimDate;

/*! Motivational or congratulatory message, as appropriate
 *  @since Available since 0.1.0
 */
@property (readonly,copy,nullable) NSString *badgeText;

/*! Badge title or name
 *  @since Available since 0.2.4
 */
@property (readonly,copy,nullable) NSString *badgeTitle;

/*! Summary of how to achieve badge
 *  @since Available since 0.2.4
 */
@property (readonly,copy,nullable) NSString *badgeSummary;

/*! URL pointing to the image used for the badge
 *  @since Available since 0.2.4
 */
@property (readonly,nullable) NSURL *badgeAchievedIconUrl;

@end
