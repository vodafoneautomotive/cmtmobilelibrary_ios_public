//
// CMTImpactUploader.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTImpactAlertNotificationDelegate.h"

@class CMTUser;

/**
 Name of notification for sending impact notification to app.
 @since Available since 9.3.0
*/
extern NSString * _Nonnull const CMTImpactObjectReceivedNotification;

/**
 Key used in the userInfo dictionary object passed in by the NSNotification object.
 The value of the key is the NSData representing the impact object JSON.
 @since Available since 9.3.0
*/
extern NSString * _Nonnull const CMTImpactObjectKey;

/**
Class to upload impact alerts.
@since Available since 9.0.0
*/
@interface CMTImpactUploader : NSObject

/**
App developers should not attempt to create an instance of `CMTImpactUploader` with default method.
*/
- (nonnull instancetype)init NS_UNAVAILABLE;
+ (nonnull instancetype)new NS_UNAVAILABLE;

/**
 Reference to delegate to notify of impacts
 @discussion Originally in CMTImpactDataManager, moved to CMTImpactUploader in 9.0.0.
 @since Available since 9.0.0
 @deprecated Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification instead for alerting the app to impacts.
 */
@property (weak) NSObject<CMTImpactAlertNotificationDelegate> * _Nullable impactAlertNotificationDelegate __attribute__((deprecated("Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification instead for alerting the app to impacts.")));

@end

