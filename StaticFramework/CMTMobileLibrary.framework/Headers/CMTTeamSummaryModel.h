//
// CMTTeamSummaryModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTTeamSummaryTripMetricsModel.h"


/**
 Data model for storing data related to team summary info
 @since Available since 1.7.0
 @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.")))
@interface CMTTeamSummaryModel : CMTMTLModel<CMTMTLJSONSerializing>

/**
 Team name
 @since Available since 1.7.0
 */
@property (nonatomic,nullable) NSString *name;

/**
 Overall team driving score
 @since Available since 1.7.0
 */
@property (nonatomic,nullable) NSNumber *score;

/**
 Score breakdown description. The contents are HTML.
 @since Available since 1.7.0
 */
@property (nonatomic,nullable) NSString *scoreCalculationDescriptionHTML;

/*!
 Team trip metric
 @since Available since 1.7.0
 */
@property (nonatomic,nullable) CMTTeamSummaryTripMetricsModel *tripMetrics;

@end
