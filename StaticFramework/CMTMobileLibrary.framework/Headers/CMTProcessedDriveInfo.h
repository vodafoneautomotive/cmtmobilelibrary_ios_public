//
// CMTProcessedDriveInfo.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTBaseDriveInfo.h"

/**
 *  Class that describes properties of a processed trip.
 
 @since Available since 1.0.0
 */
@interface CMTProcessedDriveInfo : CMTBaseDriveInfo<CMTMTLJSONSerializing>

/*! The trip's star rating, represented as a floating point number.
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRating;

/*! The trip's acceleration score, represented as a star rating (floating point number). 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingAccel;

/*! The trip's braking score, represented as a star rating (floating point number). 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingBrake;

/*! The trip's turning score, represented as a star rating (floating point number). 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingTurn;

/*! The trip's speeding score, represented as a star rating (floating point number). 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingSpeeding;

/*! The trip's phone motion score, represented as a star rating (floating point number). 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingPhoneMotion;

/*! The trip's road type score, represented as a star rating (floating point number). 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingRoads;

/*!
 The trip's star rating ignoring distraction ("passenger star rating") as a star rating (floating point number).
 @discussion This computed star rating ignores distraction because the assumption is that the phone that recorded the trip belongs to the passenger and not the driver. When a trip is labeled as a "passenger" trip, then starRatingIgnoringDistraction should be equal to the starRating.
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingIgnoringMotion;

/*! Custom trip score as a floating point number (not in DriveWell) 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *tripScore;

/*!
 Custom trip score ignoring distraction ("passenger score") as a floating point number

 @discussion This computed score ignores distraction because the assumption is that the phone that recorded the trip belongs to the passenger and not the driver. When a trip is labeled as a "passenger" trip, then the tripScore should be equal to the tripScoreIgnoringDistraction.

 @since Available since 1.9.5
 */
@property (nullable) NSNumber *tripScoreIgnoringDistraction;

/*! Custom night driving score as a floating point number (not in DriveWell) 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingNight;

/*! Custom night driving score as a floating point number (not in DriveWell) 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingSmoothness;

/*! Custom awareness score as a floating point number (not in DriveWell) 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *starRatingAwareness;

/*! `isNight` is true the trip was done during nighttime hours (Boolean) 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *isNight;

/*! `isPrimaryDriver` is true if the trip was performed by the primary driver (Boolean) 
 
 @since Available since 1.0.0
 */
@property (nullable) NSNumber *isPrimaryDriver;

/*! The server determined classification label of the trip 
 
 @since Available since 1.0.0
 */
@property CMTTransportModeType classificationLabel;

@end
