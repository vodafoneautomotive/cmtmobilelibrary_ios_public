//
// CMTBaseDataProvider.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 *  Base class for data providers that fetch and update backend data
 *  @since Available since 0.1.0
 */
@interface CMTBaseDataProvider : NSObject

/*! True if configuration has been fetched and committed to disk
 *  @since Available since 1.3.20
 */
@property (readonly) BOOL hasLocalCache;

/*! Time when the data was last fetched from the server
 *  @since Available since 0.1.0
 */
@property (nonatomic,nonnull) NSDate *lastFetchDate;

/*! Raw dictionary, the response from the server represented as a dictionary
 *  @since Available since 0.1.0
 */
@property (readonly,nonnull) NSDictionary *rawDictionary;

/*! queue on which asynchronous operations will occur
 *  @since Available since 0.1.0
 */
@property (strong,nonnull) NSOperationQueue *fetchQueue;

@end
