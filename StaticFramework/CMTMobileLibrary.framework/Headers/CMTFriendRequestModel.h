//
// CMTFriendRequestModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 *  For handling incoming friend requests
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendRequestModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Email address of person inviting you
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *fromEmail;

/*! Name of person inviting
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *fromName;

/*! Date of invitation
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSDate* inviteDate;

/*! Integer identifier for this invitation
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSNumber *inviteId;

@end
