//
// CMTTagActivationDelegate.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMTTagActivationManager;
@class CMTVehicle;
@class CMTTag;


/**
 Possible indication capabilities of the tag

 - CMTTagIndicationCapabilityNone: The tag cannot indicate
 - CMTTagIndicationCapabilityBlink: The tag can blink
 - CMTTagIndicationCapabilityBeep: The tag can beep
 
 @since Available since 4.0.0
 */
typedef NS_OPTIONS(NSUInteger, CMTTagIndicationCapability) {
    /// The tag cannot indicate (Available since 4.0.0)
    CMTTagIndicationCapabilityNone      = 0,
    /// The tag can blink (Available since 4.0.0)
    CMTTagIndicationCapabilityBlink     = 1 << 0,
    /// The tag can beep (Available since 4.0.0)
    CMTTagIndicationCapabilityBeep      = 1 << 1
};


/**
 Object conforming to the `CMTTagActivationDelegate` will get informed about successful tag activation or activation errors
 
 @since Available since 4.0.0
 */
@protocol CMTTagActivationDelegate <NSObject>

/**
 Callback when tag activation is completed. App developers must call -[CMTTagActivationManager finishLinkingTag]
 in the handler or before attempting another tag activation.
 
 @discussion This method is invoked after a successful [CMTTagActivationManager confirmLinkingTag] call.
 
 @param tagActivationManager Reference to tag activation manager object
 @param tag                  Information about `CMTTag` being linked
 @param vehicle              Information about the `CMTVehicle` being linked
 
 @since Available since 4.0.0
 */
-   (void)tagActivationManager:(CMTTagActivationManager * _Nonnull)tagActivationManager
         didCompleteLinkingTag:(CMTTag * _Nonnull)tag
                     toVehicle:(CMTVehicle * _Nonnull)vehicle;

/**
 Callback when tag activation fails. App developers must call -[CMTTagActivationManager finishLinkingTag] in the
 handler or before attempting another tag activation.

 @param tagActivationManager Reference to tag activation manager object
 @param tag                  Information about the `CMTTag` being linked
 @param vehicle              Information about the `CMTVehicle` being linked
 @param error                Reason for the failure
 
 @since Available since 4.0.0
 */
-   (void)tagActivationManager:(CMTTagActivationManager * _Nonnull)tagActivationManager
              didFailToLinkTag:(CMTTag * _Nullable)tag
                     toVehicle:(CMTVehicle * _Nonnull)vehicle
                         error:(NSError * _Nullable)error;

@optional
/**
 Callback to indicate what the tag can do to indicate that it is being activated. Some possibilities include blinking and beeping.
 Implementing this method is optional.

 @discussion This method is invoked after a successful call to [CMTTagActivationManager indicateLinkingTagWithIndicationCapability:].
 If this method is defined in the object conforming to this protocol, it will be invoked by [CMTTagActivationManager beginLinkingTagWithMacAddress:toVehicle:error] once tag linking completes.
 If this method is not defined in the object conforming to this protocol, the delegate method [CMTTagActivationDelegate tagActivationManager:didCompleteLinkingTag:toVehicle:] will be invoked instead once tag linking completes.
 
 @param tagActivationManager Reference to tag activation manager object
 @param tag                  Information about the `CMTTag` being linked
 @param capabilities         The indication capabilities of the tag, i.e., can the tag blink? can the tag beep?
 
 @since Available since 4.0.0
 */
-   (void)tagActivationManager:(CMTTagActivationManager * _Nonnull)tagActivationManager
         canIndicateLinkingTag:(CMTTag * _Nonnull)tag
    withIndicationCapabilities:(CMTTagIndicationCapability)capabilities;

@end
