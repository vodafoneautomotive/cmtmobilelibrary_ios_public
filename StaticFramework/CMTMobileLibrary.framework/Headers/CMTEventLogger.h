//
// CMTEventLogger.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class CMTFilterEngineCore;
NS_ASSUME_NONNULL_BEGIN

/*!
 * Used to insert events into the trip data files for analysis by CMT's backend servers
 *  @since Available since 0.1.0
 */
@interface CMTEventLogger : NSObject <CLLocationManagerDelegate>

/**
 App developers should not attempt to create an instance of `CMTEventLogger` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Create an instance of a CMTEventLogger object
 *  @since Available since 0.1.0
 *  @param filterEngine CMTFilterEngineIOS object
 *  @return An instance of type CMTEventLogger
 */
-(instancetype) initWithFilterEngine:(CMTFilterEngineCore *)filterEngine NS_DESIGNATED_INITIALIZER;

/*!
 *  Inserts a time-stamped event with identifier `tupleIdentifier` into the trip file
 *  @since Available since 1.2.13
 *  @param tupleIdentifier String used to identify a particular event
 */
-(void)logEventTuple:(NSString *)tupleIdentifier;

/*!
 *  Insert a time-stamped event with identifier `tupleIdentifier` into the trip file.
 *  Any fields in the dictionary are also injected into the trip file as a JSON string.
 *  @since Available since 1.2.13
 *  @param tupleIdentifier String used to identify a particular event
 *  @param dict            Additional key-value pairs that can be used to provide more information about the event
 */
-(void)logEventTuple:(NSString *)tupleIdentifier extraFields:(NSDictionary *)dict;


-(void)logPushNotificationPermissionStatus:(NSString*)authStatus;
@end
NS_ASSUME_NONNULL_END
