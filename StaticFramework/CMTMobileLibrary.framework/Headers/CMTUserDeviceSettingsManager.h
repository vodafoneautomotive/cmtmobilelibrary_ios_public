//
// CMTUserDeviceSettingsManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTUserBaseDataProvider.h"
#import "CMTUserDeviceModel.h"
#import "CMTUserDeviceUpdateModel.h"

/*! Error domain for CMTUserDeviceSettingsManager errors
 * @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTUserDeviceSettingsManagerErrorDomain;

/**
 CMTUserDeviceSettingsManager error
 
 - CMTUserDeviceSettingsManagerErrorBackgroundUpdateStoppedAppLeavingForeground: Error type if background update is cancelled due to app leaving foreground (Available since 1.5.0)
 - CMTUserDeviceSettingsManagerErrorBackgroundUpdateCancelled: Error type if user of background update cancels the operation (Available since 1.0.0)
 
 @since Available since 1.0.0
 */
typedef  NS_ENUM(NSInteger, CMTUserDeviceSettingsManagerError) {
    /// Error type if background update is cancelled due to app leaving foreground (Available since 1.5.0)
    CMTUserDeviceSettingsManagerErrorBackgroundUpdateStoppedAppLeavingForeground = -2,
    /// Error type if user of background update cancels the operation (Available since 1.0.0)
    CMTUserDeviceSettingsManagerErrorBackgroundUpdateCancelled = -1
};

/*!
 *  Use to fetch device information from the server and to update device information
 * @since Available since 1.0.0
 */
@interface CMTUserDeviceSettingsManager : CMTUserBaseDataProvider

/*!
 * Returns the list of devices for this user
 * @since Available since 1.0.0
 */
@property (readonly,atomic,nullable) NSArray <CMTUserDeviceModel *> *userDevices;

/**
 App developers should not attempt to create an instance of `CMTUserDeviceSettingsManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  <i>Synchronously</i> get list of devices that user has authorized from server
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return Array of CMTUserDeviceModel objects. Each object represents a device.
 *
 *  @throws (Swift only) NSError
 *
 * @since Available since 1.0.0
 */
-(nullable NSArray <CMTUserDeviceModel *>*) loadUserDevicesWithError:(NSError * _Nullable __autoreleasing*_Nullable)error;

/*!
 *  <i>Asynchronously</i> fetch the list of user devices from the server
 *
 *  @param handler Handler that will be called with the array of user devices, whether the call succeeded and a non-nil error parameter
 *  if there are no errors
 *
 *  @return NSOperation used to make the fetch
 *
 *  @since Available since 1.0.4
 *
 *  @deprecated Deprecated since 9.0.0, use loadUserDevicesWithHandler:/loadUserDevices(handler:) instead.
 */
-(nonnull NSOperation *)loadUserDevicesInBackgroundWithCompletionHandler:(void (^_Nullable)(NSArray <CMTUserDeviceModel *> * _Nullable userDevices,                                                                                                                 BOOL success, NSError * _Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use loadUserDevicesWithHandler:/loadUserDevices(handler:) instead ")));

/*!
 *  <i>Asynchronously</i> fetch the list of user devices from the server
 *
 *  @param handler Handler that will be called with the array of user devices, whether the call succeeded and a non-nil error parameter
 *  if there are no errors
 *
 *
 *  @since Available since 9.0.0
 */
-(void)loadUserDevicesWithHandler:(void (^_Nullable)(NSArray <CMTUserDeviceModel *> * _Nullable userDevices,
                                                     BOOL success, NSError * _Nullable error)) handler;

/*!
 *  <i>Synchronously</i> update information about the device to the server
 *
 *  @param deviceUpdateModel The data model to use to update the server's database about this device
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return Most recently fetched array of CMTUserDeviceModel objects.
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 1.0.0
 */
-(nullable NSArray <CMTUserDeviceModel *>*)updateUserDevice:(CMTUserDeviceUpdateModel *_Nonnull)deviceUpdateModel error:(NSError * _Nullable __autoreleasing* _Nullable) error;

/**
 *  <i>Asynchronously</i> update information about the device to the server
 *
 *  @param deviceUpdateModel The data model to use to update the server's database about this device
 *  @param handler           Handler that will be called after the update has been completed. The handler will be provided with
 *                           the array of user devices, whether update succeeded, and a non-nil error parameter if an error occurs.
 *
 * @return  NSOperation used to make the fetch
 *
 * @since Available since 1.0.0
 *
 * @deprecated Deprecated since 9.0.0, use updateUserDevice:handler:/updateUserDevice(_:handler:) instead.
 */
-(nonnull NSOperation *)updateUserDeviceInBackground:(CMTUserDeviceUpdateModel *_Nonnull)deviceUpdateModel
                           completionHandler:(void (^_Nullable)(NSArray<CMTUserDeviceModel *> * _Nullable userDevices, BOOL success, NSError * _Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use updateUserDevice:handler:updateUserDevice(_:handler:) instead ")));

/**
 *  <i>Asynchronously</i> update information about the device to the server
 *
 *  @param deviceUpdateModel The data model to use to update the server's database about this device
 *  @param handler           Handler that will be called after the update has been completed. The handler will be provided with
 *                           the array of user devices, whether update succeeded, and a non-nil error parameter if an error occurs.
 *
 *
 * @since Available since 9.0.0
 */
-(void)updateUserDevice:(CMTUserDeviceUpdateModel *_Nonnull)deviceUpdateModel
                handler:(void (^_Nullable)(NSArray<CMTUserDeviceModel *> * _Nullable userDevices, BOOL success, NSError * _Nullable error)) handler;

/*!
 *  <i>Asynchronously</i> update information about the device to the server with retry attempts. Typically, a developer would use
 *  this method to update the server database as a result of actions that don't involve the UI. Note that when the app is no 
 *  longer in the foreground, the retries will stop (to avoid being killed by OS for staying awake in the background too long).
 *
 *  @param deviceUpdateModel The data model to use to update the server's database about this device.
 *  @param maxRetries        The maximum number of times to retry the server update. Value of 0 means keep retrying forever.
 *  @param sleepDuration     Time in seconds between retry attempts. The minimum is 0.5 seconds.
 *  @param handler            Handler that will be called after the update has been completed. The handler will be provided with
 *                           the array of user devices, whether update succeeded, and a non-nil error parameter if an error occurs.
 *
 * @since Available since 1.0.0
 *
 * @deprecated Deprecated since 9.0.0, use updateUserDevice:withRetries:sleepDurationBetweenRetries:handler:/updateUserDevice(_:withRetries:sleepDurationBetweenRetries:handler:) instead.
 */
-(nonnull NSOperation *) updateUserDeviceInBackground:(CMTUserDeviceUpdateModel *_Nonnull)deviceUpdateModel
                                          withRetries:(NSUInteger)maxRetries
                          sleepDurationBetweenRetries:(NSTimeInterval)sleepDuration
                                    completionHandler:(void (^_Nullable)(NSArray<CMTUserDeviceModel *> * _Nullable userDevices, NSUInteger totalTries, BOOL success, NSError * _Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use updateUserDevice:handler:updateUserDevice(_:handler:) instead ")));

/*!
 *  Cancel all operations that haven't run
 *
 * @since Available since 1.0.0
 */
-(void) cancelAllOperations;

@end
