//
// CMTUtils.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


extern NSString * _Nonnull const CMTUtilsErrorDomain;
extern uint64_t const CMTUtilsUnknownTagId;

/**
 Errors that can be returned by CMTUtils methods
 
 @since Available since 3.0.0
 */
typedef NS_ENUM(NSInteger, CMTUtilsErrorType) {
    /// The device does not have a motion coprocessor (Available since 3.0.0)
    CMTUtilsRequestMotionActivityAccessNoActivityAvailableError = -1
};

/*! Various helper methods used throughout CMT code
 * @since Available since 1.3.0
 */
@interface CMTUtils : NSObject


/**
 Convert mac address provided as an array of bytes into a string
 
 @param MAC tag mac address as C array of 6 bytes
 @return tag mac address as a hex string with format XX:XX:XX:XX:XX:XX, hex digits are lowercase
 
 @since Available since 1.3.0
 */
+(nonnull NSString *) macAddrByteArrayToString:(uint8_t[_Nonnull 6])MAC;

/**
 Convert mac address provided as unsigned 64-bit integer into a string
 
 @param tagid unsigned 64-bit integer
 @return tag mac address as a hex string with format XX:XX:XX:XX:XX:XX, hex digits are lowercase
 
 @since Available since 1.9.0
 */
+(nonnull NSString *) macAddrUint64ToString:(uint64_t)tagid;

/**
 Convert mac address provided as a string into an unsigned 64-bit integer

 @param macString tag mac address as a hex string with format XX:XX:XX:XX:XX:XX
 @return unsigned 64-bit integer
 
 @since Available since 1.3.0
 */
+(uint64_t) macAddrStringToUint64:(NSString *_Nullable) macString;

/**
 Convert mac address provided as an array of bytes into an unsigned 64-bit integer

 @param MAC tag mac address as C array of 6 bytes
 @return tag mac address as a hex string with format XX:XX:XX:XX:XX:XX, hex digits are lowercase
 
 @since Available since 1.3.0
 */
+(uint64_t) macAddrByteArrayToUint64:(uint8_t[_Nonnull 6]) MAC;

/**
 Extract the tag mac address from the beacon as return as a string

 @param beacon CLBeacon object
 @return tag mac address as a hex string with format XX:XX:XX:XX:XX:XX, hex digits are lowercase
 
 @since Available since 1.9.0
 */
+(nonnull NSString *)tagMacAddressFromBeacon:(CLBeacon *_Nonnull)beacon;

/**
 Convert major and minor numbers into a tag mac address

 @param major NSNumber (unsigned integer) from major property from CLBeacon
 @param minor NSNumber (signed integer) from minor project from CLBeacon
 @return tag mac address as a hex string with format XX:XX:XX:XX:XX:XX, hex digits are lowercase
 
 @since Available since 1.9.0
 */
+(nonnull NSString *)tagMacAddressFromMajor:(NSNumber *_Nonnull)major minor:(NSNumber *_Nonnull)minor;

/**
 Get the current device battery level

 @discussion This method returns the current battery level between 0 and 1.0 where 1.0 represents a fully charged device.
             This method is a wrapper around the method: [[UIDevice currentDevice] batteryLevel]
 
 @return a value between 0 and 1.0 or -1.0 if the battery device state is  UIDeviceBatteryStateUnknown
 
 @since Available since 1.9.0
 */
+(float)currentDeviceBatteryLevel;

/**
 Request motion activity access.

 @param handler Provide a handler to determine error conditions that may occur as a result of the user denying the app
    access to the Motion processor. If the error value is nil, authorization was successful and app developers can enable
    the trip detector to use the motion processor to assist with trip detection, as follows.
 
        [CMTUtils requestMotionActivityAccessWithHandler:^(NSError * _Nullable error) {
            NSLog(@"error = %@", [error description]);
            if(error == nil) {
                [CMTUser currentUser].tripManager.tripDetector.enableMotionActivitySensing = YES;
            }
        }];
 
 Otherwise, do not enableMotionActivitySensing. If the motion coprocessor is not available, then an error will be returned.
 
 @discussion If the user has never given the app motion activity access before, then when this method is called, the user
 will be prompted to give motion activity access. This method uses the `-[CMMotionActivityManager queryActivityStartingFromDate:toDate:toQueue:withHandler:]` method. In iOS 11, one can use the `+[CMMotionActivityManager authorizationStatus]` method to determine if the user has given the app authorization to access the motion data. In iOS 10 and blow, there is no such method that can be called.
 @since Available since 3.0.0
 */
+(void)requestMotionActivityAccessWithHandler:(void (^_Nullable)(NSError * _Nullable error))handler;

@end
