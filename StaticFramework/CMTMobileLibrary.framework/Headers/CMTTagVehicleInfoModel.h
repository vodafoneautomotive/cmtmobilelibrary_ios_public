//
// CMTTagVehicleInfoModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"


/**
 Class to store vehicle information associated with a tag
 @since Available since 0.3.3
 */
@interface CMTTagVehicleInfoModel : CMTMTLModel<CMTMTLJSONSerializing>

/** 
 Tag mac address expressed as a uint64_t (unsigned long long)
 @since Available since 0.3.3
 */
@property (nonatomic,nullable) NSNumber *tagId;

/** 
 A name for the tag
 @since Available since 0.3.3
 */
@property (nonatomic,nullable) NSString  *tagName;

/** 
 A vehicle id (unsigned long long), typically set by the server
 @since Available since 1.2.8
 */
@property (nonatomic,nullable) NSNumber *vehicleId; // uint64_t

/** 
 An identifier indicating what icon to associate with this vehicle, typically set by the server
 @since Available since 0.3.3
 */
@property (nonatomic,nullable) NSNumber  *vehicleIconId;

/** 
 The make of the vehicle, typically set by the server
 @since Available since 0.3.3
 */
@property (nonatomic,nullable) NSString  *make;

/** 
 The model of the vehicle, typically set by the server
 @since Available since 0.3.3
 */
@property (nonatomic,nullable) NSString  *model;

/** 
 The vehicle registration, typically set by the server
 @since Available since 0.3.3
 */
@property (nonatomic,nullable) NSString  *registration;

/** 
 Vehicle identification
 @since Available since 1.6.0
 */
@property (nonatomic,nullable) NSString *vin;

/** True if trips from this tag should be visible to the user
 @since Available since 0.3.3
 */
@property (nonatomic) BOOL      tripsVisibleToUser;

/**
 Battery level in tag as computed by server
 @since Available since 1.6.0
 */
@property (nonatomic) NSInteger batteryLevel;

/**
 True if this vehicle has a tag linked to it
 @since Available since 1.2.8
 */
@property (nonatomic, readonly, getter=isLinked) BOOL linked;

@end
