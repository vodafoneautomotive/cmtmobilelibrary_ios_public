//
// NSError+CMTMTLModelException.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/// CMTMTLModelException category for NSError
@interface NSError (CMTMTLModelException)

/**
 Creates a new error for an exception that occurred during updating an
 CMTMTLModel.

 @param exception The exception that was thrown while updating the model. This argument must not be nil.

 @return an error that takes its localized description and failure reason
 from the exception.
 */
+ (instancetype)cmt_mtl_modelErrorWithException:(NSException *)exception;

@end
