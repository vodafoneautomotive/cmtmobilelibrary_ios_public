//
// CMTFriendEmailInviteModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTDataTypes.h"

/*!
 *  If this model is present in the response, the app can use the contents to send this invitation
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendEmailInviteModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Email address of person
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *email;

/*! Body of message to be sent to person
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *body;

/*! Subject of message to be sent to person
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *subject;

/*! Format of message to be sent by person
 *  @since Available since 0.1.0
 */
@property (nonatomic) CMTEmailFormat format;

@end
