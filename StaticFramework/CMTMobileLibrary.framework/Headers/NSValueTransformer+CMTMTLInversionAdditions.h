//
// NSValueTransformer+CMTMTLInversionAdditions.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/// CMTMTLInversionAdditions category for NSValueTransformer
@interface NSValueTransformer (CMTMTLInversionAdditions)

/**
 Flips the direction of the receiver's transformation, such that
 -transformedValue: will become -reverseTransformedValue:, and vice-versa.

 The receiver must allow reverse transformation.

 @return an inverted transformer.
*/
- (NSValueTransformer *)cmt_mtl_invertedTransformer;

@end
