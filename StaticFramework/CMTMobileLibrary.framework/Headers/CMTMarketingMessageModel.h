//
// CMTMarketingMessageModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 * Used to de-serialize the JSON sent in the marketing materials. Generally, only one of the four 
 * properties will be populated. For text that needs to be displayed, the language will appear in the locale of the phone.
 *
 *  @since Available since 1.0.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTMarketingMessageModel : CMTMTLModel<CMTMTLJSONSerializing>

/*!
 *  If not nil, text to display
 *  @since Available since 1.0.0
 */
@property (readonly,nullable) NSString *text;

/*!
 *  If not nil, raw html
 *  @since Available since 1.9.0
 */
@property (readonly,nullable) NSString *htmlString;

/*!
 *  If not nil, the URL
 *  @since Available since 1.0.0
 */
@property (readonly,nullable) NSURL *url;

/*!
 *  An array of NSString objects to display.
 *  @since Available since 1.0.0
 */
@property (readonly,nullable) NSArray <NSString *> *textArray;

@end
