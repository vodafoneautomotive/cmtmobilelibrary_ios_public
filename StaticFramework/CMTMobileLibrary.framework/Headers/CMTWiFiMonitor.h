//
// CMTWiFiMonitor.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Describes the possible state of the device's WiFi connection
 
 @since Available since 1.0.0
 */
typedef NS_ENUM(NSInteger, CMTWiFiStatus) {
    /// WiFi connection is off (Available since 1.0.0)
    CMTWiFiStatusWiFiOff = 0,
    /// WiFi connection is on (Available since 1.0.0)
    CMTWiFiStatusWiFiOn,
    /// WiFi connection is in an unknown state (Available since 1.0.0)
    CMTWiFiStatusWiFiUnknown
};

/** Data model to contain information about the WiFi connection
 * @since Available since 1.0.0
 */
@interface CMTWiFiInfo : NSObject

/** Current WiFi connection state
 * @since Available since 1.0.0
 */
@property CMTWiFiStatus connectionState;

/** If connected, the SSID of the access point that the device is connected to and nil otherwise
 * @since Available since 1.0.0
 */
@property (nullable) NSString *ssid;

/** If connected, the BSSID of the access point that the device is connected to and nil otherwise
 *  @since Available since 1.0.0
 */
@property (nullable) NSString *bssid;

@end

/**
 * Methods to log current WiFi network and test if WiFi is enabled
 * @since Available since 1.0.0
 */
@interface CMTWiFiMonitor : NSObject

/**
 * Report status of the WiFi connection.
 *
 * @return CMTWiFiStatusWiFiON if WiFi is enabled, CMTWiFiStatusWiFiOff if WiFi is disabled, and CMTWiFiStatusWiFiUnknown otherwise.
 * @since Available since 1.0.0
 */
+(CMTWiFiStatus) isWiFiEnabled;

/**
 * Get state of WiFi connection
 *
 * @return A CMTWiFiInfo object that reflects WiFi connection state
 *
 * @since Available since 1.0.0
 */
+(nonnull CMTWiFiInfo *) getCurrentWiFiInfo;

/**
 * Returns YES WiFi information can be determined with device OS version and NO otherwise
 *
 * @since Available since 1.0.0
 */
@property (getter=isAvailable, readonly) BOOL available;

@end



