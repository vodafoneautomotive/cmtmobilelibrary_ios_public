//
// CMTAppAnalyticsManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTAppAnalyticsScreenCategory.h"
@class CMTUser;

/**
 Error domain for `CMTAppAnalyticsManagerErrorDomain`

 @since Available since 8.0.0
 */
extern NSString * _Nonnull const CMTAppAnalyticsManagerErrorDomain;

/**
 Error code reported if App Analytics logging fails

 @since Available since 8.0.0
 */
typedef NS_ENUM(NSInteger, CMTAppAnalyticsManagerError) {
    /// CMT Library has not been intitialized (Available since 8.0.0)
    CMTAppAnalyticsManagerErrorLibraryNotInitialized = 0,
    /// String does not conform to requirements (Available since 8.0.0)
    CMTAppAnalyticsManagerErrorInvalidString = 1
};

/**
 Class that manages app analytics logging. Preference should be given to using methods which take a 'CMTAppAnalyticsScreenCategory' to improved
 consistency of logged data.
 
 @since Available since 8.0.0
 */
@interface CMTAppAnalyticsManager : NSObject

/**
 Logs the appearance of a given screen.
 
 @param category The category this event belongs to.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logScreenDidAppearWithCategory:(CMTAppAnalyticsScreenCategory)category withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs the disappearance of a given screen.
 
 @param category The category this event belongs to.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logScreenDidDisappearWithCategory:(CMTAppAnalyticsScreenCategory)category withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs a tap event. For example, you could use this method to track the event of a user tapping a button.
 
 @param category The category this event belongs to.
 @param label The label associated with this event.
 @param value The value associated with this event.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logTapWithCategory:(CMTAppAnalyticsScreenCategory)category
                     label:(nullable NSString *)label
                     value:(nullable NSNumber *)value
                 withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs a result event. For example, you could use this method to track a file has been downloaded successfully.
 
 @param category The category this event belongs to.
 @param label The label associated with this event.
 @param value The value associated with this event.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logResultWithCategory:(CMTAppAnalyticsScreenCategory)category
                        label:(nullable NSString *)label
                        value:(nullable NSNumber *)value
                    withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs a customized event. Note that there will be a timestamp added to each event you log.
 
 @param category The category this event belongs to.
 @param action The action that triggered this event.
 @param label The label associated with this event.
 @param value The value associated with this event.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logEventWithCategory:(CMTAppAnalyticsScreenCategory)category
                      action:(nullable NSString *)action
                       label:(nullable NSString *)label
                       value:(nullable NSNumber *)value
                   withError:(NSError * _Nullable __autoreleasing *_Nullable)error;


/**
 Logs the appearance of a given screen.

 @param categoryString The category this event belongs to.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logScreenDidAppearWithCategoryString:(nonnull NSString *)categoryString withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs the disappearance of a given screen.
 
 @param categoryString The category this event belongs to.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logScreenDidDisappearWithCategoryString:(nonnull NSString *)categoryString withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs a tap event. For example, you could use this method to track the event of a user tapping a button.

 @param categoryString The category this event belongs to.
 @param label The label associated with this event.
 @param value The value associated with this event.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logTapWithCategoryString:(nullable NSString *)categoryString
                           label:(nullable NSString *)label
                           value:(nullable NSNumber *)value
                       withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs a result event. For example, you could use this method to track a file has been downloaded successfully.
 
 @param categoryString The category this event belongs to.
 @param label The label associated with this event.
 @param value The value associated with this event.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 8.0.0
 */
+ (BOOL)logResultWithCategoryString:(nullable NSString *)categoryString
                              label:(nullable NSString *)label
                              value:(nullable NSNumber *)value
                          withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 Logs a customized event. Note that there will be a timestamp added to each event you log.

 @param categoryString The category this event belongs to.
 @param action The action that triggered this event.
 @param label The label associated with this event.
 @param value The value associated with this event.
 @param error (Objective-C only) Set on failure.
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @discussion At least one of categoryString, action, label, value must be non-null
 @since Available since 8.0.0
 */
+ (BOOL)logEventWithCategoryString:(nullable NSString *)categoryString
                            action:(nullable NSString *)action
                             label:(nullable NSString *)label
                             value:(nullable NSNumber *)value
                         withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 App developers should not attempt to create an instance of `CMTAppAnalyticsManager` with default method.
 */
- (nonnull instancetype)init NS_UNAVAILABLE;
+ (nonnull instancetype)new NS_UNAVAILABLE;

@end
