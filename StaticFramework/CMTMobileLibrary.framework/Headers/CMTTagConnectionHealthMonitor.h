//
// CMTTagConnectionHealthMonitor.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 Notification indicating that connection status of tag has changed
 @since Available since 9.0.0
 @discussion The userInfo passed in the notification contains a single entry, with the key
 CMTTagConnectionHealthKey and the value of type NSNumber representing CMTTagConnectionHealth.
 */
extern NSString * _Nonnull const CMTTagConnectionHealthChangedNotification;

/**
 Key for CMTTagConnectionHealthChangedNotification userInfo.
 @since Available since 9.0.0
 */
extern NSString * _Nonnull const CMTTagConnectionHealthKey;

/**
 Allowed values for connection health of tag (if configured)
 @since Available since 9.0.0
 */
typedef NS_ENUM(NSInteger, CMTTagConnectionHealth) {
    //Tag has good connection
    CMTTagConnectionHealthGood = 1,
    //Tag has experienced gaps in data transmission, but may still be connected
    CMTTagConnectionHealthPoor = 2,
    //Tag is disconnected
    CMTTagConnectionHealthNone = 3,
};

/**
 Class to monitor tag connection status
 @since Available since 9.0.0
 */
@interface CMTTagConnectionHealthMonitor : NSObject

/**
 Connection status of tag (if configured)
 @since Available since 9.0.0
 */
@property (readonly, nonatomic) CMTTagConnectionHealth tagConnectionHealth;

/**
 App developers should not attempt to create an instance of `CMTTagConnectionHealthMonitor` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

@end

