//
// CMTTripDetectorTagNotificationDelegate.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTTagConnectionModel.h"
NS_ASSUME_NONNULL_BEGIN

/**
 Delegate protocol to get notifications related to tag operations
 @somce Available since 1.0.0
 */
@protocol CMTTripDetectorTagNotificationDelegate <NSObject>

@optional

/**
 If implemented, called when a tag impact is detected

 @param tagMacAddress tag mac address of connected tag
 @since Available since 1.0.0
 */
-(void) didDetectImpactForMacAddress:(NSString *)tagMacAddress;

/**
 If implemented, called when a tag connects
 
 @param tagMacAddress tag mac address of connected tag
 @param connectionModel detailed information about the tag
 
 @since Available since 1.9.0
 */
-(void) didConnectToTagWithMacAddress:(NSString *)tagMacAddress connectionModel:(CMTTagConnectionModel *)connectionModel;

/**
 If implemented, called when a tag disconnects

 @param tagMacAddress tag mac address of connected tag
 @since Available since 1.0.0
 */
-(void) didDisconnectFromTagWithMacAddress:(NSString *)tagMacAddress;

/**
 If implemented, called when a tag is observed, but we haven't connected to it
 
 @param tagMacAddress tag mac address of connected tag
 @since Available since 1.0.0
 */
-(void) didObserveTagWithMacAddress:(NSString *)tagMacAddress;

@end
NS_ASSUME_NONNULL_END
