//
// CMTURLSessionManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMTServerConfiguration, CMTUser;

/**
 HTTP methods supported
 
 @since Available since 5.0.3
 */
typedef NS_ENUM(NSInteger, CMTHTTPMethod) {
    /// HTTP method GET
    CMTHTTPMethodGet,
    /// HTTP method POST
    CMTHTTPMethodPost,
    /// HTTP method PUT
    CMTHTTPMethodPut,
    /// HTTP method PATCH
    CMTHTTPMethodPatch,
    /// HTTP method DELETE
    CMTHTTPMethodDelete
};

/**
 Class to manage URL Sessions with the CMT server.
 @since Available since 2.0.0
 */
@interface CMTURLSessionManager : NSObject

/** @name Properties */
/**
 The user used to create `CMTURLSessionManager` object.
 @since Available since 2.0.0
 */
@property (readonly, nonatomic, weak, nullable) CMTUser *user;

/**
 The session configuration used to create the managed session.
 @since Available since 2.0.0
 */
@property (readonly, nonatomic, copy, nonnull) NSURLSessionConfiguration *sessionConfiguration;

/**
 The managed session.
 @since Available since 2.0.0
 */
@property (readonly, nonatomic, strong, nonnull) NSURLSession *session;

/**
 App developers should not attempt to create an instance of `CMTURLSessionManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/** @name Methods */
/**
 Initializes a `CMTURLSessionManager` object with an authenticated user and specified session configuration.

 @param user The user used to create the new `CMTURLSessionManager` object.
 @param sessionConfiguration The session configuration used to create the managed session. If `nil`, defaultSessionConfiguration will be used.
 
 @return The newly-initialized `CMTURLSessionManager` object
 
 @discussion By default, CMTURLSessionManager uses the shared NSURLCache and the default cache policy NSURLRequestUseProtocolCachePolicy. However, if you want more control of the caching of responses to URL load requests and the interaction with the cached responses, refer to the Apple Developer technical documentation for NSURLCache and NSURLRequestCachePolicy.
 @since Available since 2.0.0
 */
- (nonnull instancetype)initWithUser:(nullable CMTUser *)user sessionConfiguration:(nullable NSURLSessionConfiguration *)sessionConfiguration NS_DESIGNATED_INITIALIZER;

/**
 Creates and runs an `NSURLSessionDataTask` with a `GET` request.

 @param path The path to create the request URL. Note that the endpoint URL is formed by combining the serverConfiguration.serverURL with the path.
 @param queryParameters Key value pairs to create the request URL to filter result, e.g. { "key1": "value1", 'key2': "value2" } -> ?key1=value1&key2=value2
 @param headers Key value pairs to add to the header of the HTTP request.
 @param completionHandler A block object to be executed when the task finishes. If the task finishes successfully, the error object will be `nil`. Otherwise, the error object will describe the network error or any error returned by the server. Note that the completionHandler block will be executed in a private serial queue.
 
 @return An NSURLSessionDataTask object
 
 @since Available since 2.0.0
 */
- (nullable NSURLSessionDataTask *)getFromPath:(nonnull NSString *)path
                              queryParameters:(nullable NSDictionary<NSString *, NSString *> *)queryParameters
                                      headers:(nullable NSDictionary<NSString *, NSString *> *)headers
                             completionHandler:(void (^_Nullable)(NSURLResponse * _Nullable response,
                                                                  NSDictionary * _Nullable responseObject,
                                                                  NSError * _Nullable error))completionHandler;

/**
 Creates and runs an `NSURLSessionDataTask` with a `GET` request.
 
 @param path The path to create the request URL. Note that the endpoint URL is formed by combining the serverConfiguration.serverURL with the path.
 @param queryParameters Key value pairs to create the request URL to filter result, e.g. { "key1": "value1", 'key2': "value2" } -> ?key1=value1&key2=value2
 @param headers Key value pairs to add to the header of the HTTP request.
 @param cachedResponseObject If a cached value for this response exists, this will be set to the cached dictionary. The key used to index the cachedResponse is based on the queryParameters and path objects that are passed into the method. All cached values are cleared when a user
     first signs in.
 @param completionHandler A block object to be executed when the task finishes. If the task finishes successfully, the error object will be `nil`. Otherwise, the error object will describe the network error or any error returned by the server. Note that the completionHandler block will be executed in a private serial queue.
 
 @return An NSURLSessionDataTask object
 
 @since Available since 2.0.0
 */
- (nullable NSURLSessionDataTask *)getFromPath:(nonnull NSString *)path
                               queryParameters:(nullable NSDictionary<NSString *, NSString *> *)queryParameters
                                       headers:(nullable NSDictionary<NSString *, NSString *> *)headers
                                cachedResponse:(NSDictionary * __autoreleasing _Nullable * _Nullable)cachedResponseObject
                             completionHandler:(void (^_Nullable)(NSURLResponse * _Nullable response,
                                                                  NSDictionary * _Nullable responseObject,
                                                                  NSError * _Nullable error))completionHandler;

/**
 Creates and runs an `NSURLSessionDataTask` with a `POST` request.

 @param path The path to create the request URL. Note that the endpoint URL is formed by combining the serverConfiguration.serverURL with the path.
 @param headers Key value pairs to add to the header of the HTTP request.
 @param bodyData Data to form the HTTP body
 @param completionHandler A block object to be executed when the task finishes. If the task finishes successfully, the error object will be `nil`. Otherwise, the error object will describe the network error or any error returned by the server. Note that the completionHandler block will be executed in a private serial queue.
 
 @return An NSURLSessionDataTask object
 
 @since Available since 2.0.0
 */
- (nullable NSURLSessionDataTask *)postToPath:(nonnull NSString *)path
                                     headers:(nullable NSDictionary<NSString *, NSString *> *)headers
                                    bodyData:(nonnull NSData *)bodyData
                            completionHandler:(void (^_Nullable)(NSURLResponse * _Nullable response,
                                                                 NSDictionary * _Nullable responseObject,
                                                                 NSError * _Nullable error))completionHandler;


/**
 Creates and runs an `NSURLSessionDataTask` to perform a data task

 @param path The path to create the request URL. Note that the endpoint URL is formed by combining the serverConfiguration.serverURL with the path.
 @param method CMTHTTPMethod type
 @param queryParameters Key value pairs to create the request URL to filter result, e.g. { "key1": "value1", 'key2': "value2" } -> ?key1=value1&key2=value2
 @param bodyData Data to form the HTTP body
 @param headers Key value pairs to add to the header of the HTTP request.
 @param cachedResponse This will search the shared NSURLCache using the request created by the parameters you specify in this call, if one is found, the cachedResponse will be set, otherwise nil.
 @param completionHandler A block object to be executed when the task finishes. If the task finishes successfully, the error object will be `nil`. Otherwise, the error object will describe the network error or any error returned by the server. Note that the completionHandler block will be executed in a private serial queue.
 @return An NSURLSessionDataTask object
 
 @discussion By default, CMTURLSessionManager uses the shared NSURLCache and the default cache policy NSURLRequestUseProtocolCachePolicy. However, if you want more control of the caching of responses to URL load requests and the interaction with the cached responses, refer to the Apple Developer technical documentation for NSURLCache and NSURLRequestCachePolicy.

 @since Available since 5.0.3
 */
- (nullable NSURLSessionDataTask *)requestForPath:(nonnull NSString *)path
                                           method:(CMTHTTPMethod)method
                                  queryParameters:(nullable NSDictionary *)queryParameters
                                         bodyData:(nullable NSData *)bodyData
                                          headers:(nullable NSDictionary *)headers
                                   cachedResponse:(NSCachedURLResponse * __autoreleasing _Nullable * _Nullable)cachedResponse
                                completionHandler:(void (^_Nullable)(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable error))completionHandler;

/**
 The mobile library will maintain a custom response cache for the user this object holds a reference to.
 This method clears the response cache for the user this object holds a reference to.
 
 @discussion If the user logs out or becomes deauthorized, CMT recommends that developers call this method
 to clear the user-specific response cache. This cache is unrelated to any NSURLCache and NSURLRequestCachePolicy.
 @since Available since 2.0.0
 */
- (void)clearCache;

@end

