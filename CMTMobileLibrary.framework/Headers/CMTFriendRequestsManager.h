//
// CMTFriendRequestsManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTUserBaseDataProvider.h"
#import "CMTFriendRequestsReceivedModel.h"
#import "CMTFriendResponseModel.h"

/*!
 *  Name of the error domain for CMTFriendRequestsManager
 *  @since Available since 0.1.0
 */
extern NSString * _Nonnull const CMTFriendRequestManagerErrorDomain;

/**
 Error codes when the request manager has a problem.
 @discussion Deprecated since 9.0.0;
 @since Available since 0.1.0
 */
typedef NS_ENUM(NSInteger, CMTFriendRequestManagerError) {
    /// Serialzation error (Available since 0.1.0)
    CMTFriendRequestManagerErrorSerializationFailed = -1
};

/*!
 *  The `CMTFriendRequestManager` class manages incoming invitations and friend requests.
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendRequestsManager : CMTUserBaseDataProvider

/*!
 *  Incoming friend requests.
 *  @since Available since 0.1.0
 */
@property (readonly,strong,nullable) CMTFriendRequestsReceivedModel *friendRequests;

/**
 App developers should not attempt to create an instance of `CMTFriendRequestsManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Fetch information about friend requests for you.
 *  @since Available since 1.0.4
 *  @param handler The handler to call if specified. The handler will be passed `value`, which is an instance of an
 *  `CMTFriendRequestsReceivedModel` object, whether or not the fetch was successful as `success`. The `error` parameter
 *  will be set if there is an error.
 *  @return A reference to the NSOperation assigned to the block executing the asynchronous update
 */
-(nonnull NSOperation *)loadFriendRequestsReceivedInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTFriendRequestsReceivedModel * _Nullable requestsReceived, BOOL success, NSError * _Nullable error))handler;

/*!
 *  Sends an array of accept responses to invitations sent to this user
 *  @since Available since 1.0.4
 *  @param responseArray An `NSArray` of `CMTFriendResponseModel` objects with the `inviteId` field or `inviteSecret` populated.
 *  @param handler       If not null, handler will be called with the response and success code. The response from the server is
 *                       returned in the value parameter.
 * @return A reference to the NSOperation assigned to the block executing the asynchronous update
 */
-(nonnull NSOperation *)acceptInvitationsInBackground:(NSArray <CMTFriendResponseModel *> *_Nonnull)responseArray
                                    completionHandler:(void (^_Nullable)(CMTFriendRequestsReceivedModel * _Nullable requestsReceived, BOOL success, NSError * _Nullable error))handler;

/*!
 *  Sends an array of invite responses to invitations sent to this user
 *  @since Available since 1.0.4
 *  @param responseArray An `NSArray` of `CMTFriendResponseModel` objects with the inviteId field populated.
 *  @param handler       If not null, handler will be called with the response and success code. The response from the server is returned in the value parameter.
 *  @return A reference to the `NSOperation` assigned to the block executing the asynchronous update
 */
-(nonnull NSOperation *)ignoreInvitationsInBackground:(NSArray <CMTFriendResponseModel *> *_Nonnull)responseArray
                                    completionHandler:(void (^_Nullable)(CMTFriendRequestsReceivedModel * _Nullable requestsReceived, BOOL success, NSError * _Nullable error))handler;

@end
