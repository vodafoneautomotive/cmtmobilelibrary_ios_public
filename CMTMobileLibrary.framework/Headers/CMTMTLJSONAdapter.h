//
// CMTMTLJSONAdapter.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@protocol CMTMTLModel;
@protocol CMTMTLTransformerErrorHandling;

/// A CMTMTLModel object that supports being parsed from and serialized to JSON.
@protocol CMTMTLJSONSerializing <CMTMTLModel>
@required

/**
 Specifies how to map property keys to different key paths in JSON.

 Subclasses overriding this method should combine their values with those of
 `super`.

 Values in the dictionary can either be key paths in the JSON representation
 of the receiver or an array of such key paths. If an array is used, the
 deserialized value will be a dictionary containing all of the keys in the
 array.

 Any keys omitted will not participate in JSON serialization.

 Examples

     + (NSDictionary *)JSONKeyPathsByPropertyKey {
         return @{
             @"name": @"POI.name",
             @"point": @[ @"latitude", @"longitude" ],
             @"starred": @"starred"
         };
     }

 This will map the `starred` property to `JSONDictionary[@"starred"]`, `name`
 to `JSONDictionary[@"POI"][@"name"]` and `point` to a dictionary equivalent
 to:

     @{
         @"latitude": JSONDictionary[@"latitude"],
         @"longitude": JSONDictionary[@"longitude"]
     }

 @return A dictionary mapping property keys to one or multiple JSON key paths
 (as strings or arrays of strings).
*/
+ (NSDictionary *)JSONKeyPathsByPropertyKey;

@optional

/**
 Specifies how to convert a JSON value to the given property key. If
 reversible, the transformer will also be used to convert the property value
 back to JSON.

 If the receiver implements a `+<key>JSONTransformer` method, CMTMTLJSONAdapter
 will use the result of that method instead.

 @param key property key
 @return a value transformer, or nil if no transformation should be performed.
*/
+ (nullable NSValueTransformer *)JSONTransformerForKey:(NSString *)key;

/**
 Overridden to parse the receiver as a different class, based on information
 in the provided dictionary.

 @discussion This is mostly useful for class clusters, where the abstract base class would be passed into
 -[CMTMTLJSONAdapter initWithJSONDictionary:modelClass:error:], but a subclass should be instantiated instead.
 @param JSONDictionary JSON dictionary that will be parsed.
 @return the class that should be parsed (which may be the receiver), or nil to abort parsing (e.g., if the data is invalid).
*/
+ (nullable Class)classForParsingJSONDictionary:(NSDictionary *)JSONDictionary;

@end

/// The domain for errors originating from CMTMTLJSONAdapter.
extern NSString * const CMTMTLJSONAdapterErrorDomain;

/// +classForParsingJSONDictionary: returned nil for the given dictionary.
extern const NSInteger CMTMTLJSONAdapterErrorNoClassFound;

/// The provided JSONDictionary is not valid.
extern const NSInteger CMTMTLJSONAdapterErrorInvalidJSONDictionary;

/**
 The model's implementation of +JSONKeyPathsByPropertyKey included a key which
 does not actually exist in +propertyKeys.
 */
extern const NSInteger CMTMTLJSONAdapterErrorInvalidJSONMapping;

/** An exception was thrown and caught. */
extern const NSInteger CMTMTLJSONAdapterErrorExceptionThrown;

/** Associated with the NSException that was caught. */
extern NSString * const CMTMTLJSONAdapterThrownExceptionErrorKey;

/** Converts a CMTMTLModel object to and from a JSON dictionary. */
@interface CMTMTLJSONAdapter : NSObject

/**
 Attempts to parse a JSON dictionary into a model object.
 @param modelClass The CMTMTLModel subclass to attempt to parse from the JSON. This class must conform to <CMTMTLJSONSerializing>. This argument must not be nil.
 @param JSONDictionary A dictionary representing JSON data. This should match the format returned by NSJSONSerialization. If this argument is nil, the method returns nil.
 @param error  If not NULL, this may be set to an error that occurs during parsing or initializing an instance of `modelClass`.
 @return an instance of `modelClass` upon success, or nil if a parsing error occurred.
*/
+ (nullable id)modelOfClass:(Class)modelClass fromJSONDictionary:(nullable NSDictionary *)JSONDictionary error:(NSError **)error;

/**
 Attempts to parse an array of JSON dictionary objects into a model objects of a specific class.
 @param modelClass The CMTMTLModel subclass to attempt to parse from the JSON. This class must conform to <CMTMTLJSONSerializing>. This argument must not be nil.
 @param JSONArray A array of dictionaries representing JSON data. This should match the format returned by NSJSONSerialization. If this argument is nil, the method returns nil.
 @param error  If not NULL, this may be set to an error that occurs during parsing or initializing an any of the instances of `modelClass`.
 @return an array of `modelClass` instances upon success, or nil if a parsing error occurred.
 */
+ (nullable NSArray *)modelsOfClass:(Class)modelClass fromJSONArray:(nullable NSArray *)JSONArray error:(NSError **)error;

/**
 Converts a model into a JSON representation.
 @param model The model to use for JSON serialization. This argument must not be nil.
 @param error If not NULL, this may be set to an error that occurs during serializing.
 @return a JSON dictionary, or nil if a serialization error occurred.
 */
+ (nullable NSDictionary *)JSONDictionaryFromModel:(id<CMTMTLJSONSerializing>)model error:(NSError **)error;

/**
 Converts a array of models into a JSON representation.
 @param models The array of models to use for JSON serialization. This argument must not be nil.
 @param error  If not NULL, this may be set to an error that occurs during serializing.
 @return a JSON array, or nil if a serialization error occurred for any model.
 */
+ (nullable NSArray *)JSONArrayFromModels:(NSArray *)models error:(NSError **)error;

/**
 App developers should not attempt to create an instance of `CMTMTLJSONAdapter` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Initializes the receiver with a given model class.
 @param modelClass  The CMTMTLModel subclass to attempt to parse from the JSON and back.
 This class must conform to <CMTMTLJSONSerializing>. This argument must not be nil.
 @return an initialized adapter
 */
- (id)initWithModelClass:(Class)modelClass;

/**
 Deserializes a model from a JSON dictionary. The adapter will call -validate: on the model and consider it an error if the validation fails.
 @param JSONDictionary A dictionary representing JSON data. This should match the format returned by NSJSONSerialization. This argument must not be nil.
 @param error  If not NULL, this may be set to an error that occurs during deserializing or validation.
 @return a model object, or nil if a deserialization error occurred or the model did not validate successfully.
 */
- (nullable id)modelFromJSONDictionary:(NSDictionary *)JSONDictionary error:(NSError **)error;

/**
 Serializes a model into JSON.
 @param model The model to use for JSON serialization. This argument must not be nil.
 @param error If not NULL, this may be set to an error that occurs during serializing.
 @return a model object, or nil if a serialization error occurred.
 */
- (nullable NSDictionary *)JSONDictionaryFromModel:(id<CMTMTLJSONSerializing>)model error:(NSError **)error;

/**
 Filters the property keys used to serialize a given model.

 Subclasses may override this method to determine which property keys should
 be used when serializing `model`. For instance, this method can be used to
 create more efficient updates of server-side resources.

 The default implementation simply returns propertyKeys.

 @param propertyKeys The property keys for which `model` provides a mapping model.
 @param model The model being serialized.
 @return a subset of propertyKeys that should be serialized for a given model.
 */
- (NSSet *)serializablePropertyKeys:(NSSet *)propertyKeys forModel:(id<CMTMTLJSONSerializing>)model;

/**
 An optional value transformer that should be used for properties of the given
 class.

 A value transformer returned by the model's +JSONTransformerForKey: method
 is given precedence over the one returned by this method.

 The default implementation invokes `+<class>JSONTransformer` on the
 receiver if it's implemented. It supports NSURL conversion through
 +NSURLJSONTransformer.

 @param modelClass The class of the property to serialize. This property must not be nil.
 @return a value transformer or nil if no transformation should be used.
 */
+ (nullable NSValueTransformer *)transformerForModelPropertiesOfClass:(Class)modelClass;

/**
 A value transformer that should be used for a properties of the given
 primitive type.

 If `objCType` matches @ encode(id), the value transformer returned by
 +transformerForModelPropertiesOfClass: is used instead.

 The default implementation transforms properties that match @ encode(BOOL)
 using the CMTMTLBooleanValueTransformerName transformer.

 @param objCType The type encoding for the value of this property. This is the type as it would be returned by the @ encode() directive.
 @return a value transformer or nil if no transformation should be used.
 */
+ (nullable NSValueTransformer *)transformerForModelPropertiesOfObjCType:(const char *)objCType;

@end

@interface CMTMTLJSONAdapter (ValueTransformers)

/**
 Creates a reversible transformer to convert a JSON dictionary into a CMTMTLModel object, and vice-versa.
 @param modelClass The CMTMTLModel subclass to attempt to parse from the JSON. This class must conform to <CMTMTLJSONSerializing>. This argument must not be nil.
 @return a reversible transformer which uses the class of the receiver for transforming values back and forth.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> * _Nonnull)dictionaryTransformerWithModelClass:(Class _Nonnull)modelClass;

/**
 Creates a reversible transformer to convert an array of JSON dictionaries into an array of CMTMTLModel objects, and vice-versa.
 @param modelClass The CMTMTLModel subclass to attempt to parse from each JSON dictionary. This class must conform to <CMTMTLJSONSerializing>. This argument must not be nil.
 @return a reversible transformer which uses the class of the receiver for transforming array elements back and forth.
*/
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> * _Nonnull)arrayTransformerWithModelClass:(Class _Nonnull)modelClass;

/**
 This value transformer is used by CMTMTLJSONAdapter to automatically convert NSURL properties to JSON strings and vice versa.
 */
+ (NSValueTransformer * _Nonnull)NSURLJSONTransformer;

/**
 This value transformer is used by CMTMTLJSONAdapter to automatically convert NSUUID properties to JSON strings and vice versa.
 */
+ (NSValueTransformer * _Nonnull)NSUUIDJSONTransformer;

@end

@class CMTMTLModel;

@interface CMTMTLJSONAdapter (Deprecated)

@property (nonatomic, strong, readonly) id<CMTMTLJSONSerializing> model __attribute__((unavailable("Replaced by -modelFromJSONDictionary:error:")));

+ (NSArray *)JSONArrayFromModels:(NSArray *)models __attribute__((deprecated("Replaced by +JSONArrayFromModels:error:"))) NS_SWIFT_UNAVAILABLE("Replaced by +JSONArrayFromModels:error:");

+ (NSDictionary *)JSONDictionaryFromModel:(CMTMTLModel<CMTMTLJSONSerializing> *)model __attribute__((deprecated("Replaced by +JSONDictionaryFromModel:error:"))) NS_SWIFT_UNAVAILABLE("Replaced by +JSONDictionaryFromModel:error:");

- (NSDictionary *)JSONDictionary __attribute__((unavailable("Replaced by -JSONDictionaryFromModel:error:"))) NS_SWIFT_UNAVAILABLE("Replaced by -JSONDictionaryFromModel:error:");
- (NSString *)JSONKeyPathForPropertyKey:(NSString *)key __attribute__((unavailable("Replaced by -serializablePropertyKeys:forModel:")));

/**
 Initializes the receiver given a dictionary and the model class.
 @param JSONDictionary The input dictionary to serialize.
 @param modelClass  The CMTMTLModel subclass to attempt to parse from the JSON and back.
 This class must conform to <CMTMTLJSONSerializing>. This argument must not be nil.
 @param error  If not NULL, this may be set to an error that occurs during parsing or initializing an any of the instances of the `modelClass`.
 @return an initialized adapter
*/
- (id)initWithJSONDictionary:(NSDictionary *)JSONDictionary modelClass:(Class)modelClass error:(NSError **)error __attribute__((unavailable("Replaced by -initWithModelClass:")));
- (id)initWithModel:(id<CMTMTLJSONSerializing>)model __attribute__((unavailable("Replaced by -initWithModelClass:"))) NS_SWIFT_UNAVAILABLE("Replaced by -initWithModelClass:");
- (NSDictionary *)serializeToJSONDictionary:(NSError **)error __attribute__((unavailable("Replaced by -JSONDictionaryFromModel:error:"))) NS_SWIFT_UNAVAILABLE("Replaced by -JSONDictionaryFromModel:error:");

@end
NS_ASSUME_NONNULL_END
