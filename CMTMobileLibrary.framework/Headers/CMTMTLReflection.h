//
// CMTMTLReflection.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/// Creates a selector from a key and a constant string.
/// @param key The key to insert into the generated selector. This key should be in its natural case.
/// @param suffix A string to append to the key as part of the selector.
/// @return A selector, or NULL if the input strings cannot form a valid selector.
SEL CMTMTLSelectorWithKeyPattern(NSString *key, const char *suffix) __attribute__((pure, nonnull(1, 2)));

/// Creates a selector from a key and a constant prefix and suffix.
/// @param prefix A string to prepend to the key as part of the selector.
/// @param key The key to insert into the generated selector. This key should be in its natural case, and will have its first letter capitalized when inserted.
/// @param suffix A string to append to the key as part of the selector.
/// @return A selector, or NULL if the input strings cannot form a valid selector.
SEL CMTMTLSelectorWithCapitalizedKeyPattern(const char *prefix, NSString *key, const char *suffix) __attribute__((pure, nonnull(1, 2, 3)));
