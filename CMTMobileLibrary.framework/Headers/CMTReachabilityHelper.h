//
// CMTReachabilityHelper.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Notification for reachability change
 
 @since Available since 1.7.0
 */
extern NSString * _Nonnull const CMTReachabilityHelperReachabilityDidChangeNotification;

/**
 Helper class for reachability check
 
 @since Available since 1.7.0
 */
@interface CMTReachabilityHelper : NSObject

/**
 Boolean indicating if network is reachable
 
 @since Available since 1.7.0
 */
@property (nonatomic, readonly) BOOL reachable;

/**
 Boolean indicating if network is reachable via WiFi
 
 @since Available since 1.7.0
 */
@property (nonatomic, readonly) BOOL reachableViaWiFi;

/**
 Boolean indicating whether the reachability properties are valid
 
 @discussion When reachability is first queried, the network may be reachable; however, the CMTReachabilityHelper may provide
             incorrect results until this property returns to YES.
 @since Available since 4.0.0
 */
@property (nonatomic, readonly) BOOL isValid;

/**
 Returns singleton object of `CMTReachabilityHelper` class
 
 @sicne Available since 1.7.0
 */
+(nonnull instancetype)sharedInstance;

@end
