//
// CMTUser.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CMTBadgesDataManager.h"
#import "CMTUserSummaryDataProvider.h"
#import "CMTFriendsManager.h"
#import "CMTUserDeviceSettingsManager.h"
#import "CMTServerRequestsHelper.h"
#import "CMTCustomLeaderboardDataManager.h"
#import "CMTTripDataManager.h"
#import "CMTDriveScheduleManager.h"
#import "CMTImpactDataManager.h"
#import "CMTTripDetector.h"
#import "CMTVehicleManager.h"
@class CMTTagConnectionHealthMonitor;

NS_ASSUME_NONNULL_BEGIN
/** Errors from CMTUser will have this domain name
 * @since Available since 1.2.0
 */
extern NSString *const CMTUserErrorDomain;

/**
 Types of errors that can be returned from CMTUser
 @since      Available since 1.2.0
 */
typedef NS_ENUM(NSInteger, CMTUserError) {
    /// User was not authenticated (Available since 1.2.0)
    CMTUserErrorNotAuthenticated = 0
};

#pragma mark - NSNotificationCenter keys
/*! 
 * Key to use to notify when user's tag mode has changed
 * @since Available since 1.9.0
 */
extern NSString * const CMTUserProfileTagModeDidChange;

#pragma mark - User profile and registration keys
/*! 
 * Key to use to access / update user's email.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileEmailKey;

/*! 
 * Key to use to access / update user's firstname.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileFirstNameKey;

/*! 
 * Key to use to access / update user's lastname.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileLastNameKey;

/*! 
 * Key to use to access / update user's mobile number.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileMobileNumberKey;

/*! 
 * Key to use to access / update user's primary name.
 * @since Available since 1.1.0
 */
extern NSString *const CMTUserProfilePrimaryNameKey;

/*! 
 * Key to use to access / update user's secondary name.
 * @since Available since 1.1.0
 */
extern NSString *const CMTUserProfileSecondaryNameKey;

/*! 
 * Key to use to access / update user's group ID.
 * @since Available since 1.1.0
 */
extern NSString *const CMTUserProfileGroupIDKey;

/*! 
 * Key to use to access / update user's policy number.
 * @since Available since 1.1.0
 */
extern NSString *const CMTUserProfilePolicyNumberKey;

/*! 
 * Key to use to access / update information on how to distinguish different users with same policy number
 * @since Available since 1.6.0
 */
extern NSString *const CMTUserProfilePolicyIndexKey;

/*! 
 * Key to use to access / update user's province
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileProvinceKey;

/*! 
 * Key to use to access / update user's gender
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileGenderKey;

/*! 
 * Key to use to access / update user's South African national id
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileZaIdKey;

/*!
 * Key to use to fetch a users registration date
 * ISO 8601 date string: YYYY-MM-DD
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileRegistrationDateKey __attribute__((deprecated("Deprecated since 4.0.0. Use CMTUserProfileRegistrationDateTimeKey instead.")));

/*!
 * Key to use to fetch a users registration date and time
 * The string is represented an ISO 8601 date time string with format YYYY-MM-DDThh:mm:ssZ.
 * @since Available since 4.0.0
 */
extern NSString *const CMTUserProfileRegistrationDateTimeKey;


/*!
 * Key to use to fetch a client use's policy effective date
 * The string is represented an ISO 8601 date time string with format YYYY-MM-DDThh:mm:ssZ.
 * @since Available since 4.0.0
 */
extern NSString *const CMTUserProfileEffectiveDateTimeKey;

/*!
 * Key to use to fetch the date that score will be frozen
 * The string is represented an ISO 8601 date time string with format YYYY-MM-DDThh:mm:ssZ.
 * @since Available since 5.0.0
 */
extern NSString *const CMTUserProfileFreezeScoreDateTimeKey;

/*! 
 * Key to use to access / update handle suffix for the user
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileHandleSuffixKey;

/*! 
 * Key to use to access user's customer state: "TRIAL" or "CLIENT"
 * @since Available since 1.0.0
 * @deprecated Deprecated since 6.0.0
 */
extern NSString *const CMTUserProfileCustomerModeKey __attribute__((deprecated("Deprecated since 6.0.0.")));

/*! 
 * Key to use to access user's customer type: "DYNAMIC"
 * @since Available since 1.4.1
 */
extern NSString *const CMTUserProfileCustomerTypeKey;

/**
 * Key to use to access the customer's policy / trial expiration date
 * The string is represented an ISO 8601 date time string with format YYYY-MM-DDThh:mm:ssZ.
 * @since Available since 4.0.0
 */
extern NSString *const CMTUserProfileExpirationDateTimeKey;

/**
 * Key to use to access the customer's start recording date time
 * @since Available since 4.0.0
 */
extern NSString *const CMTUserProfileStartRecordingDateTimeKey;

/** 
 * Key to use to access whether user is in tag mode or not
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileTagModeKey;

/**
 * Key to use to access / update user's primary vehicle make
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfilePrimaryVehicleMakeKey;

/** 
 * Key to use to access / update user's primary vehicle fuel type
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfilePrimaryVehicleFuelKey;

/** 
 * Key to use to access / update user's primary vehicle transmission type
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfilePrimaryVehicleTransmissionKey;

/** 
 * Key to use to access / update user's primary vehicle engine capacity type
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfilePrimaryVehicleEngineCapacityKey;

/** 
 * Key to use to access / update user's primary vehicle registration
 * @since Available since 1.2.4
 */
extern NSString *const CMTUserProfilePrimaryVehicleRegistrationKey;

/*! 
 * Key to use to access / update user's registration token
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileRegCodeKey;

/*! 
 * Key to use to submit a user's PIN code
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileAuthCodeKey;

/*! 
 * Key to use to submit / fetch user's account id
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileAccountIdKey;

/*! 
 * Key to use to access / update user's username.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileUsernameKey;

/*! 
 * Key to use to submit enrollment / activation code for user
 * @discussion This key is only used to post enrollment / activation code for a user, not fetch it.
 * @since Available since 1.2.1
 */
extern NSString *const CMTUserProfileEnrollmentCodeKey;

/*! 
 * Key to use to submit birthdate for user, value should be ISO8601 date without time
 * @since Available since 1.2.1
 */
extern NSString *const CMTUserProfileBirthDateKey;

/*! 
 * Key to use to indicate server's view of whether user has FB auto friending
 * @since Available since 1.3.11
 */
extern NSString *const CMTUserProfileSharingWithFbAutoFriendsKey;

/*! 
 * Key to use to fetch / submit user's zip code
 * @since Available since 1.3.18
 */
extern NSString *const CMTUserProfileZipCodeKey;

/*! 
 * Key to use to fetch / submit user's city
 * @since Available since 1.3.18
 */
extern NSString *const CMTUserProfileCityKey;

/*! 
 * Key to use to fetch / submit user defined state
 * @since Available since 1.3.18
 */
extern NSString *const CMTUserProfileUserStateKey;

/*! 
 * Key to use to fetch / submit opaque user id
 * @since Available since 1.7.1
 */
extern NSString *const CMTUserProfileOpaqueUserId;

/*! 
 * Key to use to fetch customer information
 * @since Available since 1.9.6
 */
extern NSString *const CMTUserProfileCustomerKeyKey;

/*!
 * Key to use to fetch driver license
 * @since Available since 5.0.0
 */
extern NSString *const CMTUserProfileDriverLicenseKey;

/*!
 * Key to use to fetch activation code
 * @discussion This key is only used to fetch activation code, not update it.
 * @since Available since 5.0.0
 */
extern NSString *const CMTUserProfileActivationCode;

#pragma mark - Fetch profile only
/*! 
 * Key to get short user id
 * @since Available since 1.8.0
 */
extern NSString *const CMTUserProfileShortUserIdKey;

/*! 
 * Key to get URL for the photo. In profile responses only.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfilePhotoURLKey;

/*! 
 * Key that indicates whether user is a towers user or not. (Boolean)
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileTowersScoringKey;

/*!
 * Key to fetch whether user has facebook or not. (Boolean)
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileHasFacebookKey;

/*! 
 * Key to fetch whether user has South African Id or not. (Boolean)
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileHasZaIdKey;

/*! 
 * Key to fetch whether user has sent initial location to server or not. (Boolean)
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileHasInitialLocation;

#pragma mark - Update profile only
/*! 
 * Key to set user's photo. Use when updating profile.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfilePhotoKey;

/*! 
 * Key to set facebook token for user. Use when updating profile.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileFacebookTokenKey;

/*! 
 * Key to set initial location of user. Use when updating profile
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileInitialLocationKey;

/*! 
 * Key that indicates whether drive recording schedule should be fetched or not
 * @since Available since 1.5.0
 */
extern NSString *const CMTUserProfileGetDriverRecordingScheduleKey;

/*!
 * Convenience constant for the user profile key in some NSDictionary responses
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileKey;

#pragma mark - Notifications
/*! 
 * Notification that will be posted when a user becomes unauthorized and the app tries to
 * make requests to the endpoints that require user authentication.
 *
 * @note This notification lets app developers know when a user is deauthorized from the backend.
 * When a developer successfully calls [CMTUser logOutWithError:], this notification will not be
 * posted because the app developer will know that the user isn’t authenticated anymore.
 *
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserNotAuthorizedNotification;

/*! 
 * Use this key in the userInfo dictionary component of the NSNotification object to get the short userId of the not authorized user.
 * The short userId will be wrapped in an NSNumber object.
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserNotAuthorizedNotificationUserId;

/*! 
 * Notification key when user profile gets updated
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileDidChangeNotification;

/*! 
 * Use this key in the userInfo dictionary component to get the updated user profile dictionary
 * @since Available since 1.0.0
 */
extern NSString *const CMTUserProfileDidChangeNotificationProfile;
NS_ASSUME_NONNULL_END

#pragma mark - CMTUser
/**
 CMTUser object enables you to authenticate a user, obtain a user’s profile, and update a user’s profile. 
 After authentication has been completed once, authentication information is persisted in the 
 SDK until the user is de-authorized or logged out. You can use the CMTUser class to register a user 
 with credentials, register a user without credentials (anonymous user registration), or to re-authenticate a previously registered user. 
 */
@interface CMTUser : CMTMTLModel


/*!
 *  The user secret for the CMTUser
 *  @discussion This is set by the server upon successful authentication and is nil otherwise.
 *  @since Available since 1.8.0
 */
@property (readonly,nullable) NSString *userSecret;

/*!
 *  The short user id for the CMTUser
 *  @discussion This is provided by the server and should be used for logging and tracking a user across a system.
 *  The value is unique across each CMT backend system, but is not a globally unique id.
 *  @since Available since 1.8.0
 */
@property (readonly) NSInteger shortUserId;

/*!
 * true when the userId is not nil.
 * @since Available since 1.0.0
 */
@property (readonly, getter=isAuthenticated) BOOL authenticated;


/*! true when the tag_user field in the profile is true
 * @since Available since 1.0.0
 */
@property (readonly) BOOL tagUser;

/*!
 * The email address of the user if userProfile is not nil
 * @since Available since 1.0.0
 */
@property (readonly,nullable) NSString *emailAddress;

/*!
 * References to the CMTTripDataManager object for this user
 * @since Available since 1.0.0
 */
@property (readonly,nullable) CMTTripDataManager *tripManager;


/**
 * References to the CMTTagConnectionHealthMonitor object for this user
 * @since Available since 8.0.0
 */
@property (readonly,nullable) CMTTagConnectionHealthMonitor *tagConnectionHealthMonitor;

/*! 
 * Reference to the CMTBadgesDataProvider object for this user
 * @since Available since 1.0.0
 * @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
@property (readonly,nullable) CMTBadgesDataManager *badgesProvider __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));

/*! 
 * Provides a reference to a CMTUserSummaryDataProvider object for this user
 * @since Available since 1.0.0
 * @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
@property (readonly,nullable) CMTUserSummaryDataProvider *userSummaryProvider __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));

/*! 
 * Provides a reference to a CMTFriendsManager object for this user
 * @since Available since 1.0.0
 * @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
@property (readonly,nullable) CMTFriendsManager *friendsManager __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));

/*! 
 * Provides a references to the CMTCustomLeaderboardDataManager object for this user
 * @since Available since 1.0.0
 * @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
@property (readonly,nullable) CMTCustomLeaderboardDataManager *customLeaderboardManager __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));

/*! 
 * Provides a reference to a CMTUserDeviceSettingsManager object for this user
 * @since Available since 1.0.0
 */
@property (readonly,nullable) CMTUserDeviceSettingsManager *deviceSettingsManager;

/*! 
 * Provides a reference to a CMTImpactDataManager object for this user
 * @since Available since 1.7.0
 * @deprecated Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property (readonly, nullable) CMTImpactDataManager *impactDataManager __attribute__((deprecated("Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts."))); 

/*! 
 * Provides a reference to a CMTServerRequestsHelper object for this user
 * @since Available since 1.0.0
 * @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
@property (readonly,nullable) CMTServerRequestsHelper *requestsHelper __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));

/*! 
 * A reference to the most recently fetched userProfile
 * @since Available since 1.0.0
 */
@property (readonly, nullable) NSDictionary <NSString*, id> *userProfile;

/*! 
 * A reference to a driveScheduleManager for this user
 * @since Available since 1.5.0
 */
@property (nullable) CMTDriveScheduleManager *driveScheduleManager;

/**
 Reference to the CMTVehicleManager object for this user
 @since Available since 5.0.0
 */
@property (nullable) CMTVehicleManager *vehicleManager;

/*! 
 * Queue on which asynchronous operations will occur
 * @since Available since 1.0.0
 */
@property (nonnull) NSOperationQueue *fetchQueue;

/*!
 *  <i>Synchronously</i> sends a preregister request to CMT's backend
 *
 *  @param request  An NSDictionary containing `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId` as keys.
 *                  You may also send the additional fields: `CMTUserProfileEnrollmentCode` to help the app display the right thing to the user.
 *                  Values must all be NSString.
 *  @param response An NSDictionary with the following keys: `CMTUserProfileHandleSuffix`, `CMTUserProfileKey`. The value of the `CMTUserProfileKey`
 *                  key is an NSDictionary containing the profile.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 * 
 *  @since Available since 1.0.0
 */
-(BOOL)preregisterRequest:(NSDictionary *_Nonnull)request
                 response:(NSDictionary * _Nonnull *_Nonnull)response
                    error:(NSError *_Nullable*_Nullable)error;

/*!
 *  <i>Asynchronous</i> version of `preregisterRequest:response:error`;
 *
 *  @param request An NSDictionary containing `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId` as keys.
 *                  You may also send the additional fields: `CMTUserProfileEnrollmentCode` to help the app display the right thing to the user.
 *                  Values must all be NSString.
 *  @param handler If not nil, handler will be called with an NSDictionary response, whether the fetch succeeded or failed, and an *error object.
 *
 *  @return NSOperation instance that you can use to cancel the operation
 *
 *  @since Available since 1.0.0
 *
 *  @deprecated Deprecated since 9.0.0, use preregisterWithRequest:handler:/preregister(withRequest: handler:) instead.
 */
-(nonnull NSOperation *)preregisterRequest:(NSDictionary *_Nonnull)request
                         completionHandler:(void (^_Nullable)(NSDictionary * _Nonnull response,
                                                              BOOL success, NSError * _Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use preregisterWithRequest:handler:/preregister(withRequest: handler:) instead ")));

/*!
 *  <i>Asynchronous</i> version of `preregisterRequest:response:error`;
 *
 *  @param request An NSDictionary containing `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId` as keys.
 *                  You may also send the additional fields: `CMTUserProfileEnrollmentCode` to help the app display the right thing to the user.
 *                  Values must all be NSString.
 *  @param handler If not nil, handler will be called with an NSDictionary response, whether the fetch succeeded or failed, and an *error object.
 *
 *  @since Available since 9.0.0
 */
-(void)preregisterWithRequest:(NSDictionary *_Nonnull)request
                      handler:(void (^_Nullable)(NSDictionary * _Nonnull response,
                                                 BOOL success, NSError * _Nullable error)) handler;

/*!
 *  <i>Synchronously</i> sends a register request to CMT's backend. If successful, the user becomes authenticated
 *  and `[CMTUser currentUser]` will point to this user. If the user is a client user, the user will also become linked.
 *  If the user is a tag user, `[CMTUser currentUser].tagUser` will return true.
 *
 *  @param request  An NSDictionary containing a CMT profile. The NSDictionary must have at least one of the following keys:
 *                  `CMTUserProfileEmailKey` or `CMTUserProfileZaId`. Additional fields that are part of the profile
 *                  may also be sent.
 *  @param response An NSDictionary with the `CMTUserProfileKey`.
 *                  The value for the `CMTUserProfileKey` will be an NSDictionary containing the user profile.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 * 
 *  @since Available since 1.0.0
 */
-(BOOL)registerRequest:(NSDictionary *_Nonnull)request
              response:(NSDictionary *_Nonnull *_Nonnull)response
                 error:(NSError *_Nullable *_Nullable)error;

/**
 <i>Asynchronous</i> version of `registerRequest:response:error`.

 @param request An NSDictionary containing a CMT profile. The NSDictionary must have at least one of the following keys:`CMTUserProfileEmailKey` or `CMTUserProfileZaId`. Additional fields that are part of the profile may also be sent.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @return NSOperation instance that can be used to cancel the request
 @since Available since 1.0.0
 @deprecated Deprecated since 9.0.0, use registerWithRequest:handler:/register(withRequest:handler:) instead.
 */
-(nonnull NSOperation *)registerRequest:(NSDictionary *_Nonnull)request
                      completionHandler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError *_Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use registerWithRequest:handler:/register(withRequest:handler:) instead ")));

/**
 <i>Asynchronous</i> version of `registerRequest:response:error`.
 
 @param request An NSDictionary containing a CMT profile. The NSDictionary must have at least one of the following keys:`CMTUserProfileEmailKey` or `CMTUserProfileZaId`. Additional fields that are part of the profile may also be sent.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @since Available since 8.0.0
 */
-(void)registerWithRequest:(NSDictionary *_Nonnull)request
                   handler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError *_Nullable error)) handler;

/*!
 *  <i>Synchronously</i> sends a register and activate request to CMT's backend. If successful, the user becomes authenticated
 *  and `[CMTUser currentUser]` will point to this user. The user will also become a client user automatically, so `[CMTUser currentUser].linked` will be true.
 *  If the user is a tag user, `[CMTUser currentUser].tagUser` will return true.
 *
 *  @param request  An NSDictionary containing a CMT profile and CMTUserProfileEnrollmentCode and CMTUserProfileBirthDate as keys. The profile must have one of the following keys `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId`. Additional fields that are part of the profile may also be sent.
 *  @param response An NSDictionary with the `CMTUserProfileKey`.
 *                  The value for the CMTUserProfileKey will be an NSDictionary containing the user profile.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 1.0.0
 */
-(BOOL)registerAndActivateRequest:(NSDictionary *_Nonnull)request
                         response:(NSDictionary *_Nonnull *_Nonnull)response
                            error:(NSError *_Nullable *_Nullable)error;

/**
 <i>Asynchronous</i> version of `registerAndActivateRequest:response:error`

 @param request An NSDictionary containing a CMT profile and `CMTUserProfileEnrollmentCode` and `CMTUserProfileBirthDate` as keys. The profile must have one of the following keys: `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId`. Additional fields that are part of the profile may also be sent.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @return NSOperation instance that can be used to cancel the request
 @since Available since 1.0.0
 @deprecated Deprecated since 9.0.0, use registerAndActivateWithRequest:handler:/registerAndActivate(withRequest:handler:) instead.
 */
-(nonnull NSOperation *)registerAndActivateRequest:(NSDictionary *_Nonnull)request
                                 completionHandler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError * _Nullable error)) handler
 __attribute__((deprecated("Deprecated since 9.0.0, use registerAndActivateWithRequest:handler:/registerAndActivate(withRequest:handler:) instead ")));

/**
 <i>Asynchronous</i> version of `registerAndActivateRequest:response:error`
 
 @param request An NSDictionary containing a CMT profile and `CMTUserProfileEnrollmentCode` and `CMTUserProfileBirthDate` as keys. The profile must have one of the following keys: `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId`. Additional fields that are part of the profile may also be sent.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @since Available since 9.0.0
 */
-(void)registerAndActivateWithRequest:(NSDictionary *_Nonnull)request
                              handler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError * _Nullable error)) handler;

/*!
 *  <i>Synchronously</i> performs a preactivation request. This is not strictly needed. Use only
 *  if you wish to show Terms and Conditions to the user before activation.
 *
 *  @param request  An NSDictionary containing `CMTUserProfileEnrollmentCodeKey` as key.
 *  @param response The passed in NSDictionary will contain `CMTUserProfileHandleSuffixKey` as key.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *
 * @since Available since 1.0.0
 */
-(BOOL)preactivateRequest:(NSDictionary *_Nonnull)request
                 response:(NSDictionary *_Nonnull *_Nonnull)response
                    error:(NSError *_Nullable *_Nullable)error;

/**
 <i>Asynchronous</i> version of `preactivateRequest:response:error`

 @param request An NSDictionary containing `CMTUserProfileEnrollmentCodeKey` as key.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @return NSOperation instance that can be used to cancel the request
 @since Available since 1.0.0
 @deprecated Deprecated since 9.0.0, use preactivateWithRequest:handler:/preactivate(withRequest:handler:) instead.
 */
-(nonnull NSOperation *)preactivateRequest:(NSDictionary *_Nonnull)request
                         completionHandler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError * _Nullable error)) handler
 __attribute__((deprecated("Deprecated since 9.0.0, use preactivateWithRequest:handler:/preactivate(withRequest:handler:) instead ")));

/**
 <i>Asynchronous</i> version of `preactivateRequest:response:error`
 
 @param request An NSDictionary containing `CMTUserProfileEnrollmentCodeKey` as key.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @since Available since 9.0.0
 */
-(void)preactivateWithRequest:(NSDictionary *_Nonnull)request
                      handler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError * _Nullable error)) handler;

/*!
 *   <i>Synchronously</i> sends an activate request to CMT's backend servers
 *
 *  @param request  An NSDictionary containing `CMTUserEnrollmentProfileCode` and `CMTUserProfileBirthDate` as keys. The values for
 *                  these fields are NSString *.
 *  @param response An NSDictionary containing a `CMTUserProfileKey` key. The value of the `CMTUserProfileKey` key is an
 *                  NSDictionary containing the profile. The user should become a client user at this point.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *
 * @since Available since 1.0.0
 */
-(BOOL)activateRequest:(NSDictionary *_Nonnull)request
              response:(NSDictionary *_Nonnull *_Nonnull)response
                 error:(NSError *_Nullable *_Nullable)error;

/*!
 *  <i>Asynchronous</i> version of `activateRequest:response:error`
 *
 *  @param request An NSDictionary containing `CMTUserProfileEnrollmentCode` and `CMTUserProfileBirthDate` as keys. The values for
 *                 these fields are NSString *.
 *  @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object
 *
 *  @return Handle to NSOperation instance that can be used to cancel the request
 *
 *  @since Available since 1.0.0
 *
 *  @deprecated Deprecated since 9.0.0, use activateWithRequest:handler:/activate(withRequest:handler:) instead.
 */
-(nonnull NSOperation *)activateRequest:(NSDictionary *_Nonnull)request
                      completionHandler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError *_Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use activateWithRequest:handler:/activate(withRequest:handler:) instead ")));

/*!
 *  <i>Asynchronous</i> version of `activateRequest:response:error`
 *
 *  @param request An NSDictionary containing `CMTUserProfileEnrollmentCode` and `CMTUserProfileBirthDate` as keys. The values for
 *                 these fields are NSString *.
 *  @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object
 *
 *  @since Available since 9.0.0
 */
-(void)activateWithRequest:(NSDictionary *_Nonnull)request
                   handler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError *_Nullable error)) handler;

/*!
 *  <i>Synchronously</i> sends initial login information to the server
 *
 *  @param request  The NSDictionary containing information that identifies the previously
 *                  registered user. The NSDictionary must have one of the following keys:
 *                  `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId`.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 1.0.0
 */
-(BOOL)loginStep1Request:(NSDictionary *_Nonnull)request
                   error:(NSError *_Nullable *_Nullable)error;

/**
 <i>Asynchronous</i> version of `loginStep1Request:error`

 @param request The NSDictionary containing information that identifies the previously registered user. The NSDictionary must have one of the following keys: `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId`.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @return Handle to NSOperation instance that can be used to cancel the request
 @since Available since 1.0.0
 @deprecated Deprecated since 9.0.0, use loginStep1WithRequest:handler:/loginStep1(withRequest:handler:) instead.
 */
-(nonnull NSOperation *)loginStep1Request:(NSDictionary *_Nonnull)request
                        completionHandler:(void (^_Nullable)(BOOL success, NSError *_Nullable error)) handler
__attribute__((deprecated("Deprecated since 9.0.0, use loginStep1WithRequest:handler:/loginStep1(withRequest:handler:) instead ")));

/**
 <i>Asynchronous</i> version of `loginStep1Request:error`
 
 @param request The NSDictionary containing information that identifies the previously registered user. The NSDictionary must have one of the following keys: `CMTUserProfileAccountId`, `CMTUserProfileEmailKey`, or `CMTUserProfileZaId`.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @since Available since 9.0.0
 */
-(void)loginStep1WithRequest:(NSDictionary *_Nonnull)request
                     handler:(void (^_Nullable)(BOOL success, NSError *_Nullable error)) handler;

/*!
 *  <i>Synchronously</i> sends login information to the server. If successful, the user becomes
 *  authenticated and `[CMTUser currentUser]` will point to this user.
 *
 *  @param request  The NSDictionary containing the authentication code sent to the user via e-mail.
 *  @param response An NSDictionary with the `CMTUserProfileKey`.
 *                  The value for the `CMTUserProfileKey` will be an NSDictionary containing the user profile.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *
 * @since Available since 1.0.0
 */
-(BOOL)loginStep2Request:(NSDictionary *_Nonnull)request
                response:(NSDictionary *_Nonnull *_Nonnull)response
                   error:(NSError *_Nullable *_Nullable)error;

/**
 <i>Asynchronous</i> version of `loginStep2Request:response:error`

 @param request The NSDictionary containing the authentication code sent to the user via e-mail.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @return Handle to NSOperation instance that can be used to cancel the request
 @since Available since 1.0.0
 @deprecated Deprecated since 9.0.0, use loginStep2WithRequest:handler:/loginStep2(withRequest:handler:) instead.
 */
-(nonnull NSOperation *)loginStep2Request:(NSDictionary *_Nonnull)request
                        completionHandler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError *_Nullable error)) handler
  __attribute__((deprecated("Deprecated since 9.0.0, use loginStep2WithRequest:handler:/loginStep2(withRequest:handler:) instead ")));

/**
 <i>Asynchronous</i> version of `loginStep2Request:response:error`
 
 @param request The NSDictionary containing the authentication code sent to the user via e-mail.
 @param handler If not nil, handler will be called with an NSDictionary response containing the profile whether the fetch succeeded or failed, and an *error object.
 @since Available since 9.0.0
 */
-(void)loginStep2WithRequest:(NSDictionary *_Nonnull)request
                     handler:(void (^_Nullable)(NSDictionary * _Nonnull response, BOOL success, NSError *_Nullable error)) handler;


/*!
 * <i>Asynchronously</i> requests the server to refresh the session.
 * @param authCode The authorization code (required)
 * @param handler If not nil, handler will be called with a success indicator, and an *error object.
 * @since Available since 7.0.0
 */
-(void)refreshSessionWithAuthCode:(NSString *_Nonnull)authCode
                completionHandler:(void (^_Nullable)(BOOL success, NSError *_Nullable error)) handler;

/*!
 *  <i>Synchronously</i> fetches the profile from the server if the user is authenticated
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return An NSDictionary containing the profile or nil if the fetch failed
 *
 *  @throws (Swift only) NSError
 *
 * @since Available since 1.0.0
 */
-(nullable NSDictionary *)loadProfile:(NSError *_Nullable *_Nullable)error;

/*!
 *  <i>Asynchronously</i> fetches the profile from the server if the user is authenticated
 *
 *  @param handler If not nil, the handler will be called when the call completes. A profile will be returned and
 *                 a success indicator. If true, the fetch succeeded and the error will be nil.
 *
 * @return A reference to the NSOperation executing the download task. This can be used to cancel the asynchronous task.
 *
 * @since Available since 1.0.4
 *
 * @deprecated Deprecated since 9.0.0, use loadProfileWithHandler:/loadProfile(handler:) instead.
 */
-(nonnull NSOperation *)loadProfileInBackgroundWithCompletionHandler:(void (^_Nullable)(NSDictionary * _Nonnull profile, BOOL success, NSError *_Nullable error))handler
 __attribute__((deprecated("Deprecated since 9.0.0, use loadProfileWithHandler:/loadProfile(handler:) instead ")));

/*!
 *  <i>Asynchronously</i> fetches the profile from the server if the user is authenticated
 *
 *  @param handler If not nil, the handler will be called when the call completes. A profile will be returned and
 *                 a success indicator. If true, the fetch succeeded and the error will be nil.
 *
 * @since Available since 9.0.0
 */
-(void)loadProfileWithHandler:(void (^_Nullable)(NSDictionary * _Nonnull profile, BOOL success, NSError *_Nullable error))handler;

/*!
 *  <i>Synchronously</i> updates the profile for the user
 *
 *  @param request  The desired updated profile
 *  @param response The updated profile as a dictionary
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 1.0.0
 */
-(BOOL)updateProfileRequest:(NSDictionary *_Nonnull)request
                   response:(NSDictionary *_Nonnull *_Nullable)response
                      error:(NSError *_Nullable *_Nullable)error;

/*!
 *  <i>Asynchronously</i> updates the profile of the user on the server if the user is authenticated
 *
 *  @param profile The NSDictionary containing the profile, @see method updateProfileRequest:response:error:
 *  @param handler The block to execute after update profile is completed.
 *
 *  @return A reference to the NSOperation executing the task. This can be used to cancel the asynchronous task.
 *
 *  @since Available since 1.0.0
 *
 *  @deprecated Deprecated since 9.0.0, use updateProfile:handler:/updateProfile(_:handler:) instead.
 */
-(nonnull NSOperation *)updateProfileInBackground:(NSDictionary *_Nonnull)profile
                                completionHandler:(void (^_Nullable)(NSDictionary * _Nonnull profile, BOOL success, NSError *_Nullable error))handler
 __attribute__((deprecated("Deprecated since 9.0.0, use updateProfile:handler:/updateProfile(_:handler:) instead ")));

/*!
 *  <i>Asynchronously</i> updates the profile of the user on the server if the user is authenticated
 *
 *  @param profile The NSDictionary containing the profile, @see method updateProfileRequest:response:error:
 *  @param handler The block to execute after update profile is completed.
 *
 *  @since Available since 9.0.0
 */
-(void)updateProfile:(NSDictionary *_Nonnull)profile
             handler:(void (^_Nullable)(NSDictionary * _Nonnull profile, BOOL success, NSError *_Nullable error))handler;

/*!
 *  <i>Synchronously</i> delete a user (merely updates the profile with email, za_id, or mobile set to NSNull)
 *
 *  @param error (Objective-C only) Set on failure.
 *  @return (Objective-C only) YES if successful, NO otherwise
 *  @throws (Swift only) NSError
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
-(BOOL)deleteUserWithError:(NSError *_Nullable *_Nullable)error __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));

/**
 *  <i>Asynchronously</i> delete a user
 *
 *  @param handler The block to execute after deleting a profile is completed. If successful, success will be TRUE, FALSE otherwise. In the latter case, the error will be set appropriately.
 *  @return A reference to the NSOperation executing the task. This can be used to cancel the asynchronous task.
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 9.0.0, use deleteUserWithHandler: instead.
 */
-(nonnull NSOperation *)deleteUserWithCompletionHandler:(void (^_Nullable)(BOOL success, NSError *_Nullable error))handler
 __attribute__((deprecated("Deprecated since 9.0.0, use deleteUserWithHandler: instead ")));

/**
 *  <i>Asynchronously</i> delete a user
 *
 *  @param handler The block to execute after deleting a profile is completed. If successful, success will be TRUE, FALSE otherwise. In the latter case, the error will be set appropriately.
 *
 *  @since Available since 9.0.0
 *  @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
-(void)deleteUserWithHandler:(void (^_Nullable)(BOOL success, NSError *_Nullable error))handler __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));


/*!
 *  <i>Synchronously</i> updates the tag mode of the user
 *
 *  @param tagUser  Set to YES to enable tag trip detection, NO enables phone-based trip detection.
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 1.9.0
 */
-(BOOL)setTagUser:(BOOL)tagUser error:(NSError *__autoreleasing _Nullable *_Nullable)error;

/*!
 *  <i>Asynchronously</i> updates the tag mode of the user on the server if the user is authenticated.
 *
 *  @discussion If tagUser is true, then the trip detector will be set to start trips based on tag connections. If tagUser is false, then the trip detector will be set to start using phone's sensors
 *
 *  @param tagUser Set to YES to enable tag trip detection, NO enables phone-based trip detection.
 *  @param handler The block to execute after update profile is completed. An error is passed if success is NO.
 *
 *  @return A reference to the NSOperation executing the task. This can be used to cancel the asynchronous task.
 *
 *  @since Available since 1.9.0
 *
 *  @deprecated Deprecated since 9.0.0, use setTagUserValue:handler:/setTagUserValue(_:handler:) instead.
 */
-(nonnull NSOperation *)setTagUserInBackground:(BOOL)tagUser
                             completionHandler:(void (^_Nullable)(BOOL success, BOOL tagMode, NSError *_Nullable error))handler
__attribute__((deprecated("Deprecated since 9.0.0, use setTagUserValue:handler:/setTagUserValue(_:handler:) instead ")));

/*!
 *  <i>Asynchronously</i> updates the tag mode of the user on the server if the user is authenticated.
 *
 *  @discussion If tagUser is true, then the trip detector will be set to start trips based on tag connections. If tagUser is false, then the trip detector will be set to start using phone's sensors
 *
 *  @param tagUser Set to YES to enable tag trip detection, NO enables phone-based trip detection.
 *  @param handler The block to execute after update profile is completed. An error is passed if success is NO.
 *
 *  @since Available since 9.0.0
 */
-(void)setTagUserValue:(BOOL)tagUser
               handler:(void (^_Nullable)(BOOL success, BOOL tagMode, NSError *_Nullable error))handler;

/**
 <i>Synchronously</i> logs out the user. If the user is the current user, +currentUser will return nil

 @param error Pointer to NSError object, set on failure
 @return (Objective-C only) Returns YES if logging out successfully, otherwise NO.
 @throws (Swift only) NSError
 @since Available since 1.0.0
 */
-(BOOL)logOutWithError:(NSError *__autoreleasing _Nullable *_Nullable)error;

/**
 <i>Synchronously</i> logs out the current user. +currentUser will return nil.

 @param error Pointer to NSError object, set on failure
 @return (Objective-C only) YES if successful, NO otherwise
 @throws (Swift only) NSError
 @since Available since 1.0.0
 */
+(BOOL)logOutWithError:(NSError *__autoreleasing _Nullable *_Nullable)error;

/*!
 *  Gets the currently logged in user (from persistent storage if necessary) and returns an instance of it
 *
 *  @return a CMTUser that is the currently logged in user. If there is none, returns nil
 *
 * @since Available since 1.0.0
 */
+(nullable CMTUser *)currentUser;

/**
 Creates a new CMTUser object.

 @return a new CMTUser object
 
 @since Available since 2.1.0
 */
+(nonnull CMTUser *)createUser;

/*!
 * Creates an authenticated CMTUser object
 * 
 * @discussion This method makes a synchronous network call. You may call the method on a background thread, 
 * but be sure that calls that depend on a valid CMTUser don't happen until this method returns.
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return a new CMTUser object that is authenticated or nil if authentication fails
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 1.9.0
 */
+(nullable CMTUser *)authenticateWithoutCredentialsWithError:(NSError *__autoreleasing _Nullable *_Nullable)error;

/*!
 * <i>Asynchronous</i> version of the `authenticateWithoutCredentials` method
 *
 * @param handler Handler will get called with a CMTUser instance. If authentication fails, the user parameter will be nil and the error parameter will be set
 *
 * @return an NSOperation object that points to the task
 *
 * @since Available since 1.9.0
 */
+(nonnull NSOperation *)authenticateWithoutCredentialsInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTUser * _Nullable user, NSError *_Nullable error)) handler;

/*!
 * Call this method if the App is using `CLLocationManager` to monitor circular geographic regions via `CLCircularRegion`s. Call this method shortly AFTER the
 * call to `CLLocationManager stopMonitoringForRegion:`
 *
 * @param region The region for which monitoring has been stopped.
 *
 * @since Available since 9.1.0
 */
-(void)didStopMonitoringForRegion:(CLRegion *_Nonnull)region;

@end

