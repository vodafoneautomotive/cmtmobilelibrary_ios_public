//
// CMTFriendInvitesManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTUserBaseDataProvider.h"
#import "CMTFriendInviteModel.h"
#import "CMTFriendRequestsSentModel.h"
#import "CMTFriendResponseModel.h"

/*!
 *  Name of the error domain for CMTFriendInvitesManager.
 *  @since Available since 0.1.0
 */
extern NSString * _Nonnull const CMTFriendInvitesManagerErrorDomain;

/**
 Error codes when the invite manager has a problem.
 
 @since Available since 0.1.0
 @discussion Deprecated since 9.0.0.
 */
typedef NS_ENUM(NSInteger, CMTFriendInvitesManagerError){
    ///  Returned when de-serialization of the response into a object fails (Available since 0.1.0)
    CMTFriendInvitesManagerErrorDeserializationFailed = -1
};

/*!
 *  Helps manage friend invitations sent by user
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendInvitesManager : CMTUserBaseDataProvider

/*!
 *  Friend invitations sent by user
 *  @since Available since 0.1.0
 */
@property (readonly,strong,nullable) CMTFriendRequestsSentModel *invitationsSent;

/**
 App developers should not attempt to create an instance of `CMTFriendInvitesManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Fetch the invitations sent by the user
 *  @since Available since 1.0.5
 *  @param handler          Reference to handler that will be called when fetching of invitations is complete. The handler will
 *                          be passed the updated CMTFriendRequestsSentModel object, whether the request succeeded, and an NSError object if it failed.
 *  @return A reference to the NSOperation object that that is executing the fetch
 */
-(nonnull NSOperation *)loadSentInvitationsInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTFriendRequestsSentModel * _Nullable requestsSent, BOOL success, NSError * _Nullable error))handler;

/*!
 *  Cancel invitations sent in the invitationsArray
 *  @since Available since 1.0.5
 *  @param invitationsArray An NSArray of CMTFriendResponseModel objects. Each object should have
 *                          the inviteId field populated so that the server will know which invites to cancel.
 *  @param handler          Reference to handler that will be called when cancelations of invitations is complete. The handler will
 *                          be passed the updated CMTFriendRequestsSentModel object, whether the request succeeded, and an NSError object if it failed.
 *  @return A reference to the NSOperation object that that is executing the fetch
 */
-(nonnull NSOperation *)cancelSentInvitationsInBackground:(NSArray <CMTFriendResponseModel *> *_Nonnull)invitationsArray
                                        completionHandler:(void (^_Nullable)(CMTFriendRequestsSentModel * _Nullable requestsSent, BOOL success, NSError * _Nullable error)) handler;

/*!
 *  Send invitations for all people in the invitationsArray
 *  @since Available since 1.0.5
 *  @param invitationsArray An NSArray of CMTFriendInviteModel objects.
 *  @param handler          Reference to handler that will be called when sending of invitations is complete. The handler will
 *                          be passed the updated CMTFriendRequestsSentModel object, whether the request succeeded, and an NSError object if it failed.
 *  @return A reference to the NSOperation object that that is executing the fetch
 */
-(nonnull NSOperation *)sendInvitationsInBackground:(NSArray <CMTFriendInviteModel *> *_Nonnull)invitationsArray
                                  completionHandler:(void (^_Nullable)(CMTFriendRequestsSentModel * _Nullable requestsSent, BOOL success, NSError * _Nullable error)) handler;

/*!
 *  Search users by username
 *  @since Available since 1.4.1
 *  @param prefix   An NSString of username prefix entered by user
 *  @param handler  Reference to handler that will be called when searching username is complete. The handler will
 *                  be passed an array of usernames matching the prefix, whether the request succeeded, and an NSError object if it failed.
 *  @return A reference to the NSOperation object that that is executing the fetch
 */
- (nonnull NSOperation *)searchUsersByUsernameInBackground:(NSString *_Nonnull)prefix completionHandler:(void(^_Nullable)(NSArray<NSString *> * _Nullable users, BOOL success, NSError * _Nullable error))handler;

@end
