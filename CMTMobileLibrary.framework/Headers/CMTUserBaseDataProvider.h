//
// CMTUserBaseDataProvider.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTBaseDataProvider.h"

@class CMTUser;

/**
 *  Used to fetch data from the server from APIs that require a user Id
 *
 *  @since Available since 1.0.0
 */
@interface CMTUserBaseDataProvider : CMTBaseDataProvider
/**
 App developers should not attempt to create an instance of `CMTUserBaseDataProvider` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 *  Constructor to initialize a data provider / manager with the given user id
 *
 *  @param user authenticated CMTUser
 *
 *  @return Instance of type `CMTUserBaseDataProvider`
 *
 *  @since Available since 1.5.0
 */
-(nonnull instancetype)initWithUser:(CMTUser *_Nonnull)user NS_DESIGNATED_INITIALIZER;

/**
 *  Handle to the cached version of the response, if available
 *
 *  @since Available since 1.0.0
 */
@property (readonly,nonnull) id fileCache;

/**
 * CMTUser reference
 *
 * @since Available since 1.5.0
 */
@property (readonly, atomic, nullable, weak) CMTUser *user;

@end
