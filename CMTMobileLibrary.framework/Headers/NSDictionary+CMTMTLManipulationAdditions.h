//
// NSDictionary+CMTMTLManipulationAdditions.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/// CMTMTLManipulationAdditions category for NSDictionary
@interface NSDictionary (CMTMTLManipulationAdditions)

/**
 Merges the keys and values from the given dictionary into the receiver. If
 both the receiver and `dictionary` have a given key, the value from
 `dictionary` is used.

 @param dictionary Input dictionary
 @return a new dictionary containing the entries of the receiver combined with
 those of `dictionary`.
 */
- (NSDictionary *)cmt_mtl_dictionaryByAddingEntriesFromDictionary:(NSDictionary *)dictionary;

/**
 Creates a new dictionary with all the entries for the given keys removed from the receiver.
 @param keys Array of keys to remove
 @return A new dictionary with all of the keys and associated values removed
 */
- (NSDictionary *)cmt_mtl_dictionaryByRemovingValuesForKeys:(NSArray *)keys;

@end

@interface NSDictionary (CMTMTLManipulationAdditions_Deprecated)

- (NSDictionary *)cmt_mtl_dictionaryByRemovingEntriesWithKeys:(NSSet *)keys __attribute__((deprecated("Replaced by -cmt_mtl_dictionaryByRemovingValuesForKeys:")));

@end
