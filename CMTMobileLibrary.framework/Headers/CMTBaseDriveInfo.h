//
// CMTBaseDriveInfo.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//


#import "CMTMantle.h"
#import <CoreLocation/CoreLocation.h>
#import "CMTTripLabelModel.h"
#import "CMTDataTypes.h"

/*!
 *  `CMTBaseDriveInfo` includes the base properties of a trip.
 *  @since Available since 0.2.51
 */
@interface CMTBaseDriveInfo : CMTMTLModel<CMTMTLJSONSerializing>

/*! The unique identifier of the trip.
 *  @since Available since 0.2.51
 */
@property (nullable) NSString *driveId;

/*! UTC offset in seconds (as an integer). This property can be used to tell the user about the time zone in which the trip was recorded.
 *  @since Available since 0.2.51
 */
@property (nullable) NSNumber *utcOffset;

/*!
 When the trip started, in UTC time.
 @since Available since 0.2.51
 */
@property (nullable) NSDate *startTime;

/*!
 The starting location of the trip.
 @since Available since 0.2.51
 */
@property (nullable) CLLocation *startLocation;

/*!
 The reverse geocoded name of the location at `startLocation`. If this is nil or an empty string, there are APIs in the `CMTTripListDataManager` class
 that will get the reverse geocoded name using the Apple APIs and persist the name for future display.

 @since Available since 0.2.51
 */
@property (nullable) NSString *startLocationName;

/*! The reverse geocoded CLPlacemark of the location at `startLocation`. If this is nil or an empty string, there are
    APIs in the `CMTTripListDataManager` class that will get the reverse geocoded CLPlacemark using the Apple APIs and
    persist the CLPlacemark for future display.
 *  @since Available since 6.0.0
 */
@property (nullable) CLPlacemark *startLocationPlacemark;

/*! When the trip ended in UTC time.
 *  @since Available since 0.2.51
 */
@property (nullable) NSDate *endTime;

/*!
 Ending location of the trip.
 @since Available since 0.2.51
 */
@property (nullable) CLLocation *endLocation;

/*!
 The reverse geocoded name of the location at `endLocation`. If this is nil or an empty string, there are APIs in the `CMTTripListDataManager` class
 that will get the reverse geocoded name using the Apple APIs and persist the name for future display.
 @since Available since 0.2.51
 */
@property (nullable) NSString *endLocationName;

/*!
 The reverse geocoded CLPlacemark of the location at `endLocation`. If this is nil or an empty string, there are
 APIs in the `CMTTripListDataManager` class that will get the reverse geocoded CLPlacemark using the Apple APIs and
 persist the CLPlacemark for future display.

 @since Available since 6.0.0
*/
@property (nullable) CLPlacemark *endLocationPlacemark;

/*!
 Trip length in kilometers (floating point number).

 @since Available since 0.2.51
*/
@property (nullable) NSNumber *tripLength;

/*!
 The mac address or id of the tag as a `uint64_t` if the trip was recorded with a tag. Otherwise nil.

 @since Available since 0.2.51
 */
@property (nullable) NSNumber *tagId;

/*!
 If the trip was recorded with a tag, this flag is set to TRUE if the phone wasn't in the car at the time the trip was recorded.
 @since Available since 0.3.2
 */
@property BOOL tagOnly;

/*!
 Returns TRUE if the CMT backend believes that the trip should be hidden.
 @since Available since 0.2.52
 */
@property (nullable)  NSNumber *shouldHide;

/*!
 Indicates how this trip was started as a `CMTStartReasonType`.
 @since Available since 0.2.52
 */
@property CMTStartReasonType startReason;

/*!
Indicates how this trip was stopped as a `CMTStopReasonType`.
@since Available since 0.2.52
*/
@property CMTStopReasonType stopReason;

/*!
 Returns a `CMTTripLabelModel` corresponding to a user provided label for this trip.
 @discussion If the user labeled this trip, the value will be non nil and will include the label the user provided.
 */
@property (nullable) CMTTripLabelModel *userLabel;

/*!
 Returns the processing status of the trip as a `CMTTripProcessingStatusType`.
 @discussion When `status == CMTTripProcessingStatusTypeProcessed`, the trip has been processed by the server.
 @since Available since 0.2.51
 */
@property CMTTripProcessingStatusType status;

@end
