//
// CMTFriendInviteSentModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 *  Model that tracks invites that have already been sent
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendInviteSentModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Email address of the person the user invited
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *toEmail;

/*! Phone number of the person the user invited
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *toMobile;

/*! Facebook id of the person the user invited
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *toFacebookId;

/*! Name of the person the user invited
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSString *toName;

/*! Date that the invitation was sent
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSDate *inviteDate;

/*! Boolean flag indicating whether the person you sent invitation is registered to use the app
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSNumber *isRegistered;

/*! Integer identifier for this invitation
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSNumber *inviteId;

@end
