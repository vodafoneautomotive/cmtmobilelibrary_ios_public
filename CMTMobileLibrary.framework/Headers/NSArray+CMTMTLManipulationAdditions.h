//
// NSArray+CMTMTLManipulationAdditions.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/// CMTMTLManipulationAdditions category for NSArray
@interface NSArray (CMTMTLManipulationAdditions)

/**
 The first object in the array or nil if the array is empty.
 Forwards to `firstObject` which has been first declared in iOS7, but works with iOS4/10.6.
 */
@property (nonatomic, readonly, strong) id cmt_mtl_firstObject;

/**
 Returns a new array without all instances of the given object.
 @param object the given object
 @return a new array without all instances of the given object
 */
- (NSArray *)cmt_mtl_arrayByRemovingObject:(id)object;

/**
 Returns a new array without the first object. If the array is empty, it returns the empty array.
 @return a new array without the first object. If the array is empty, it returns the empty array
 */
- (NSArray *)cmt_mtl_arrayByRemovingFirstObject;

/**
 Create a new array without the last object. If the array is empty, it returns the empty array.
 @return a new array without the last object. If the array is empty, it
 returns the empty array
 */
- (NSArray *)cmt_mtl_arrayByRemovingLastObject;

@end
