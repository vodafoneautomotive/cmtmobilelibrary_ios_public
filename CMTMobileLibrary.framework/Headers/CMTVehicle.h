//
// CMTVehicle.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

@class CMTVehicleDescription;

/**
 Data model to hold information about a vehicle. The information includes a unique vehicle identifier, as well
 as data about the vehicle’s tag, and other items related to the driver’s policy.
 */
@interface CMTVehicle : CMTMTLModel<CMTMTLJSONSerializing>

/**
 A unique identifier that can be used to identify the
 vehicle. Could be null.
 @since Available since 4.0.0
 */
@property (nonatomic, nullable, readonly) NSNumber *shortVehicleIdentifier;

/**
 Allow all drivers on this vehicle; default = NO
 @since Available since 5.0.0
 */
@property (nonatomic, readonly) BOOL allowAllDrivers;

/**
 The short user ID of the primary driver for this vehicle; integer.
 @since Available since 5.0.0
 */
@property (nonatomic, nullable, readonly) NSNumber *primaryDriverShortUserIdentifier;

/**
 The MAC address of the tag associated with this vehicle.
 @since Available since 4.0.0
 */
@property (nonatomic, nullable, readonly) NSString *tagMacAddress;

/**
 Boolean indicating tag linking and activation. A YES value indicates that the tag has connected to the SDK and successfully registered
 a tag connection with the CMT backend server; NO otherwise.
 @since Available since 5.0.0
 */
@property (nonatomic, readonly) BOOL tagIsActive;

/**
 Date that tag linking is completed. Otherwise nil.
 @since Available since 5.0.0
 */
@property (nonatomic, nullable, readonly) NSDate *tagLinkDate;

/**
 Trips from this vehicle won't be scored until after the specified `effectiveDate`. A
 nil value indicates there is no effective date limitation.
 @since Available since 4.0.0
 */
@property (nonatomic, nullable, readonly) NSDate *effectiveDate;

/**
 The date after which trips will not be recorded. A nil value for `expirationDate` indicates
 there is no expiration date limitation.
 @since Available since 4.0.0
 */
@property (nonatomic, nullable, readonly) NSDate *expirationDate;

/**
 The date after which trips will be recorded. A nil value indicates
 there is no start recording date limitation.
 @since Available since 4.0.0
 */
@property (nonatomic, nullable, readonly) NSDate *startRecordingDate;

/**
 Contains the response data

 @discussion If additional custom fields are added, they will appear in the responseData after conversion.
 @since Available since 5.0.0
 */
@property (nonatomic, nullable, readonly) NSData *responseData;

/**
 Description of the vehicle (for convenience only)

 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) CMTVehicleDescription *vehicleDescription;

/**
 App developers should not attempt to create an instance of `CMTVehicle` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

@end
