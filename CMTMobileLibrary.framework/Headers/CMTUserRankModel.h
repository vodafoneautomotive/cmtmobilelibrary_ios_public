//
// CMTUserRankModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

@class CMTBadgeModel;
/**
 *  User rank information
 *  @since Available since 1.0.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTUserRankModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! State to display: Could be null, if null, don't display
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSString *state;

/*! Rank (integer)
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSNumber *rank;

/*! Nickname provided by friend
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSString *name;

/*! Score (float)
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSNumber *score;

/*! Distance (float) in km
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSNumber *distance;

/*! Indicates if this user is you (BOOL)
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSNumber *you;

/*! Custom description field could contain arbitrary text
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSString *customDescription;

/*! Integer value friend id
 *  @since Available since 1.0.0
 */
@property (nonatomic, nullable) NSNumber *friendId;

/*! User profile photo url
 *  @since Available since 1.5.0
 */
@property (nonatomic, nullable) NSString *photoURL;

/*! Badges this user owns
 *  @since Available since 1.5.0
 */
@property (nonatomic, nullable) NSArray<CMTBadgeModel *> *badges;

@end
