//
// CMTFriendRequestsSentModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

#import "CMTFriendFacebookInviteModel.h"
#import "CMTFriendEmailInviteModel.h"
#import "CMTFriendMobileInviteModel.h"
#import "CMTFriendInviteSentModel.h"

/*!
 *  JSON response from server
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendRequestsSentModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Array of CMTFriendInviteSentModel objects, invitations sent by the server
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSArray <CMTFriendInviteSentModel *> *invitesSent;

/*! Array of CMTFriendEmailInviteModel objects, invitations to be sent by user via email
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSArray <CMTFriendEmailInviteModel *> *emailInvitesToSend;

/*! Array of CMTFriendMobileInviteModel objects, invitations to be sent by user via SMS
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSArray <CMTFriendMobileInviteModel *> *mobileInvitesToSend;

/*! Array of CMTFriendFacebookInviteModel objects, invitations to be sent by user ia Facebook
 *  @since Available since 0.1.0
 */
@property (nonatomic,nullable) NSArray <CMTFriendFacebookInviteModel *> *facebookInvitesToSend;

@end
