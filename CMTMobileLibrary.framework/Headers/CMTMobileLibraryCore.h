//
// CMTMobileLibraryCore.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

#import "CMTServerConfiguration.h"
#import "CMTMarketingMaterialsDataProvider.h"
#import "CMTEventLogger.h"
#import "CMTDeviceManager.h"
#import "CMTVersionState.h"
#import "CMTMobileLibraryConfigurationModel.h"

@class CMTFilterEngineCore;
/**
 Error domain for `CMTMobileLibraryCore`
 */
extern NSString * _Nonnull const CMTMobileLibraryCoreErrorDomain;

/**
 Error code reported if SDK fails to initialize
 
 @since Available since 1.8.1
 */
typedef NS_ENUM(NSInteger, CMTMobileLibraryCoreErrorType) {
    /// Unknown error (Available since 1.8.1)
    CMTMobileLibraryCoreErrorUnknown = 0,
    /// Unable to initialize the CMTFilterEngineIOS library (Available since 1.8.1)
    CMTMobileLibraryCoreErrorInitializationError = 1,
    /// Set if we aren't able to set file protection to None (Available since 1.8.1)
    CMTMobileLibraryCoreErrorBadFileProtectionSetting = 2,
    /// Indicates server configuration was nil (Available since 2.0.0)
    CMTMobileLibraryCoreErrorServerConfigurationIsNil = 3,
    /// Indicates there's an error parsing configuration model (Available since 3.0.0)
    CMTMobileLibraryCoreErrorConfigurationModelParseError = 4,
    /// Indicates there's an error in initializing the CMTDeviceManager (Available since 7.0.0)
    CMTMobileLibraryCoreErrorDeviceManagerInitializationError = 5
};

/**
 `CMTMobileLibraryCore` handles the configuration of the CMT SDK. This class includes methods to initialize the CMT SDK.
 When initializing the CMT SDK, we recommend you do so synchronously on the main thread during
 `-[application:didFinishLaunchingWithOptions:]`.
 For example:  `[CMTMobileLibraryCore initializeWithServerConfiguration:withLibraryConfiguration:withLaunchOptions:withError:]`

 @since Available since 1.0.0
 */
@interface CMTMobileLibraryCore : NSObject

/*! Return a reference to the server configuration object
 *  @since Available since 1.0.0
 */
@property (readonly, nullable) CMTServerConfiguration *serverConfiguration;

/*! Return a reference to the marketing materials data provider 
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
@property (readonly, nullable) CMTMarketingMaterialsDataProvider *marketingDataProvider __attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")));

/*! Return the device manager 
 *  @since Available since 1.0.0
 */
@property (readonly, nullable) CMTDeviceManager *deviceManager;

/*! Return the version state 
 *  @since Available since 1.4.1
 */
@property (readonly, nullable) CMTVersionState *versionInfo;

/*! App version; if not nil or empty, app will use this string to annotate trips and calls to application server 
 *  @since Available since 1.2.7
 */
@property (nonatomic, nullable) NSString *appVersion;

/*! A reference to the CMTEventLogger object 
 *  @since Available since 1.0.0
 */
@property (readonly, nullable) CMTEventLogger *eventLogger;

/*! Launch time 
 *  @since Available since 1.0.0
 */
@property (readonly, nullable) NSDate *launchDate;

/*! Launch options 
 *  @since Available since 1.0.0
 */
@property (readonly, nullable) NSDictionary <UIApplicationLaunchOptionsKey, id> *launchOptions;

/*!
 * The configuration provided by the app to configure the SDK.
 *  @since Available since 1.9.0
 */
@property (readonly, nullable) CMTMobileLibraryConfigurationModel *configurationModel;

/**
 App developers should not attempt to create an instance of `CMTMobileLibraryCore` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Reset library
 *
 *  Stops background threads that are independent of users.
 *  @since Available since 1.0.0
 */
-(void) resetLibrary;


/**
 Call this method when the application receives a push notification AND you want the push notification to be handled
 by the library.
 
 @param application The currently running UIApplication object, ignored by the library
 @param userInfo The userInfo data received by the UIApplication object
 @return true if the mobile library handles the push notification
 @since Available since 1.0.7
 @deprecated Deprecated since 9.0.0, use libraryHandlesRemoteNotification: and applicationDidReceiveRemoteNotification:fetchCompletionHandler:
 */
-(BOOL) application:(UIApplication *_Nonnull)application didReceiveRemoteNotification:(NSDictionary *_Nonnull)userInfo __attribute__((deprecated("Deprecated since 9.0.0, use libraryHandlesRemoteNotification: and applicationDidReceiveRemoteNotification:fetchCompletionHandler: instead")));

/**
 Call this method when the application receives a push notification AND you want the push notification to be handled
 by the library.

 @param application The currently running UIApplication object, ignored by the library
 @param userInfo The userInfo data received by the UIApplication object
 @param completionHandler The handler is not called by the library and should be called by the caller instead
 @return true if the mobile library handles the push notification
 @since Available since 1.0.7
 @deprecated Deprecated since 9.0.0, use libraryHandlesRemoteNotification: and applicationDidReceiveRemoteNotification:fetchCompletionHandler:
 */
-(BOOL) application:(UIApplication *_Nonnull)application didReceiveRemoteNotification:(NSDictionary *_Nonnull)userInfo fetchCompletionHandler:(void (^_Nullable)(UIBackgroundFetchResult))completionHandler __attribute__((deprecated("Deprecated since 9.0.0, use libraryHandlesRemoteNotification: and applicationDidReceiveRemoteNotification:fetchCompletionHandler: instead")));

/**
 Call this method when the application receives a push notification to determine whether the push notification should be handled
 by the library.

 @param userInfo The userInfo data received by the UIApplication object
 @return true if the mobile library handles the push notification
 @since Available since 9.0.0
 */
-(BOOL)libraryHandlesRemoteNotification:(NSDictionary *_Nonnull)userInfo;

/**
 Call this method when the application receives a push notification that you want to be handled by the library,
 i.e. if libraryHandlesRemoteNotification was true

 @param userInfo The userInfo data received by the UIApplication object
 @param completionHandler The handler to be called by the library
 @return true if the library called the completion handler
 @since Available since 9.0.0
 */
-(BOOL)applicationDidReceiveRemoteNotification:(NSDictionary *_Nonnull)userInfo fetchCompletionHandler:(void (^_Nonnull)(UIBackgroundFetchResult))completionHandler;

/**
 Performs fetch in background.
 
 @param completionHandler Method to invoke after the library is done performing background fetches.
 @discussion Call this method in your `[UIApplication application:performFetchWithCompletionHandler:]` method to allow the library to use background fetch to upload / download data in the background.
 @since Available since 1.0.0
 */
-(void)performFetchWithCompletionHandler:(void (^_Nonnull)(UIBackgroundFetchResult))completionHandler;

/*!
 *  Configure the library
 *
 *  @param serverConfig     instance of CMTServerConfiguration
 *  @param libraryConfig    instance of CMTMobileLibraryConfigurationModel
 *  @param launchOptions    Dictionary explaining how app was started
 *  @param error            (Objective-C only) Set on failure.
 *  @return                 The shared CMTLibrary instance, or nil on error
 *  @throws                 (Swift only) NSError
 *  @discussion             Any error returned or thrown by this method indicates that the library has not been
 *                          properly initialized and will not function properly. At a minimum, the error should
 *                          be logged. Initialization can be retried, the app can be terminated (not recommended
 *                          by Apple), or the user can be prompted to restart the app.
 *                          This method should be called before calling sharedInstance.
 *  @since Available since 2.0.0
 */
+(nullable CMTMobileLibraryCore *)initializeWithServerConfiguration:(CMTServerConfiguration *_Nonnull)serverConfig
                                           withLibraryConfiguration:(CMTMobileLibraryConfigurationModel *_Nullable)libraryConfig
                                                  withLaunchOptions:(NSDictionary <UIApplicationLaunchOptionsKey, id> *_Nullable)launchOptions
                                                          withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/*!
 *  Configure the library
 *
 *  @param serverConfig     instance of CMTServerConfiguration
 *  @param launchOptions    Dictionary explaining how app was started
 *  @param error            (Objective-C only) Set on failure.
 *  @return                 The shared CMTLibrary instance, or nil on error
 *  @throws                 (Swift only) NSError
 *  @discussion             Any error returned or thrown by this method indicates that the library has not been
 *                          properly initialized and will not function properly. At a minimum, the error should
 *                          be logged. Initialization can be retried, the app can be terminated (not recommended
 *                          by Apple), or the user can be prompted to restart the app.
 *                          This method should be called before calling sharedInstance.
 *  @since Available since 2.0.0
 */
+(nullable CMTMobileLibraryCore *)initializeWithServerConfiguration:(CMTServerConfiguration *_Nonnull)serverConfig
                                                  withLaunchOptions:(NSDictionary <UIApplicationLaunchOptionsKey, id> *_Nullable)launchOptions
                                                          withError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/*!
 *  Developers should interact with the CMT Mobile Library using the `sharedInstance`
 *
 *  @return                 The shared CMTLibrary instance.
 *  @discussion             This method will return nil until one of the initialization methods has been called.
 *  @since Available since 1.0.0
 */
+(nonnull CMTMobileLibraryCore *) sharedInstance;

@end
