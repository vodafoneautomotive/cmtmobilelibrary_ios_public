//
// CMTDeviceManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
/*!
 *  NSNotification type, posted when the device id changes
 *  @since Available since 0.1.0
 */
extern NSString* const CMTDeviceManagerDeviceIdDidChangeNotification;

/*!
 *  Class to manage device identifier creation
 *  @since Available since 0.1.0
 */
@interface CMTDeviceManager : NSObject

/** @name: Properties */
/*! device id associated with this device
 *  @since Available since 0.3.2
*/
@property (atomic, readonly) NSString *deviceIdentifier;

/*! shortened device id
 *  @since Available since 1.3.0
 */
@property (atomic, readonly) NSString *shortDeviceIdentifier;

/*! device id did change
 *  @since Available since 1.0.0
 */
@property BOOL deviceIdDidChange;

/*! TRUE when a device id is created at app launch
 *  @since Available since 0.1.0
 *  @deprecated "Deprecated since 9.2.0; use appWasInstalled instead"
 */
@property (atomic, readonly) BOOL firstInstall __attribute__((deprecated("Replaced by appWasInstalled")));

/*! YES when an app is installed on a device
 *  @discuss When an app that uses the CMTMobileLibrary is installed on a device,
 *  this flag will be true at app launch.
 *  In subsequent app launches, this flag will be false.
 *  @since Available since 9.2.0
 */
@property (atomic, readonly) BOOL appWasInstalled;

@end
NS_ASSUME_NONNULL_END
