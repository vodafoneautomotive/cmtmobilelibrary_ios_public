//
// CMTTripDataManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#import "CMTDataTypes.h"

@class CMTUser;
@class CMTTripDetector;
@class CMTTripListDataManager;
@class CMTTagDataManager;

/*!
 *  @typedef CMTTelematicsDataSendingUploadCallback
 *
 *  @param shouldUpload Whether trip uploading should be allowed or not
 *  @since Available since 1.0.0
 */
typedef void (^CMTTelematicsDataSendingUploadCallback)(BOOL shouldUpload);

/*!
 * @typedef CMTTelematicsDataSendingUploadFilter
 *  A function that determines whether an upload can proceed or not
 *
 *  @param connectedToWiFi True if we are connected to Wi-Fi
 *  @param callback        Callback to the library to allow trip sending to proceed or not
 *
 *  @since Available since 1.0.0
 */
typedef void (^CMTTelematicsDataSendingUploadFilter)(BOOL connectedToWiFi, CMTTelematicsDataSendingUploadCallback _Nonnull callback);

/*!
 * A `CMTTripDataManager` object exposes APIs to enable and disable the `CMTTripDetector`, manage 
 * Tag data and provide a trip list of all processed and unprocessed trips, except in the case
 * where there is a currently recording trip.
 *
 * @since Available since 1.0.0
 */
@interface CMTTripDataManager : NSObject

/**
 App developers should not attempt to create an instance of `CMTTripDataManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Create an instance of a CMTTripDataManager object
 *
 *  @param user A CMTUser object
 *  @return An instance of a CMTTripDataManager object
 * 
 *  @since Available since 1.0.0
 */
-(nonnull instancetype) initWithUser:(CMTUser *_Nonnull)user;

/*!
 *  Clears the `tagDataManager`, `tripDetector` and `tripListDataManager` properties
 * 
 *  @since Available since 1.0.0
 */
-(void)resetTripDataManager;

/*!
 A reference to a `CMTTagDataManager` object
 @since Available since 1.0.0
 */
@property (readonly,nullable) CMTTagDataManager *tagDataManager;

/*!
 A reference to a `CMTTripDetector` object
 @since Available since 1.0.0
 */
@property (readonly,nullable) CMTTripDetector *tripDetector;

/*!
 A reference to a `CMTTripListDataManager` object
 @since Available since 1.0.0
 */
@property (readonly,nullable) CMTTripListDataManager *tripListDataManager;

/*! 
 * Send trip data to server using WiFi only, default = NO
 * @discussion Previously, this was not persisted across launches. As of SDK 9.0.0, the SDK will persist what value is set here across launches, so after the first time this value is set, the app does not need to take care of persisting this value and setting at launch. When a user logs out or is deauthorized, this value will default to NO.
 * @since Available since 1.0.0
 */
@property BOOL uploadUsingWiFiOnly;

/*!
 * If defined, the uploadFilter block will be called once per attempt to upload to inform the upload
 *  instance whether to allow the upload. The uploadFilter will be called frequently, so the block should
 *  not do a lot of work or make network calls.
 *
 * @since Available since 1.0.0
 */
@property (strong, atomic) CMTTelematicsDataSendingUploadFilter _Nullable uploadFilter;

@end
