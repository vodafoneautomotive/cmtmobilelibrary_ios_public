//
// CMTMobileResultsModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTBaseResultsModel.h"
#import "CMTEventModel.h"

/*!
 *  This model provides event and waypoint data for processed trips. It contains data for the detailed trajectory
 *  of a trip, as well as a list of events (CMTEventModel objects), such as braking, cornering, acceleration, speeding, and so on.
 *
 *  @since Available since 1.0.0
 */
@interface CMTMobileResultsModel : CMTBaseResultsModel<CMTMTLJSONSerializing>

/*! Array of CMTEventModel objects @see CMTEventModel.h 
 *  @since Available since 1.0.0
 */
@property (nullable) NSArray <CMTEventModel *> *events;

@end
