//
// CMTTagDataManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTUserBaseDataProvider.h"

@class CMTUser, CMTTagVehicleInfoModel, CMTTagConnectionModel, CMTTagStateModel, CMTTagVehicleLocationInfoModel;

/**
 Error domain for `CMTTagDataManager`
 
 @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTTagDataManagerErrorDomain;

/**
 Error codes for `CMTTagDataManagerErrorTypes` class

 @since Available since 1.0.0

 */
typedef NS_ENUM(NSInteger, CMTTagDataManagerErrorTypes) {
    /// Tag not found error (Available since 1.0.0)
    CMTTagDataManagerErrorTagNotFound = 0
};

/**
 Class for managing data related to tags, such as obtaining tag connection information, loading a list of linked vehicles, checking the link state of a tag, and so on.
 
 @since Available since 1.0.0
 */
@interface CMTTagDataManager : CMTUserBaseDataProvider

/**
 App developers should not attempt to create an instance of `CMTTagDataManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 *  Reset the instance, clear local cache and cancel all pending network operations
 *  @since Available since 1.0.0
 */
-(void) resetTagDataManager;

/**
 ** TAG CONNECTION
 **/

/*!
 *  A dictionary mapping tagId to the most recent connection information for every tag.
 *  @since Available since 1.0.0
 */
@property (readonly, atomic, nullable) NSDictionary <NSNumber *, CMTTagConnectionModel *> *mostRecentTagConnectionDictionary;


/*!
 * Returns tag connection info for the currently connected tag.
 * @discussion If there is no currently connected tag, this property is set to nil.
 * @since Available since 1.9.0
 */
@property (readonly,atomic,nullable) CMTTagConnectionModel *tagConnectionModelForConnectedTag;

/*!
 *  Force a reload of the most recent connection information for every tag. The mostRecentTagConnectionDictionary propery
 *  will be updated automatically.
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return An NSDictionary mapping tagId to the most recent connection information for every tag.
 *  @since Available since 1.0.0
 */
-(nonnull NSDictionary <NSNumber *, CMTTagConnectionModel *> *) reloadMostRecentTagConnectionDictionaryWithError:(NSError *_Nullable*_Nullable)error;

/**
 ** TAG/VEHICLE INFO 
 **/
/*!
 *  Gets updated vehicle information from server and updates local data. The list of new tags found will be provided in the
 *  return value. The tagInfoDictionary property will get updated automatically when there are new tags.
 *  The mostRecentTagConnectionDictionary property will also get updated.
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return Dictionary of tags that we app should know about
 *  @since Available since 1.2.8
 */
-(nonnull NSDictionary <NSNumber *, CMTTagVehicleInfoModel *> *)loadTagVehicleInfoWithError:(NSError * _Nullable __autoreleasing*_Nullable)error;

/*!
 *  Asynchronously calls the synchronousReloadTagStatusWithError: method. Periodically call this method
 *  to get updates on tag status
 *
 *  @param handler If the handler is defined, it will be called after the reload has happened
 *
 *  @return An NSOperation object that can be used to cancel the update
 *
 *  @since Available since 1.2.8
 *
 *  @deprecated Deprecated since 9.0.0, use loadTagVehicleInfoWithHandler:/loadTagVehicleInfo(handler:) instead.
 */
-(nonnull NSOperation *)loadTagVehicleInfoInBackgroundWithCompletionHandler:(void (^_Nullable)(NSDictionary <NSNumber *, CMTTagVehicleInfoModel *> * _Nullable tagInfoDictionary, BOOL success, NSError * _Nullable error))handler
__attribute__((deprecated("Deprecated since 9.0.0, use loadTagVehicleInfoWithHandler:/loadTagVehicleInfo(handler:) instead ")));

/*!
 *  Asynchronously calls the synchronousReloadTagStatusWithError: method. Periodically call this method
 *  to get updates on tag status
 *
 *  @param handler If the handler is defined, it will be called after the reload has happened
 *
 *  @since Available since 9.0.0
 */
-(void)loadTagVehicleInfoWithHandler:(void (^_Nullable)(NSDictionary <NSNumber *, CMTTagVehicleInfoModel *> * _Nullable tagInfoDictionary,
                                                                    BOOL success, NSError * _Nullable error))handler;

/**
 *  Determine link state of a tag -- not persisted, so network is needed
 *
 *  @param tagIdentifiers List of tag mac addresses that you want checked
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return An array of CMTTagStateModel objects, where each object contains information
 *          about whether or not a tag is linked or not.
 *
 *  @throws (Swift only) NSError
 *  @since Available since 1.2.13
 */
-(nullable NSArray <CMTTagStateModel *>*)loadLinkStateForTags:(NSArray <NSString *> *_Nonnull)tagIdentifiers error:(NSError * _Nullable __autoreleasing*_Nullable)error;

/**
 *  Determine link state of a tag in the background -- not persisted
 *
 *  @param tagIdentifiers List of tag mac addresses that you want checked
 *  @param handler        If set, this callback will be called with the list of CMTTagStateModel objects, one for each tag mac address in the `tagIdentifiers` input.
 *  @return An NSOperation object that can be used to cancel the update.
 *  @since Available since 1.2.13
 *  @deprecated Deprecated since 9.0.0, use loadLinkStateForTags:handler:/loadLinkState(forTags:handler:) instead.
 */
-(nonnull NSOperation *)loadLinkStateForTagsInBackground:(NSArray <NSString *> *_Nonnull) tagIdentifiers
                                       completionHandler:(void (^_Nullable)(NSArray <CMTTagStateModel *> * _Nullable tagStateList, BOOL success, NSError * _Nullable error))handler
__attribute__((deprecated("Deprecated since 9.0.0, use loadLinkStateForTags:handler:/loadLinkState(forTags:handler:) instead ")));

/**
 *  Determine link state of a tag in the background -- not persisted
 *
 *  @param tagIdentifiers List of tag mac addresses that you want checked
 *  @param handler        If set, this callback will be called with the list of CMTTagStateModel objects, one for each tag mac address in the `tagIdentifiers` input.
 *
 *  @since Available since 9.0.0
 */
-(void)loadLinkStateForTags:(NSArray <NSString *> *_Nonnull) tagIdentifiers
                    handler:(void (^_Nullable)(NSArray <CMTTagStateModel *> * _Nullable tagStateList, BOOL success, NSError * _Nullable error))handler;

/**
 *  Provide information for a tag
 *
 *  @param model Contains the tag mac address and the information associated with that tag
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *  @since Available since 1.2.13
 */
-(BOOL)updateInfoForVehicle:(CMTTagVehicleInfoModel *_Nonnull)model error:(NSError * _Nullable __autoreleasing*_Nullable)error;

/**
 *  Update the vehicle information in the background
 *
 *  @param model   Contains the tag mac address and the information associated with that tag
 *  @param handler If set, callback will contain whether or not the update was successful or not
 *  @return An NSOperation object that can be used to cancel the update
 *  @since Available since 1.2.13
 *  @deprecated Deprecated since 9.0.0, use updateInfoForVehicle:handler:/updateInfo(forVehicle:handler:) instead.
 */
-(nonnull NSOperation *)updateInfoForVehicleInBackground:(CMTTagVehicleInfoModel *_Nonnull)model
                                       completionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error))handler
 __attribute__((deprecated("Deprecated since 9.0.0, use updateInfoForVehicle:handler:/updateInfo(forVehicle:handler:) instead ")));

/**
 *  Update the vehicle information in the background
 *
 *  @param model   Contains the tag mac address and the information associated with that tag
 *  @param handler If set, callback will contain whether or not the update was successful or not
 *
 *  @since Available since 9.0.0
 */
-(void)updateInfoForVehicle:(CMTTagVehicleInfoModel *_Nonnull)model handler:(void (^_Nullable)(BOOL success, NSError * _Nullable error))handler;

/**
 Get an array of all connection data for a tag with mac address tagId

 @param tagId The mac address of the tag as an unsigned 64 bit integer
 @return An array of connection data
 @since Available since 1.7.0
 */
-(nonnull NSArray<CMTTagConnectionModel *> *)allConnectionDataForTag:(uint64_t)tagId;

/**
 Fetch location information of the vehicles related to this user in the background.

 @param handler If set, callback will contain an array of CMTTagVehicleLocationInfoModel objects if success, otherwise nil will be returned.
 @return An NSOperation object that can be used to cancel the update.
 @since Available since 1.7.0
 @deprecated Deprecated since 9.0.0, use loadVehicleLocationsWithHandler:/loadVehicleLocations(handler:) instead.
 */
- (nonnull NSOperation *)loadVehicleLocationsInBackgroundWithCompletionHandler:(void(^_Nullable)(NSArray <CMTTagVehicleLocationInfoModel *> * _Nullable vehicleLocations, BOOL success, NSError * _Nullable error))handler
 __attribute__((deprecated("Deprecated since 9.0.0, use loadVehicleLocationsWithHandler:/loadVehicleLocations(handler:)  instead ")));

/**
 Fetch location information of the vehicles related to this user in the background.
 
 @param handler If set, callback will contain an array of CMTTagVehicleLocationInfoModel objects if success, otherwise nil will be returned.
 @since Available since 9.0.0
 */
- (void)loadVehicleLocationsWithHandler:(void(^_Nullable)(NSArray <CMTTagVehicleLocationInfoModel *> * _Nullable vehicleLocations, BOOL success, NSError * _Nullable error))handler;

/**
 *  Asynchronously link a vehicle with a tag
 *
 *  @param vehicleId vehicle identifier
 *  @param macAddress mac address of the tag
 *  @param handler If set, callback will contain whether the linking is successful or not
 *  @return An NSOperation object that can be used to cancel the update
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.0.0, use linkVehicle:toTag:handler:/linkVehicle(_:toTag:handler:) instead.
 */
- (nonnull NSOperation *)linkVehicle:(NSString *_Nonnull)vehicleId withTag:(NSString *_Nonnull)macAddress completionHandler:(void (^_Nonnull)(BOOL success, NSError * _Nonnull error))handler
 __attribute__((deprecated("Deprecated since 9.0.0, use linkVehicle:toTag:handler:linkVehicle(_:toTag:handler:) instead ")));

/**
 *  Asynchronously link a vehicle with a tag
 *
 *  @param vehicleId vehicle identifier
 *  @param macAddress mac address of the tag
 *  @param handler If set, callback will contain whether the linking is successful or not
 *
 *  @since Available since 9.0.0
 */
- (void)linkVehicle:(NSString *_Nonnull)vehicleId toTag:(NSString *_Nonnull)macAddress handler:(void (^_Nonnull)(BOOL success, NSError * _Nonnull error))handler;

/*!
 *  Return a dictionary that is keyed on tagId, an NSNumber.
 * @since Available since 1.0.0
 */
@property (readonly,nonnull) NSDictionary <NSNumber *, CMTTagVehicleInfoModel *> *tagInfoDictionary;

/*!
 *  Return a dictionary that is keyed on vehicleId, an NSNumber.
 *  @since Available since 1.2.13
 */
@property (readonly,nonnull) NSDictionary <NSNumber *, CMTTagVehicleInfoModel *> * unlinkedVehicleDictionary;

/*!
 *  Return a list of the user's linked vehicles
 *  @since Available since 1.7.0
 */
@property (readonly,nonnull) NSArray<CMTTagVehicleInfoModel *> *linkedVehicleList;

/*!
 *  Returns a list of vehicle location's that are visible to the authenticated user 
 *  @since Available since 1.7.0
 */
@property (readonly,nullable) NSArray<CMTTagVehicleLocationInfoModel *> *vehicleLocationList;

@end
