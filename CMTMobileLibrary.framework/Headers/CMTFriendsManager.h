//
// CMTFriendsManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTLeaderboardDataManager.h"
#import "CMTFriendRequestsManager.h"
#import "CMTFriendInvitesManager.h"

/*!
 * Provide interfaces to all friend-related server APIs. Includes server APIs for inviting new friends,
 * responding to friend invitations, listing friends, and removing friends.
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendsManager : NSObject

/*!
 * A reference to a `CMTFriendRequestManager` instance
 *  @since Available since 0.1.0
 */
@property (nonatomic,nonnull) CMTFriendRequestsManager *friendRequestsManager;

/*!
 * A reference to a `CMTFriendInvitesManager` instance
 *  @since Available since 0.1.0
 */
@property (nonatomic,nonnull) CMTFriendInvitesManager *friendInvitesManager;

/*!
 * A reference to a `CMTLeaderboardDataManager` instance
 *  @since Available since 0.1.0
 */
@property (nonatomic,nonnull) CMTLeaderboardDataManager *leaderboardManager;

/**
 App developers should not attempt to create an instance of `CMTFriendsManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Create a `CMTFriendManager` object for the `user`
 *  @since Available since 0.1.0
 *  @param user an authenticated CMTUser
 *  @return A `CMTFriendManager` instance
 */
-(nonnull instancetype) initWithUser:(CMTUser *_Nonnull)user NS_DESIGNATED_INITIALIZER;

/*!
 *  Cancel all pending operations related to friend operations
 *  @since Available since 0.2.10
 */
-(void)cancelAllOperations;

@end
