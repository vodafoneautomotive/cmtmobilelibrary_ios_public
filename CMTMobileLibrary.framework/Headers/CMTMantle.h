//
// CMTMantle.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Mantle.
FOUNDATION_EXPORT double MantleVersionNumber;

//! Project version string for Mantle.
FOUNDATION_EXPORT const unsigned char MantleVersionString[];

#import "CMTMTLJSONAdapter.h"
#import "CMTMTLModel.h"
#import "CMTMTLModel+NSCoding.h"
#import "CMTMTLValueTransformer.h"
#import "CMTMTLTransformerErrorHandling.h"
#import "NSArray+CMTMTLManipulationAdditions.h"
#import "NSDictionary+CMTMTLManipulationAdditions.h"
#import "NSDictionary+CMTMTLMappingAdditions.h"
#import "NSObject+CMTMTLComparisonAdditions.h"
#import "NSValueTransformer+CMTMTLInversionAdditions.h"
#import "NSValueTransformer+CMTMTLPredefinedTransformerAdditions.h"
