//
// CMTUnprocessedTripStatusDidChangeDelegate.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTBaseDriveInfo.h"

@class CMTTripListDataManager;

/*! Get upload state updates using this delegate
 * @since Available since 1.0.0
 */
@protocol CMTUnprocessedTripStatusDidChangeDelegate

/*! 
 * Called when an unprocessed trip has a processing state change
 * @since Available since 1.7.0
 */

/**
 Called when an unprocessed trip has a processing state change

 @param dm A `CMTTripListDataManager` object
 @param trip The trip whose processing status has changed.
 */
-(void)tripListDataManager:(CMTTripListDataManager *_Nonnull)dm processingStatusDidChangeForTrip:(CMTBaseDriveInfo *_Nonnull)trip;

@end
