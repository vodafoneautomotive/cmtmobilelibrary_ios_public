//
// CMTServerRequestsHelper.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CMTUser;

/*! CMTServerRequestsHelper Error Domain 
 *
 *  @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTServerRequestsHelperErrorDomain;

/**
 Error codes from CMTServerRequestHelper
 
 @since Available since 1.0.0
 @discussion Deprecated since 9.0.0
 */
typedef NS_ENUM(NSInteger, CMTServerRequestHelperErrorType) {
    /// Error code for empty support email message (Available since 1.0.0)
    CMTServerRequestHelperSupportErrorMissingMessage
};

/**
 *  Convenience key to access contents of target view dictionary pertaining to promo code information
 */
extern NSString * _Nonnull const CMTServerRequestsHelperTargetViewPromoCodeKey;

/*!
 * Class to help with various server calls that require a valid user
 *  @since Available since 1.0.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTServerRequestsHelper : NSObject

/**
 App developers should not attempt to create an instance of `CMTServerRequestsHelper` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 *  Create instance of CMTServerRequestHelper class
 *
 *  @param user Authenticated CMTUser object
 *
 *  @return An instance of a CMTServerRequestHelper object
 *  @since Available since 1.0.0
 */
-(nonnull instancetype) initWithUser:(CMTUser *_Nonnull)user NS_DESIGNATED_INITIALIZER;

/*!
 *  Send a support message on the user's behalf
 *
 *  @param message The contents of the message to send
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *  @since Available since 1.0.0
 */
-(BOOL) sendSupportEmailMessage:(NSString *_Nonnull)message error:(NSError * _Nullable __autoreleasing *_Nullable)error;

/*!
 *  Send support email message in the background
 *
 *  @param message The contents of the message to send
 *  @param handler If non nil, the handler will be called after the request has finished. The handler will be passed two parameters:
 *                 `success` is set to whether or not the request succeeded (TRUE) or failed (FALSE)
 *                 `error` is non-nil if an error occurred
 *  @return An NSOperation object that can be used to cancel the operation
 *  @since Available since 1.0.0
 */
-(nonnull NSOperation *)sendSupportEmailMessageInBackground:(NSString *_Nonnull)message
                                          completionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error))handler;

/*!
 *  Have the server send a request for quote on the user's behalf
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return (Objective-C only) YES if successful, NO otherwise
 *
 *  @throws (Swift only) NSError
 *  @since Available since 1.0.0
 */
-(BOOL) requestQuoteWithError:(NSError * _Nonnull __autoreleasing *_Nonnull)error;

/*!
 *  Request a quote in the background
 *
 *  @param handler If non nil, the handler will be called after the request has finished. The handler will be passed two parameters:
 *                 `success` is set to whether or not the request succeeded (TRUE) or failed (FALSE)
 *                 `error` is non-nil if an error occurred
 *  @return An NSOperation object that can be used to cancel the operation
 *  @since Available since 1.0.0
 */
-(nonnull NSOperation *)requestQuoteInBackgroundWithCompletionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error))handler;

/**
 *  Fetch custom target view dictionary
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return nil if error occurred, otherwise, returns an updated targetViewDictionary
 *
 *  @throws (Swift only) NSError
 *  @since Available since 1.2.17
 */
-(nullable NSDictionary <NSString *, NSString *> *) loadTargetViewDictionaryWithError:(NSError * _Nullable __autoreleasing *_Nullable)error;

/**
 *  Fetch custom target view dictionary in the background
 *
 *  @param handler If set, the handler will be called with the updated target view dictionary. If the call failed, a cached version of the targetViewDictionary will be returned.
 *  @return An NSOperation object that can be used to cancel the operation
 *  @since Available since 1.2.17
 */
-(nonnull NSOperation *) loadTargetViewDictionaryInBackgroundWithCompleltionHandler:(void (^_Nullable)(NSDictionary <NSString *,
                                                                                              NSString *> * _Nullable targetViewDictionary,
                                                                                              BOOL success,
                                                                                                       NSError * _Nullable error))handler;

/**
 Send report on business/personal trips to email address.

 @param startDate Report start date.
 @param endDate Report end date.
 @param emailAddress Email address to receive the report.
 @param handler If set, the handler will be called with the updated target view dictionary. If the call failed, a cached version of the targetViewDictionary will be returned.
 @return An NSOperation object that can be used to cancel the operation
 @since Available since 1.7.0
 */
- (nonnull NSOperation *)requestTripReportInBackgroundFromDate:(NSDate *_Nonnull)startDate
                                                        toDate:(NSDate *_Nonnull)endDate
                                            sendToEmailAddress:(NSString *_Nonnull)emailAddress
                                         withCompletionHandler:(void (^_Nullable)(BOOL success, NSError * _Nullable error))handler;

/**
 *  Fetch policy dashboard dictionary synchronously
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return nil if error occurred, otherwise, returns an dictionary with policy dashboard info
 *
 *  @throws (Swift only) NSError
 *  @since Available since 1.7.1
 */
- (nullable NSDictionary<NSString *, NSString *> *)loadPolicyDashboardDictionaryWithError:(NSError * _Nullable __autoreleasing* _Nullable)error;

/**
 * The values of the dictionary correspond to contents to display in a custom full screen dialog
 *  @since Available since 1.7.0
 */
@property (atomic,strong,readonly,nullable) NSDictionary <NSString *, NSString *> *targetViewDictionary;

/*!
 *  Cancels any pending server helper fetches
 *  @since Available since 1.0.0
 */
-(void)cancelAllOperations;
@end
