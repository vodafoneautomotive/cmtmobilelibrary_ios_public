//
// CMTRecordingScheduleModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

@class CMTTimeRange;


/**
 Class that holds info about time ranges when trip recording should be suppressed
 @since Available since 1.4.0
 */
@interface CMTRecordingScheduleModel : CMTMTLModel<CMTMTLJSONSerializing>


/**
 Array of `CMTTimeRange` class indicating time ranges when trip recording should be suppressed.
 @since Available since 1.4.0
 */
@property (nullable) NSArray <CMTTimeRange *> *standbyTimeRanges;

@end
