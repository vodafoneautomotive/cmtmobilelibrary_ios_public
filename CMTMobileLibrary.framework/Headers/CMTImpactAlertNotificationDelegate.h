//
// CMTImpactAlertNotificationDelegate.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTImpactAlertModel.h"

/*!
 *  Callback to confirm or suppress impact.
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
typedef void (^ImpactAlertConfirmationCallback)(BOOL suppressImpact) __attribute__((deprecated("Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*!
*  Delegate protocol to receive notification of impact events
*  @since Available since 1.7.0
*  @deprecated Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
*/
__attribute__((deprecated("Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")))
@protocol CMTImpactAlertNotificationDelegate <NSObject>

@optional

/*!
 *  Ask the delegate if this impact alert should be suppressed or not
 *  @since Available since 1.7.0
 *  @param impactAlertModel model containing data for detected impact
 *  @param callback block returning YES if impact reporting should be suppressed or NO otherwise
 *  @deprecated Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
- (void)confirmImpactAlert:(CMTImpactAlertModel *_Nonnull)impactAlertModel withCallback:(ImpactAlertConfirmationCallback _Nonnull )callback __attribute__((deprecated("Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*!
 *  Inform the delegate the impact alert has been handled
 *  @since Available since 1.7.0
 *  @param tagMacAddress MAC address of the tag which has reported the impact
 *  @deprecated Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
- (void)displayLocationNotificationImpactForMacAddress:(NSString * _Nullable)tagMacAddress __attribute__((deprecated("Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

@end
