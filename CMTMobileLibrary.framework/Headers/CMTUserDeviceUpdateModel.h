//
// CMTUserDeviceUpdateModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 *  Class to use when updating information about the user devices
 * @since Available since 1.0.0
 */
@interface CMTUserDeviceUpdateModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Set to TRUE if user has elected to upload via WiFi only
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *uploadWiFiOnly;

/*!
 *  Push token
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSString *pushToken;

/*!
 *  Nickname for device
 *  @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSString *deviceNickname;

/*!
 * Set to true if notifications should be sent for invites (Boolean)
 * @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *useNotificationsForInvites;

/*!
 * Set to true if notifications should be sent for new results(Boolean)
 * @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *useNotificationsForNewResults;

/*!
 * Set to true if notifications should be sent for badges (Boolean)
 * @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *useNotificationsForBadges;

/*!
 *  Set to true if distribution build (Boolean)
 * @since Available since 1.0.0
 */
@property (nonatomic,nullable) NSNumber *distributionBuild;

@end
