//
// CMTTagActivationManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTTagActivationDelegate.h"

/**
 Name of errors that come from the `CMTTagActivationManager`
 @since Available since 4.0.0
 */
extern NSString * _Nonnull const CMTTagActivationErrorDomain;

/**
 Error codes that could be returned during the tag linking process
 @since Available since 4.0.0 
 */
typedef NS_ENUM(NSInteger, CMTTagActivationErrorType) {
    /// No error (Available since 4.0.0)
    CMTTagActivationErrorNone                           = 0,
    /// Could not find tag specified in `beginLinkingTagWithMacAddress:toVehicle:error:` (Available since 4.0.0)
    CMTTagActivationScanningErrorTagNotFound            = -1,
    /// While scanning for tag, no tags were found within the timeout period (returned if no tag mac address is specified in `beginLinkingTagWithMacAddress:toVehicle:error:`) (Available since 4.0.0)
    CMTTagActivationScanningErrorNoTagsFound            = -2,
    /// A working network connection is not available (Available since 4.0.0)
    CMTTagActivationNetworkErrorNetworkNotAvailable     = -3,
    /// The provided vehicle cannot be linked to that tag by this user (Available since 4.0.0)
    CMTTagActivationServerErrorVehicleLinkNotAllowed    = -4,
    /// The tag provided has already been linked (Available since 4.0.0)
    CMTTagActivationServerErrorTagAlreadyUsed           = -5,
    /// The provided tag cannot be found in the server database (Available since 4.0.0)
    CMTTagActivationServerErrorUnknownTag               = -6,
    /// The server is not available or there is some other server error (Available since 4.0.0)
    CMTTagActivationServerErrorUnknown                  = -7,
    /// Tag is read from during the activation process, but that process could fail (Available since 4.0.0)
    CMTTagActivationTagErrorReadFailed                  = -8,
    /// The tag is written to during the activation process, but that process could fail (Available since 4.0.0)
    CMTTagActivationTagErrorWriteFailed                 = -9,
    /// The indication mode selected is not supported by this tag (Available since 5.0.0)
    CMTTagActivationTagIndicationModeNotSupported       = -10,
    /// Writing of activation instructions failed (Available since 5.0.3)
    CMTTagActivationInstructionsFailed                  = -11,
    /// Location permissions are insufficient aka not Always permission, so tag scanning would fail (Available since 9.0.0)
    CMTTagActivationLocationPermissionNotAlways         = -12,
    /// Bluetooth is powered off 
    CMTTagActivationBluetoothPoweredOff                 = -13,
    /// Another tag activation is in progress OR `finishLinkingTag` was not called (Available since 4.0.0)
    CMTTagActivationTagErrorTagActivationInProgress     = -100
};

@class CMTUser;

/**
 Used to link and activate tags that support the new tag activation process.
 
 @since Available since 4.0.0
 */
@interface CMTTagActivationManager : NSObject

/**
 Call this method to begin the tag activation process.
 
 @discussion If the `tagMacAddress` is not provided, then the tag with the highest RSSI value will be selected during the activation process. 
 A working network connection is needed for tag activation to succeed.
 After calling this method, the method [CMTTagActivationDelegate tagActivationManager:canIndicateLinkingTag:withIndicationCapabilities:] will be invoked if the delegate object responds to it (i.e. the app developer defined that protocol method).
 Otherwise [CMTTagActivationDelegate tagActivationManager:didCompleteLinkingTag:toVehicle:] will be invoked when activation completes and the app developer does not need to call [CMTTagActivationManager confirmLinkingTag].
 If any errors occur during any part of the activation process, [CMTTagActivationDelegate tagActivationManager:didFailToLinkTag:toVehicle:error:] will be invoked.
 
 @param tagMacAddress tag mac address of the tag being linked (optional)
 @param vehicle A vehicle object with `shortVehicleIdentifier` set.
 @param error If an error occurs while trying to start tag linking, this parameter will be set to a non-nil value.
 @return YES if the linking process was started successfully
 
 @since Available since 4.0.0
 */
-(BOOL)beginLinkingTagWithMacAddress:(NSString * _Nullable)tagMacAddress
                           toVehicle:(CMTVehicle * _Nonnull)vehicle
                               error:(NSError * _Nullable __autoreleasing * _Nullable)error;


/**
 @method indicateLinkingTagWithIndicationCapability:

 @param capability App developers should set indication capability to the appropriate value in order to get the tag to blink and/or beep. Not all tags support
 all indication modes. The supported modes are returned to the app developer if the app developer provides an object that responds to the 
 [CMTTagActivationDelegate tagActivationManager:canIndicateLinkingTag:withIndicationCapabilities:] method.
 
 @since Available since 4.0.0
 */
-(void)indicateLinkingTagWithIndicationCapability:(CMTTagIndicationCapability)capability;


/**
 @method confirmLinkingTag
 
 @discussion If the app developer does not implement the delegate method [CMTTagActivationDelegate tagActivationManager:canIndicateLinkingTag:withIndicationCapabilities:], they do not need to call this method.
 App developers should call this method if the [CMTTagActivationManager indicateLinkingTagWithIndicationCapability:] method is also used.
 When that method is called, the tag will start blinking and/or beeping.
 This allows the user to confirm that the tag they are trying to link to a vehicle is the correct tag.
 The app developer should provide a UI element for users to provide this feedback.
 Once the user confirms that the correct tag is being linked, the app developer can inform the SDK by calling this method.
 
 @since Available since 4.0.0
 */
-(void)confirmLinkingTag;

/**
 @method finishLinkingTag
 
 @discussion App developers must call this method when either [CMTTagActivationDelegate tagActivationManager:didCompleteLinkingTag:toVehicle:]
 or [CMTTagActivationDelegate tagActivationManager:didFailToLinkTag:toVehicle:error:] is called by the SDK. Also, the app developer should call this 
 method when the UI element that creates the CMTTagActivationManager disappears.
 
 @since Available since 4.0.0
 */
-(void)finishLinkingTag;

/**
 App developers should not attempt to create an instance of `CMTTagActivationManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Create an instance of a CMTTagActivationManager

 @discussion You should only create one of these objects at a time.
 
 @param user An authenticated user
 @param activationDelegate Object that conforms to the CMTTagActivationDelegate protocol
 @return instance of CMTTagActivationManager
 
 @since Available since 4.0.0
 */
-(nonnull instancetype) initWithUser:(CMTUser * _Nonnull)user
                         andDelegate:(NSObject<CMTTagActivationDelegate> * _Nonnull)activationDelegate NS_DESIGNATED_INITIALIZER;

@end
