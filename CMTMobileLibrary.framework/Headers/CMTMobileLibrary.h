//
//  CMTMobileLibrary.h
//  CMTMobileLibrary
//
//  Created by Eugene on 4/28/17.
//  Copyright © 2017 Cambridge Mobile Telematics. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CMTMobileLibrary.
FOUNDATION_EXPORT double CMTMobileLibraryVersionNumber;

//! Project version string for CMTMobileLibrary.
FOUNDATION_EXPORT const unsigned char CMTMobileLibraryVersionString[];

// BEGIN PUBLIC HEADERS
#import <CMTMobileLibrary/CMTSVRAlertModel.h>
#import <CMTMobileLibrary/CMTBaseDataProvider.h>
#import <CMTMobileLibrary/CMTTripMetricsModel.h>
#import <CMTMobileLibrary/CMTUserDeviceUpdateModel.h>
#import <CMTMobileLibrary/CMTTagUpdater.h>
#import <CMTMobileLibrary/CMTUserSummaryDataProvider.h>
#import <CMTMobileLibrary/CMTMarketingMaterialsDataProvider.h>
#import <CMTMobileLibrary/CMTFriendInviteSentModel.h>
#import <CMTMobileLibrary/CMTFriendModel.h>
#import <CMTMobileLibrary/CMTRecordingScheduleModel.h>
#import <CMTMobileLibrary/CMTTagScanner.h>
#import <CMTMobileLibrary/CMTUserDeviceModel.h>
#import <CMTMobileLibrary/CMTFriendRequestsManager.h>
#import <CMTMobileLibrary/CMTFriendMobileInviteModel.h>
#import <CMTMobileLibrary/CMTTagActivationDelegate.h>
#import <CMTMobileLibrary/NSDictionary+CMTMTLManipulationAdditions.h>
#import <CMTMobileLibrary/CMTMantle.h>
#import <CMTMobileLibrary/NSDictionary+CMTMTLJSONKeyPath.h>
#import <CMTMobileLibrary/NSArray+CMTMTLManipulationAdditions.h>
#import <CMTMobileLibrary/NSObject+CMTMTLComparisonAdditions.h>
#import <CMTMobileLibrary/CMTEXTScope.h>
#import <CMTMobileLibrary/CMTmetamacros.h>
#import <CMTMobileLibrary/CMTEXTRuntimeExtensions.h>
#import <CMTMobileLibrary/CMTEXTKeyPathCoding.h>
#import <CMTMobileLibrary/CMTMTLReflection.h>
#import <CMTMobileLibrary/CMTMTLJSONAdapter.h>
#import <CMTMobileLibrary/CMTMTLModel.h>
#import <CMTMobileLibrary/CMTMTLValueTransformer.h>
#import <CMTMobileLibrary/CMTMTLModel+NSCoding.h>
#import <CMTMobileLibrary/NSDictionary+CMTMTLMappingAdditions.h>
#import <CMTMobileLibrary/CMTMTLTransformerErrorHandling.h>
#import <CMTMobileLibrary/NSValueTransformer+CMTMTLPredefinedTransformerAdditions.h>
#import <CMTMobileLibrary/NSValueTransformer+CMTMTLInversionAdditions.h>
#import <CMTMobileLibrary/NSError+CMTMTLModelException.h>
#import <CMTMobileLibrary/CMTFriendsManager.h>
#import <CMTMobileLibrary/CMTUserDeviceSettingsManager.h>
#import <CMTMobileLibrary/CMTTeamSummaryModel.h>
#import <CMTMobileLibrary/CMTTagConnectionModel.h>
#import <CMTMobileLibrary/CMTVersionState.h>
#import <CMTMobileLibrary/CMTImpactAlertModel.h>
#import <CMTMobileLibrary/CMTFriendInvitesManager.h>
#import <CMTMobileLibrary/CMTVehicleManager.h>
#import <CMTMobileLibrary/CMTServerRequestsHelper.h>
#import <CMTMobileLibrary/CMTConsoleLogger.h>
#import <CMTMobileLibrary/CMTSVRDetectorDelegate.h>
#import <CMTMobileLibrary/CMTLeaderboardDataManager.h>
#import <CMTMobileLibrary/CMTSVRDetector.h>
#import <CMTMobileLibrary/CMTImpactDataManager.h>
#import <CMTMobileLibrary/CMTMobileResultsModel.h>
#import <CMTMobileLibrary/CMTPanicAlertManager.h>
#import <CMTMobileLibrary/CMTTripLabelModel.h>
#import <CMTMobileLibrary/CMTVehicle.h>
#import <CMTMobileLibrary/CMTEventLogger.h>
#import <CMTMobileLibrary/CMTTipModel.h>
#import <CMTMobileLibrary/CMTBadgeModel.h>
#import <CMTMobileLibrary/CMTDataTypes.h>
#import <CMTMobileLibrary/CMTImpactAlertNotificationDelegate.h>
#import <CMTMobileLibrary/CMTDeviceManager.h>
#import <CMTMobileLibrary/CMTEventModel.h>
#import <CMTMobileLibrary/CMTTagConnectionHealthMonitor.h>
#import <CMTMobileLibrary/CMTUtils.h>
#import <CMTMobileLibrary/CMTTag.h>
#import <CMTMobileLibrary/CMTAppAnalyticsManager.h>
#import <CMTMobileLibrary/CMTServerConfiguration.h>
#import <CMTMobileLibrary/CMTTripDataManager.h>
#import <CMTMobileLibrary/CMTCategoryRankingModel.h>
#import <CMTMobileLibrary/CMTWiFiMonitor.h>
#import <CMTMobileLibrary/CMTTagVehicleInfoModel.h>
#import <CMTMobileLibrary/CMTCurrentlyRecordingDriveInfo.h>
#import <CMTMobileLibrary/CMTSynchronousDataProviderProtocol.h>
#import <CMTMobileLibrary/CMTDriveScheduleManager.h>
#import <CMTMobileLibrary/CMTProcessedDriveInfo.h>
#import <CMTMobileLibrary/CMTImpactUploader.h>
#import <CMTMobileLibrary/CMTTagUpgradeStatusManager.h>
#import <CMTMobileLibrary/CMTTripDetectorRecordingStateDelegate.h>
#import <CMTMobileLibrary/CMTReachabilityHelper.h>
#import <CMTMobileLibrary/CMTBaseDriveInfo.h>
#import <CMTMobileLibrary/CMTMobileLibraryConfigurationModel.h>
#import <CMTMobileLibrary/CMTCustomLeaderboardDataManager.h>
#import <CMTMobileLibrary/CMTUserBaseDataProvider.h>
#import <CMTMobileLibrary/CMTUserSummaryTripMetricsModel.h>
#import <CMTMobileLibrary/CMTFriendResponseModel.h>
#import <CMTMobileLibrary/CMTFriendRequestModel.h>
#import <CMTMobileLibrary/CMTWaypointModel.h>
#import <CMTMobileLibrary/CMTURLSessionManager.h>
#import <CMTMobileLibrary/CMTUser.h>
#import <CMTMobileLibrary/CMTBaseResultsModel.h>
#import <CMTMobileLibrary/CMTLogger.h>
#import <CMTMobileLibrary/CMTUserRankModel.h>
#import <CMTMobileLibrary/CMTUnprocessedTripStatusDidChangeDelegate.h>
#import <CMTMobileLibrary/CMTSafetyManager.h>
#import <CMTMobileLibrary/CMTTeamSummaryTripMetricsModel.h>
#import <CMTMobileLibrary/CMTTimeRange.h>
#import <CMTMobileLibrary/CMTTagActivationManager.h>
#import <CMTMobileLibrary/CMTSQLiteInterface.h>
#import <CMTMobileLibrary/CMTAppAnalyticsScreenCategory.h>
#import <CMTMobileLibrary/CMTTagDataManager.h>
#import <CMTMobileLibrary/CMTMobileLibraryCore.h>
#import <CMTMobileLibrary/CMTFriendInviteModel.h>
#import <CMTMobileLibrary/CMTTagVehicleLocationInfoModel.h>
#import <CMTMobileLibrary/CMTFriendEmailInviteModel.h>
#import <CMTMobileLibrary/CMTTripListDataManager.h>
#import <CMTMobileLibrary/CMTBadgesDataManager.h>
#import <CMTMobileLibrary/CMTLeaderboardModel.h>
#import <CMTMobileLibrary/CMTFriendFacebookInviteModel.h>
#import <CMTMobileLibrary/CMTFriendRequestsSentModel.h>
#import <CMTMobileLibrary/CMTVehicleDescription.h>
#import <CMTMobileLibrary/CMTFriendRequestsReceivedModel.h>
#import <CMTMobileLibrary/CMTBadgesModel.h>
#import <CMTMobileLibrary/CMTTagStateModel.h>
#import <CMTMobileLibrary/CMTTripDetector.h>
#import <CMTMobileLibrary/CMTUserSummaryModel.h>
#import <CMTMobileLibrary/CMTMarketingMessageModel.h>
#import <CMTMobileLibrary/CMTTripDetectorTagNotificationDelegate.h>
