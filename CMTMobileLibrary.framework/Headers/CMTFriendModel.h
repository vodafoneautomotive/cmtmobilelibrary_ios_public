//
// CMTFriendModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTBadgeModel.h"

/*!
 *  `CMTFriendModel` objects are sent to or received from the server.
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Unique integer identifier, friendId = 0 corresponds to the user
 *  @since Available since 0.1.0
 */
@property (nullable) NSNumber *friendId;

/*! Nickname provided by friend
 *  @since Available since 0.1.0
 */
@property (nullable) NSString *name;

/*! Gender of friend or nil if not provided
 *  @since Available since 0.1.0
 */
@property (nullable) NSString *gender;

/*! Friend's user score (integer) or nil if they have no score yet
 *  @since Available since 0.1.0
 */
@property (nullable) NSNumber *score;

/*! Integer rank among friends or nil if they have no score yet
 *  @since Available since 0.1.0
 */
@property (nullable) NSNumber *rank;

/*! Email address if friendship was established previously via email, otherwise nil
 *  @since Available since 0.1.0
 */
@property (nullable) NSString *email;

/*! URL of profile photo if user provided a custom photo, otherwise nil
 *  @since Available since 0.1.0
 */
@property (nullable) NSURL *photoURL;

/*! hex encoded sha1 of predetermined salt + lowercase of friend's email address
 *  @since Available since 0.1.0
 */
@property (nullable) NSString *ehash;

/*! An array of badges achieved by the friend or nil if the friend has no badges
 *  @since Available since 0.1.0
 */
@property (nullable) NSArray <CMTBadgeModel *> *badges;

@end
