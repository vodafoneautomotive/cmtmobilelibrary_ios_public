//
// CMTImpactDataManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTImpactUploader.h"

@class CMTUser;

/**
 Class to manage tag impact alerts.
 @since Available since 1.7.0
 @deprecated Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
__attribute__((deprecated("Deprecated since 9.3.0. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")))
@interface CMTImpactDataManager : CMTImpactUploader

/**
 App developers should not attempt to create an instance of `CMTImpactDataManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

@end
