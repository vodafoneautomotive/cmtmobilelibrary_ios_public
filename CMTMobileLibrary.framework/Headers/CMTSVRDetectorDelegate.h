//
// CMTSVRDetectorDelegate.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTSVRAlertModel.h"

@class CMTSVRDetector;

/**
 SVR detector delegate methods
 
 @since Available since 1.2.1
 */
@protocol CMTSVRDetectorDelegate<NSObject>

@optional

/**
 *  Invoked when an SVR tag is discovered
 *
 *  @since Available since 1.2.6
 *  @param svrDetector   Reference to svrDetector object
 *  @param tagMacAddress Tag Mac Address of discovered tag
 */
-(void)svrDetector:(CMTSVRDetector * _Nonnull)svrDetector didDiscoverTagWithIdentifier:(NSString * _Nonnull)tagMacAddress;

/**
 *  Invoked when the location(s) of any SVR tags that was / were encountered are successfully posted to the server.
 *
 *  @since Available since 1.2.1
 *  @param svrDetector Reference to svrDetector object
 *  @param beacons     An array of CMTSVRAlertModel objects that was sent to the server
 */
-(void)svrDetector:(CMTSVRDetector * _Nonnull)svrDetector didNotifyServerOfBeacons:(NSArray<CMTSVRAlertModel *> * _Nonnull)beacons;

/**
 *  Invoked when an error has occurred when trying to post a notification to the CMT servers. Error types are defined in "CMTDataTypes.h".
 *
 *  @since Available since 1.2.1
 *  @param svrDetector Reference to svrDetector object
 *  @param beacons     An array of CMTSVRAlertModel objects that was sent to the server
 *  @param error       The reason for the failure
 */
-(void)svrDetector:(CMTSVRDetector * _Nonnull)svrDetector failedToNotifyServerOfBeacons:(NSArray<CMTSVRAlertModel *> * _Nonnull)beacons error:(NSError * _Nonnull)error;

@end
