//
// NSValueTransformer+CMTMTLPredefinedTransformerAdditions.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CMTMTLTransformerErrorHandling.h"

/// The name for a value transformer that converts strings into URLs and back.
extern NSString * const CMTMTLURLValueTransformerName;

/// The name for a value transformer that converts strings into NSUUIDs and back.
extern NSString * const CMTMTLUUIDValueTransformerName;

/**
 Ensure an NSNumber is backed by __NSCFBoolean/CFBooleanRef

 NSJSONSerialization, and likely other serialization libraries, ordinarily
 serialize NSNumbers as numbers, and thus booleans would be serialized as
 0/1. The exception is when the NSNumber is backed by __NSCFBoolean, which,
 though very much an implementation detail, is detected and serialized as a
 proper boolean.
*/
extern NSString * const CMTMTLBooleanValueTransformerName;

@interface NSValueTransformer (CMTMTLPredefinedTransformerAdditions)

/**
 An optionally reversible transformer which applies the given transformer to
 each element of an array.

 @param transformer The transformer to apply to each element. If the transformer
               is reversible, the transformer returned by this method will be
               reversible. This argument must not be nil.

 @return a transformer which applies a transformation to each element of an
 array.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_arrayMappingTransformerWithTransformer:(NSValueTransformer *)transformer;

/**
 A reversible value transformer to transform between the keys and objects of a
 dictionary.

 @param dictionary The dictionary whose keys and values should be
                       transformed between. This argument must not be nil.
 @param defaultValue The result to fall back to, in case no key matching the
                       input value was found during a forward transformation.
 @param reverseDefaultValue The result to fall back to, in case no value matching
                       the input value was found during a reverse
                       transformation.

 Can for example be used for transforming between enum values and their string
 representation.

   NSValueTransformer *valueTransformer = [NSValueTransformer cmt_mtl_valueMappingTransformerWithDictionary:@{
     @"foo": @(EnumDataTypeFoo),
     @"bar": @(EnumDataTypeBar),
   } defaultValue: @(EnumDataTypeUndefined) reverseDefaultValue: @"undefined"];

 @return a transformer which will map from keys to objects for forward
 transformations, and from objects to keys for reverse transformations.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_valueMappingTransformerWithDictionary:(NSDictionary *)dictionary defaultValue:(id)defaultValue reverseDefaultValue:(id)reverseDefaultValue;

/**
 Wraps `+cmt_mtl_valueMappingTransformerWithDictionary:defaultValue:reverseDefaultValue:` method with default value of `nil` and reverse default value of `nil`.
 @param dictionary The dictionary whose keys and values should be transformed between. This argument must not be nil.
 @return a value transformer created by calling
 `+cmt_mtl_valueMappingTransformerWithDictionary:defaultValue:reverseDefaultValue:`
 with a default value of `nil` and a reverse default value of `nil`.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_valueMappingTransformerWithDictionary:(NSDictionary *)dictionary;

/**
 A reversible value transformer to transform between a date and its string representation.

 @param dateFormat The date format used by the date formatter (http://www.unicode.org/reports/tr35/tr35-31/tr35-dates.html#Date_Field_Symbol_Table)
 @param calendar The calendar used by the date formatter
 @param locale The locale used by the date formatter
 @param timeZone The time zone used by the date formatter
 @param defaultDate A default date
 @return a transformer which will map from strings to dates for forward transformations, and from dates to strings for reverse transformations.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_dateTransformerWithDateFormat:(NSString *)dateFormat calendar:(NSCalendar *)calendar locale:(NSLocale *)locale timeZone:(NSTimeZone *)timeZone defaultDate:(NSDate *)defaultDate;

/**
 A reversible value transformer to transform between a date and its string representation.
 
 @param dateFormat The date format used by the date formatter (http://www.unicode.org/reports/tr35/tr35-31/tr35-dates.html#Date_Field_Symbol_Table)
 @param locale The locale used by the date formatter
 @return a value transformer created by calling
 `+cmt_mtl_dateTransformerWithDateFormat:calendar:locale:timeZone:defaultDate:`
 with a calendar, locale, time zone and default date of `nil`.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_dateTransformerWithDateFormat:(NSString *)dateFormat locale:(NSLocale *)locale;

/**
 A reversible value transformer to transform between a number and its string
 representation

 @param numberStyle The number style used by the number formatter
 @param locale The locale used by the number formatter
 
 @return a transformer which will map from strings to numbers for forward
 transformations, and from numbers to strings for reverse transformations.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_numberTransformerWithNumberStyle:(NSNumberFormatterStyle)numberStyle locale:(NSLocale *)locale;

/**
 A reversible value transformer to transform between an object and its string
 representation

 @param formatter   The formatter used to perform the transformation
 @param objectClass The class of object that the formatter operates on

 @return a transformer which will map from strings to objects for forward
 transformations, and from objects to strings for reverse transformations.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_transformerWithFormatter:(NSFormatter *)formatter forObjectClass:(Class)objectClass;

/**
 A value transformer that errors if the transformed value are not of the given
 class.

@param modelClass The expected class. This argument must not be nil.
@return a transformer which will return an error if the transformed in value
 is not a member of class. Otherwise, the value is simply passed through.
 */
+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_validatingTransformerForClass:(Class)modelClass;

+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_JSONDictionaryTransformerWithModelClass:(Class)modelClass __attribute__((deprecated("Replaced by +[CMTMTLJSONAdapter dictionaryTransformerWithModelClass:]")));

+ (NSValueTransformer<CMTMTLTransformerErrorHandling> *)cmt_mtl_JSONArrayTransformerWithModelClass:(Class)modelClass __attribute__((deprecated("Replaced by +[CMTMTLJSONAdapter arrayTransformerWithModelClass:]")));

@end
