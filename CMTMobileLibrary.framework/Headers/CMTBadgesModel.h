//
// CMTBadgesModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTBadgeModel.h"

/*!
 *  Wrapper class around the array of badges for a user
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTBadgesModel : CMTMTLModel <CMTMTLJSONSerializing>

/*! An array of CMTBadgeModel objects
 *  @since Available since 0.1.0
*/
@property (nullable) NSArray <CMTBadgeModel*> *badges;

@end
