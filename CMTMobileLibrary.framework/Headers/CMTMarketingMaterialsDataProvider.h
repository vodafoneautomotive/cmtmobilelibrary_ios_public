//
// CMTMarketingMaterialsDataProvider.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTBaseDataProvider.h"
#import "CMTMarketingMessageModel.h"
#import "CMTSynchronousDataProviderProtocol.h"

extern NSString * _Nonnull const CMTMarketingMaterialsDidChangeNotification;

/*!
 *  Marketing materials is provided using this class. This method will automatically refresh at an interval
 *  that is configurable by the app when the SDK is initialized.
 *
 *  @since Available since 1.0.0
 *  @discussion <b>Deprecated since 9.0.0.</b> To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint `/mobile/v3/get_marketing_materials`.
 */
__attribute__((deprecated("Deprecated since 9.0.0. To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint /mobile/v3/get_marketing_materials.")))
@interface CMTMarketingMaterialsDataProvider : CMTBaseDataProvider<CMTSynchronousDataProviderProtocol>

/*!
 *  Provide the marketing message to retrieve the CMTMarketingMessageModel for given `key`.
 *  Returns nil if the key is not present. The locale string is determined by the locale setting of the phone
 *
 *  @param key Key to get object. CMT will work with the customer to determine appropriate keys.
 *
 *  @return CMTMarketingMessageModel with content for phone locale corresponding to key, nil if the key is not present
 *
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 9.0.0. This method is deprecated because the CMTMarketingMaterialsDataProvider class  is deprecated.
 */
-(nullable CMTMarketingMessageModel *) objectForKey:(NSString *_Nonnull)key
__attribute__((deprecated("Deprecated since 9.0.0. This method is deprecated because the CMTMarketingMaterialsDataProvider class is deprecated.")));
/*!
 *  Provide the marketing message to retrieve the CMTMarketingMessageModel for given `key`.
 *  Returns nil if the key is not present. This version of the method allows you to explicitly specify the locale
 *
 *  @param key          Key to get object. CMT will work with the customer to determine appropriate keys.
 *  @param localeString String representation of the language portion of the locale identifier, e.g. en_US.UTF-8 -> en
 *
 *  @return CMTMarketingMessageModel with content for phone locale corresponding to key, nil if the key is not present
 *
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 9.0.0. This method is deprecated because the CMTMarketingMaterialsDataProvider is deprecated.
 */
-(nullable CMTMarketingMessageModel *) objectForKey:(NSString *_Nonnull)key localeString:(NSString *_Nullable)localeString
__attribute__((deprecated("Deprecated since 9.0.0. This method is deprecated because the CMTMarketingMaterialsDataProvider class is deprecated.")));

/*!
 *  <i>Asychronously</i> fetches the most recent version of the marketing materials, stores the response to disk and deserializes the response
 *
 *  @param handler  If not nil, this handler will be called when the task is finished. The handler will be passed
 *  a reference to this object and whether or not the fetch succeeded. The error parameter will be set if an error occurs.
 *
 *  @return Handle to the NSOperation used to execute this background task.
 *  @since Available since 1.0.4
 *  @deprecated Deprecated since 9.0.0. To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint `/mobile/v3/get_marketing_materials`.
 */
-(nonnull NSOperation *)loadMarketingMaterialsInBackgroundWithCompletionHandler:(void (^_Nullable)(CMTMarketingMaterialsDataProvider * _Nonnull value, BOOL success, NSError * _Nullable err))handler
__attribute__((deprecated("Deprecated since 9.0.0. To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint `/mobile/v3/get_marketing_materials`.")));

/**
 *  <i>Synchronously</i> fetch the marketing materials, store the response to disk, and deserialize the response
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @discussion     Deserializing the response can be slow and isn't needed until the data is used. The `fetchMarketingMaterialsWithError:` method
 *                  omits the deserialization step and is preferred.
 *  @return         The most recently fetched dictionary of CMTMarketingMessageModel objects.
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 1.0.0
 *  @deprecated Deprecated since 9.0.0. To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint `/mobile/v3/get_marketing_materials`.
 */
-(NSDictionary <NSString *, CMTMarketingMessageModel *> *_Nullable)loadMarketingMaterialsWithError:(NSError * _Nullable __autoreleasing*_Nullable)error __attribute__((deprecated("Deprecated since 9.0.0. To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint `/mobile/v3/get_marketing_materials`.")));

/**
 *  <i>Synchronously</i> fetch the marketing materials and store the response to disk
 *
 *  @param error (Objective-C only) Set on failure.
 *
 *  @return YES if successful
 *
 *  @throws (Swift only) NSError
 *
 *  @since Available since 2.0.0
 *  @deprecated Deprecated since 9.0.0. To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint `/mobile/v3/get_marketing_materials`.
 */
-(BOOL)fetchMarketingMaterialsWithError:(NSError * _Nullable __autoreleasing*_Nullable)error __attribute__((deprecated("Deprecated since 9.0.0. To fetch marketing materials, use the CMTURLSessionManager to fetch data from the CMT endpoint `/mobile/v3/get_marketing_materials`.")));


@end
