//
// CMTTagUpdater.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@class CMTUser;

extern NSString * _Nonnull const CMTTagUpdaterErrorDomain;
extern NSInteger const CMTTagUpdaterRSSIThreshold;

/**
 @typedef    CMTTagUpdaterErrorCode
 @discussion Possible error conditions that may result while foreground updating the tag
 @since      Available since 1.9.0
 
 - CMTTagUpdaterErrorConnectionAttemptTimedOut: Code when connection attempt to peripheral times out
 (Available since 1.9.0)
 - CMTTagUpdaterErrorFailedToConnect: Code when connection attempt to peripheral fails (Available since 1.9.0)
 - CMTTagUpdaterErrorNoDriveWellServicesFound: Connection succeeded, but no DriveWell services found (Available since 1.9.0)
 - CMTTagUpdaterErrorFirmwareDataCorrupted: Firmware retrieved from server, but data is corrupt (Available since 1.9.0)
 - CMTTagUpdaterErrorAppNotInForeground: App was not in foreground when scanning was attempted (Available since 1.9.0)
 - CMTTagUpdaterErrorScanForDriveWellServicesTimedOut: Tried to scan for DriveWell services, but timed out (Available since 1.9.0)
 - CMTTagUpdaterErrorFirmwareDataHasZeroLength: Firmware returned from server is empty (Available since 2.0.0)
 - CMTTagUpdaterErrorCouldNotCompleteUpgrade: Firmware upgrade started, but could not complete (Available since 3.0.3)
 - CMTTagUpdaterErrorFirmwareWriteTimedOut: Copying firmware from phone to tag can timeout (Available since 3.0.3)
 - CMTTagUpdaterErrorUpdateNotificationForCharacteristicFailed: For some tags, notifications must be set up in order for successful upgrade (Available since 3.0.3)
 - CMTTagUpdaterErrorAlreadyUpgraded: The tag you are trying upgrade has been upgraded alread (Available since 3.0.3)
 */
typedef NS_ENUM(NSInteger, CMTTagUpdaterErrorCode) {
    CMTTagUpdaterErrorConnectionAttemptTimedOut = -1,
    CMTTagUpdaterErrorFailedToConnect = -2,
    CMTTagUpdaterErrorNoDriveWellServicesFound = -3,
    CMTTagUpdaterErrorFirmwareDataCorrupted = -4,
    CMTTagUpdaterErrorAppNotInForeground = -5,
    CMTTagUpdaterErrorScanForDriveWellServicesTimedOut = -6,
    CMTTagUpdaterErrorFirmwareDataHasZeroLength = -7,
    CMTTagUpdaterErrorCouldNotCompleteUpgrade = -8,
    CMTTagUpdaterErrorFirmwareWriteTimedOut = -9,
    CMTTagUpdaterErrorUpdateNotificationForCharacteristicFailed = -10,
    CMTTagUpdaterErrorAlreadyUpgraded = -11
};

@class CMTTagUpdater;

/**
 Protocol used to notify delegate of different phases of foreground firmware upgrade
 @since Available since 1.9.0
 */
@protocol CMTTagUpdaterDelegate <NSObject>

@optional

/** Method called when scanning for tags to upgrade begins
 *
 * @param tagUpdater Reference to the CMTTagUpdater that called the delegate method
 *
 * @since Available since 1.9.0
 */
-(void) tagUpdaterDidBeginScanningForTags:(CMTTagUpdater * _Nonnull)tagUpdater;

/** Method called when updater stops scanning for tags to upgrade
 *
 * @param tagUpdater Reference to the CMTTagUpdater that called the delegate method
 *
 * @since Available since 3.0.3
 */
-(void) tagUpdaterDidStopScanningForTags:(CMTTagUpdater * _Nonnull)tagUpdater;

/**
 * Method called when a tag that might need upgrade is discovered
 *
 * @param updater        a reference to the CMTTagUpdater object
 * @param tagMacAddress  mac address of tag that might need upgrade, could be 0 if can't extract it
 * @since Available since 2.0.0
 */
-(void) tagUpdater:(CMTTagUpdater * _Nonnull)updater didDiscoverTagNeedingUpgradeWithMacAddress:(uint64_t)tagMacAddress;

/**
 Method called when a bluetooth peripheral that might be upgradeable is discovered

 @param updater      a reference to the CMTTagUpdater object
 @param peripheral   a reference to a CBPeripheral that might be upgradeable
 */
-(void) tagUpdater:(CMTTagUpdater * _Nonnull)updater didDiscoverCandidatePeripheral:(CBPeripheral * _Nonnull)peripheral;

/*!
 * Method called when firmware upgrade of a specific tag begins
 *
 * @param updater        a reference to the CMTTagUpdater object
 * @param peripheral     reference to peripheral being upgraded
 * @param tagMacAddress  mac address of tag that is currently being updated, could be 0 if can't extract it
 *
 * @since Available since 1.9.0
 */
-(void) tagUpdater:(CMTTagUpdater * _Nonnull)updater didBeginUpdateForPeripheral:(CBPeripheral * _Nonnull)peripheral withMacAddress:(uint64_t)tagMacAddress;

/*!
 * Method will be called when the firmware update completes without error
 * The success of an upgrade cannot be confirmed until a trip is completed with
 * the updated tag by the user who upgraded it.
 *
 * @param updater        a reference to the CMTTagUpdater object
 * @param peripheral     reference to peripheral being upgraded
 * @param tagMacAddress  mac address of tag that is being reported on, could be 0 if can't extract it
 *
 * @since Available since 1.9.0
 */
-(void) tagUpdater:(CMTTagUpdater * _Nonnull)updater didFinishUpdateForPeripheral:(CBPeripheral * _Nonnull)peripheral withMacAddress:(uint64_t)tagMacAddress;

/*!
 * Method will be called if the firmware upgrade fails
 *
 * @param updater        a reference to the CMTTagUpdater object
 * @param peripheral     reference to peripheral being upgraded
 * @param tagMacAddress  mac address of tag that is being reported on, could be 0 if can't extract it
 * @param error          A description of the error
 * 
 * @since Available since 1.9.0
 */
-(void) tagUpdater:(CMTTagUpdater * _Nonnull)updater didFailToUpdateForPeripheral:(CBPeripheral * _Nonnull)peripheral withMacAddress:(uint64_t)tagMacAddress error:(NSError * _Nullable)error;

/*!
 * Method will be called periodically during tag firmware upgrade to indicate
 * the progress of the update
 *
 * @param updater        a reference to the CMTTagUpdater object
 * @param progress       value between 0 and 1 that indicates progress of upgrade
 * @param peripheral     reference to peripheral being upgraded
 * @param tagMacAddress  mac address of tag that is being reported on, could be 0 if can't extract it
 *
 * @since Available since 1.9.0
 */
-(void) tagUpdater:(CMTTagUpdater * _Nonnull)updater didUpdateProgress:(float)progress forPeripheral:(CBPeripheral * _Nonnull)peripheral withMacAddress:(uint64_t)tagMacAddress;

@end


/**
 @since Available since 1.9.0
 */
@interface CMTTagUpdater : NSObject

/**
 App developers should not attempt to create an instance of `CMTTagUpdater` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 * Create a CMTTagUpdater object

 * @param user        An authenticated user object
 * @param delegate    delegate object that implements the CMTTagUpdaterDelegate
 * @return An instance of CMTTagUpdater
 *
 * @since Available since 1.9.0
 */
-(nonnull instancetype)initWithUser:(CMTUser * _Nonnull)user delegate:(NSObject<CMTTagUpdaterDelegate> * _Nullable)delegate NS_DESIGNATED_INITIALIZER;

/*!
 * Attempt to scan for tags and update all CMT tags that are visible to this phone
 *
 * @discussion This method will attempt to upgrade any BLE hardware that
 * provides CMT services. The app must be in the foreground for this
 * method to work.  This method scans for and updates tag and will
 * make calls on the `delegate` property during the upgrade.
 *
 * The `CMTTagUpdater` object will scan forever until the object is deallocated OR
 * `stopScanningForTags` is called
 *
 * @param error     If we cannot enable the scanning, an error will be set
 *
 * @since Available since 1.9.0
 */
-(void)startScanningForTagsAndUpgradeFirmwareWithError:(NSError * _Nullable __autoreleasing*_Nullable)error;

/*!
 * End or abort firmware update
 *
 * @discussion Developers should call this to stop scanning
 *
 * @since Available since 1.9.0
 */
-(void)stopScanningForTags;

/** Tracks scanning state 
 *  @since Available since 1.9.0
 */
@property (readonly, getter=isScanning) BOOL scanningForTagsToUpgrade;

/** True if a tag is currently being upgraded
 *  @since Available since 1.9.0
 */
@property (readonly) BOOL currentlyUpgrading;

/** The peripheral identifier UUID string of the currently upgrading tag, only valid if `currentlyUpgrading` is TRUE
 *  @since Available since 1.9.0
 */
@property (readonly, nullable) NSString *peripheralUUIDStringOfUpgradingTag;

/** Upgrade progress of currently upgrading tag, only valid if `currentlyUpgrading` is TRUE
 *  @since Available since 1.9.0
 */
@property (readonly) float upgradeProgress;

/**
 * Scanned peripherals with RSSI < rssiThreshold will not be upgraded. The default value for this
 * is CMTTagUpdaterRSSIThreshold (-80 dB).
 * @since Available since 3.0.2
 */
@property (atomic) NSInteger rssiThreshold;
/**
 *  Set to object that implements the `CMTTagUpdaterDelegate` protocol
 *  @since Available since 1.9.0
 */
@property (weak, nullable) NSObject<CMTTagUpdaterDelegate> *delegate;

@end
