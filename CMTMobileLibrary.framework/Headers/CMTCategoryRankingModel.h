//
// CMTCategoryRankingModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTUserRankModel.h"

/*!
 *  Ranking model used by to display each entry of the custom leaderboard
 *  @since Available since 0.2.28
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTCategoryRankingModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Category handle
 *  @since Available since 0.2.28
 */
@property (nullable) NSString * categoryHandle;

/*! Category label
 *  @since Available since 0.2.28
 */
@property (nullable) NSString * categoryLabel;

/*! Category description
 *  @since Available since 0.2.28
 */
@property (nullable) NSString * categoryDescription;

/*! Category title to display as a table header
 *  @since Available since 0.2.28
 */
@property (nullable) NSString * categoryTableTitle;

/*! Show state column in this table if TRUE
 *  @since Available since 0.2.28
 */
@property BOOL displayStateColumn;

/*! Show invite friends if TRUE
 *  @since Available since 0.2.28
 */
@property BOOL displayInviteFriends;

/*! Rank information pertaining to the authenticated user
 *  @since Available since 0.2.28
 */
@property (nullable) CMTUserRankModel * userRank;

/*! List of the top users as CMTUserRankModel objects
 *  @since Available since 0.2.28
 */
@property (nullable) NSArray <CMTUserRankModel *> * topUsers;

/*! Total number of the users in this category
 *  @since Available since 1.5.0
 *  @deprecated Deprecated since 1.7.0, use numberOfTotalUsers instead.
 */
@property NSInteger numberOfUsers __attribute__((deprecated("Deprecated since 1.7.0, use numberOfTotalUsers instead")));

/*! Total number of the users in this category
 *  @since Available since 1.7.0
 */
@property NSInteger numberOfTotalUsers;

/*! Display the number of total users if TRUE
 *  @since Available since 1.7.0
 */
@property BOOL displayNumberOfTotalUsers;

/*! Hide user detailed info if TRUE
 *  @since Available since 1.8.1
 */
@property BOOL hideUserDetailedInfo;

@end
