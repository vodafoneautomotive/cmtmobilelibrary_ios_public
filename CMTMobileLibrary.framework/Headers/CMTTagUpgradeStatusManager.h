//
// CMTTagUpgradeStatusManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 Type to indicate upgrade status of tags
 
 @since Available since 1.9.0
 */
typedef NS_ENUM(NSInteger, CMTTagUpgradeState) {
    /// tag has not beeen upgraded (Available since 1.9.0)
    CMTTagUpgradeStateNotUpgraded = 0,
    /// tag has been upgraded (Available since 1.9.0)
    CMTTagUpgradeStateUpgraded = 1
};


/**
 Use interface to store and retrieve upgrade status for tags
 @since Available since 1.9.0
 */
@interface CMTTagUpgradeStatusManager : NSObject


/**
 @return shared instance that should be used by app developers
 
 @since Available since 1.9.0
 */
+(nonnull CMTTagUpgradeStatusManager *)sharedInstance;

/**
 Get the upgrade state for a given tag, if a tag is not in the table, 
 the tag will be assumed to be not upgraded

 @param tagMacAddress tag mac address expressed as a uint64_t
 @return the upgrade status for the tag
 
 @since Available since 1.9.0
 */
-(CMTTagUpgradeState)upgradeStateForTagWithMacAddress:(uint64_t)tagMacAddress;

/**
 Get the upgrade state for a given tag, if a tag is not in the table,
 the tag will be assumed to be not upgraded
 
 @param tagMacAddress tag mac address expressed as an NSString
 @return the upgrade status for the tag
 
 @since Available since 1.9.0
 */
-(CMTTagUpgradeState)upgradeStateForTagWithMacAddressAsString:(NSString *_Nonnull)tagMacAddress;

/**
 Set the upgrade status for a given tag

 @param state The upgrade state
 @param tagMacAddress tag mac address expressed as a uint64_t
 
 @since Available since 1.9.0
 */
-(void)setUpgradeState:(CMTTagUpgradeState)state forTagWithMacAddress:(uint64_t)tagMacAddress;

/**
 Set the upgrade status for a given tag
 
 @param state The upgrade state
 @param tagMacAddress tag mac address expressed as an NSstring
 
 @since Available since 1.9.0
 */
-(void)setUpgradeState:(CMTTagUpgradeState)state forTagWithMacAddressAsString:(NSString *_Nonnull)tagMacAddress;

/**
 Remove all entries in the tag upgrade state table
 
 @discussion Only call then when a user logs out or is deauthorized
 
 @since Available since 1.9.0
 */
-(void) clear;

@end
