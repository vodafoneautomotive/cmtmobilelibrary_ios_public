//
// CMTTagVehicleLocationInfoModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTMantle.h"
#import <CoreLocation/CoreLocation.h>

/**
 Data model class to location information of tag / vehicle
 @since Available since 1.7.0
 */
@interface CMTTagVehicleLocationInfoModel : CMTMTLModel<CMTMTLJSONSerializing>

/**
 Whether or not tag is currently connected
 @since Available since 1.7.0
 */
@property BOOL connected;

/**
 Location of a tag / vehicle
 @since Available since 1.7.0
 */
@property (nullable) CLLocation *location;

/**
 Tag MAC address as unsignedLongLong
 @since Available since 1.7.0
 */
@property (nullable) NSNumber *tagId;

@end
