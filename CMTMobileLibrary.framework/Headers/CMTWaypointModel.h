//
// CMTWaypointModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTDataTypes.h"
#import <CoreLocation/CoreLocation.h>

/*!
 *  Used to de-serialize the events in the mobile results JSON file
 *  @since Available since 1.0.0
 */
@interface CMTWaypointModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Location of waypoint
 *  @since Available since 1.0.0
 */
@property (nullable) CLLocation *location;

/*! UTC Time at which the waypoint occurred
 *  @since Available since 1.0.0
 */
@property (nullable) NSDate *waypointTime;

/*! Average moving speed during waypoint in kilometers per hour. If the user was not moving at any point
 *  between this waypoint and previous waypoint, that section of time is not included in the average.
 *  @since Available since 1.0.0 */
@property (nullable) NSNumber *avgMovingSpeedKmh;

/*! Maximum speed in kilometers per hour between previous and current waypoint (floating point)
 *  @since Available since 1.0.0 */
@property (nullable) NSNumber *maxSpeedKmh;

/*! Speed limit in kilometers per hour for this segment. Nil if not present. (floating point)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *speedLimitKmh;

/*! The primary type for the segment for which this waypoint is on.
 *  Use this property to determine if this segment was estimated, or should be marked as speeding
 *  @since Available since 1.0.5
 */
@property CMTPrimarySegmentType primarySegmentType;

/*! The secondary type for which this waypoint is on
 *  Use this property to determine if phone motion is present
 *  @since Available since 1.0.5
 */
@property CMTSecondarySegmentType secondarySegmentType;

@end
