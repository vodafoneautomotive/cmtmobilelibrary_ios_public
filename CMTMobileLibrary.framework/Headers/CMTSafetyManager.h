//
// CMTSafetyManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Types of users initiating Panic Alerts
 @since Available since 8.0.0
 */
typedef NS_ENUM(NSInteger, CMTSafetyManagerUserType) {
    //User is a Rider
    CMTSafetyManagerUserTypeRider = 1,
    //User is a Driver
    CMTSafetyManagerUserTypeDriver = 2,
};

/**
 Sources initiating Panic Alerts
 @since Available since 8.0.0
 */
typedef NS_ENUM(NSInteger, CMTSafetyManagerAlertSource) {
    //Source is the the application
    CMTSafetyManagerAlertSourceApp = 2,
    //Source is Siri
    CMTSafetyManagerAlertSourceSiri = 3,
};

/**
 Transport methods allowed for retrieving data
 @since Available since 8.0.0
 */
typedef NS_ENUM(NSInteger, CMTSafetyManagerTransportType) {
    //Use WiFi
    CMTSafetyManagerTransportTypeWifi = 0,
    //Use Bluetooth
    CMTSafetyManagerTransportTypeBluetooth = 1,
};

/**
 Camera from which to retrieve image
 @since Available since 8.0.0
 */
typedef NS_ENUM(NSInteger, CMTSafetyManagerCamera) {
    //Cabin of vehicle
    CMTSafetyManagerCabinCamera = 0,
};

extern NSString * _Nonnull const CMTSafetyManagerErrorDomain;

/**
 Error codes for CMTSafetyManager operations.
 @since Available since 8.0.0
 */
typedef NS_ENUM(NSInteger, CMTSafetyManagerErrorCode) {
    //This method in not supported in this OS version
    CMTSafetyManagerErrorUnsupportedMethod = -1,
    //Failed to retrieve image
    CMTSafetyManagerErrorImageNotFound = -2,
    //Failed to retrieve video
    CMTSafetyManagerErrorVideoNotFound = -3,
    //Device manager is not available
    CMTSafetyManagerErrorDeviceManagerNotAvailable = -4,
    //Sending of command to device via BLE failed
    CMTSafetyManagerErrorDeviceCommunicationError = -5,
};

@class CMTUser;

/**
 CMTSafetyManager manages tasks related to retrieving audio and video data from the device, as well as managing panic alerts.
 @since Available since 8.0.0
 */
@interface CMTSafetyManager : NSObject

/**
 Will be TRUE if a panic alert is in progress.
 @since Available since 8.0.0
 */
@property (readonly) BOOL panicActive;

/**
 Time that current panic alert will expire. Will be nil if no panic is active.
 @since Available since 8.0.0
 */
@property (readonly, nullable) NSDate *timeOut;

/**
 Default SSID for WiFi connection.
 @since Available since 8.0.0
 */
@property (readonly, nonnull) NSString *defaultSSID;

/**
 App developers should not attempt to create an instance of `CMTSafetyManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Initialize
 @param user current user
 @since Available since 8.0.0
 */
-(nullable instancetype)initWithUser:(CMTUser *_Nonnull)user;

/**
 Retrieves a camera image of the selected type, over the selected transport.
 @param camera camera from which to retrieve image
 @param transport transport type to be used
 @param handler will be called with the image data (if successful), success flag, and error (if unsuccessful)
 @discussion handler will be called on background thread
 @since Available since 8.0.0
 */
-(void)getImageFromCamera:(CMTSafetyManagerCamera)camera
            overTransport:(CMTSafetyManagerTransportType)transport
    withCompletionHandler:(void (^_Nullable)(NSData * _Nullable imageData, BOOL success, NSError * _Nullable error))handler;

/**
 Configures a WiFi interface to allow connection to the device.
 @param ssid SSID of device (if null, default value will be used)
 @param passphrase security passphrase for SSID (if null, default value will be used)
 @param joinOnce When set to true, the hotspot remains configured and connected only as long as the app that configured it is running in the foreground.
 @param lifeTimeInDays The number of days the network retains the associated configuration.
 @param handler will be called with an error, if any
 @see Apple documentation for NEHotspotConfiguration
 @discussion handler will be called on background thread
 @since Available since 8.0.0
 */
-(void)configureWiFiWithSSID:(NSString * _Nullable)ssid
                  passphrase:(NSString * _Nullable)passphrase
                    joinOnce:(BOOL) joinOnce
              lifeTimeInDays:(NSInteger) lifeTimeInDays
                     handler:(void (^ _Nullable)(NSError * _Nullable error))handler;

/**
 Removes configuration and disconnects from device
 @param ssid SSID of device (if null, default value will be used)
 @see Apple documentation for NEHotspotConfiguration
 @since Available since 8.0.0
 */
-(void)removeWifiConfigurationWithSSID:(NSString * _Nullable)ssid;

/**
 Attempts to enable WiFi Access Point for currently connected (over BLE) device

 @param enable YES to enable, NO to disable
 @param handler will be called with an error, if any
 @discussion handler will be called on background thread
*/
-(void)enableWiFiAccessPoint:(BOOL)enable withHandler:(void (^ _Nullable)(NSError * _Nullable error))handler;

/**
 Start a panic alert
 @param userType the type user initiating the panic
 @param source the source of the panic alert
 @since Available since 8.0.0
 */
-(void)startPanicForUserType:(CMTSafetyManagerUserType)userType fromSource:(CMTSafetyManagerAlertSource)source;

@end

