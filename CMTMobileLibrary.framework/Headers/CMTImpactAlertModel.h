//
// CMTImpactAlertModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 * Class to provide information about an impact alert
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
__attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts."))) @interface CMTImpactAlertModel : CMTMTLModel

/*! Mac address of the tag that detects an impact
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property (nullable) NSString *tagMacAddress __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Number of trips of the tag that detects an impact
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property NSInteger tagTripsCount __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Number of connections the tag that detects an impact
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property NSInteger tagConnectionCount __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Number of impacts
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property NSInteger tagImpactId __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Number of seconds ago that the impact occurred
 *  @since Available since 1.7.0
 *  @deprecated in 9.0.0
 */
@property (nonatomic, readonly) NSInteger tagTimeDelta __attribute__((deprecated("Deprecated in 9.0.0, renamed to impactTimeDelta.")));

/*! Number of seconds ago that the impact occurred
 *  @since Available since 9.0.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property NSInteger impactTimeDelta __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Phone time since 1970 in seconds
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
*/
@property NSInteger phoneTime __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! FilterEngine time in seconds
 *  @since Available since 3.0.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
*/
@property NSInteger filterEngineTime __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Boolean indicating this impact alert should be suppressed or not
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property BOOL shouldSuppress __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Planar magnitude
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property float planarMagnitude __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Log offset of impact from last reported trip start - can be negative
 *  @since Available since 1.9.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property NSInteger logOffset __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Impact duration in ms
 *  @since Available since 1.9.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property NSInteger impactDurationMs __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Last observed location at time of impact
 *  @since Available since 1.7.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
 */
@property (nullable) NSArray<NSDictionary<NSString*,NSNumber*>*> *locations __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

/*! Identifier for drive recording (if any) while impact is being processed
 *  @since Available since 9.0.0
 *  @deprecated Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.
*/
@property (nullable) NSString *driveId __attribute__((deprecated("Deprecated since 9.3.0 since CMTImpactAlertNotificationDelegate has been deprecated. Use the CMTImpactObjectReceivedNotification NSNotification (found in CMTImpactUploader) instead for alerting the app to impacts.")));

@end
