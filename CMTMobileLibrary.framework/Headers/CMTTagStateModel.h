//
// CMTTagStateModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/**
 Data model indicating link state of a tag
 
 @since Available since 1.2.13
 */
@interface CMTTagStateModel : CMTMTLModel<CMTMTLJSONSerializing>

/**
 Whether or not the tag is linked to a user. Boolean.
 
 @since Available since 1.2.13
 */
@property (nonatomic,readonly,nullable,getter=isLinked) NSNumber *linked;

/**
 Tag mac address as unsignedLongLong.
 
 @since Available since 1.2.13
 */
@property (nonatomic,nullable) NSNumber *tagId;

@end
