//
// CMTBaseResultsModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTWaypointModel.h"

/*!
 *  Captures the information for unprocessed trips (which can include trajectory information) and includes a list of waypoints (CMTWaypointModel objects),
 *  which can be used to visually display a trip. Note that the waypoints property can be nil.
 *  @since Available since 0.3.0
 */
@interface CMTBaseResultsModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! UTC offset in seconds (as an integer)
 *  @since Available since 0.3.0
 */
@property (nullable) NSNumber *utcOffset;

/*! Array of CMTWaypointModel objects @see CMTWaypointModel.h
 *  @since Available since 0.3.0
 */
@property (nullable)  NSArray<CMTWaypointModel *> *waypoints;

@end
