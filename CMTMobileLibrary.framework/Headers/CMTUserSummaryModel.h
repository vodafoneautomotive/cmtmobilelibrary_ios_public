//
// CMTUserSummaryModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTDataTypes.h"
#import "CMTBadgeModel.h"
#import "CMTTipModel.h"
#import "CMTUserSummaryTripMetricsModel.h"
#import "CMTTeamSummaryModel.h"

/*!
 *  Dashboard note
 *  @since Available since 1.0.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend..
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.")))
@interface CMTUserSummaryDashboardNoteModel : CMTMTLModel <CMTMTLJSONSerializing>

/*! Id of the specific note contents
 *  @since Available since 1.0.0
 */
@property NSInteger noteId;

/*! HTML string which contains the data to display
 *  @since Available since 1.0.0
 */
@property (nullable) NSString* noteContents;

@end

/*!
 *  User summary
 *  @since Available since 1.0.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to fetch user summary information from the CMT backend.")))
@interface CMTUserSummaryModel : CMTMTLModel <CMTMTLJSONSerializing>

/*! Overall user driving score
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *userScore;

/*! The maximum score achievable
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *maxScore;

/*! An array of CMTTipModel objects
 *  @since Available since 1.0.0
 */
@property (copy,nullable) NSArray <CMTTipModel *> *tips;

/*! The most recently awarded badge
 *  @since Available since 1.0.0
 */
@property (nullable) CMTBadgeModel *latestBadge;

/*! Reference to additional statistics about the user
 *  @since Available since 1.0.0
 */
@property (nullable) CMTUserSummaryTripMetricsModel *tripMetrics;

/*! Reference to a note that should be displayed on the dashboard
 *  @since Available since 1.0.0
 */
@property (nullable) CMTUserSummaryDashboardNoteModel *dashboardNote;

/*! Whether app is in competition mode or not (Boolean)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *competitionMode;

/*! Number of tickets awarded to user (integer)
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *tickets;

/*! Quote text to display 
 *  @since Available since 1.0.0
 */
@property (nullable) NSString *getQuoteText;

/*! Score breakdown description. The contents are HTML.
 *  @since Available since 1.0.0
 */
@property (nullable) NSString *scoreCalculationDescriptionHTML;

/*! Qualification information. The contents are HTML.
 *  @since Available since 1.0.0
 */
@property (nullable) NSString *qualificationInformationHTML;

/*! Dashboard main webview content. The content is HTML.
 *  @since Available since 1.7.0
 */
@property (nullable) NSString *dashboardMainWebViewContentHTML;

/* Team related properties
 *  @since Available since 1.7.0
 */
@property (nullable) CMTTeamSummaryModel *teamSummary;

@end
