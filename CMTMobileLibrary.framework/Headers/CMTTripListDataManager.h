//
// CMTTripListDataManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CMTProcessedDriveInfo.h"
#import "CMTBaseDriveInfo.h"
#import "CMTMobileResultsModel.h"
#import "CMTTripLabelModel.h"
#import "CMTUnprocessedTripStatusDidChangeDelegate.h"

/*!
 @since Available since 1.0.0
 */
extern NSString * _Nonnull const CMTTripListDataManagerErrorDomain;

/**
 Error types that might be returned by this CMTTripListDataManager
 
 @since Available 1.0.0
 */
typedef NS_ENUM(NSInteger, CMTTripListDataManagerErrorType) {
    /// Unkonwn error has occurred
    CMTTripListDataManagerErrorUnknown,
    /// Returned when referring to a drive id that doesn't exist (Available since 1.0.0)
    CMTTripListDataManagerErrorDriveDoesNotExist,
    /// Returned when referring to a null drive id (Available since 1.0.0)
    CMTTripListDataManagerErrorDriveIdIsNull,
    /// Returned if mobile results for a specific trip cannot be retrieved (Available since 1.0.0)
    CMTTripListDataManagerErrorMobileResultsFailed,
    /// Returned when reverse geocoding failes (Available since 1.0.0)
    CMTTripListDataManagerErrorReverseGeocodingIncomplete,
    /// Returned when attempt to reverse geocode a hidden drive is made (Available since 1.0.5)
    CMTTripListDataManagerErrorDriveNotReverseGeocodedSinceHidden,
    /// JSON conversion error (Available since 1.7.0)
    CMTTripListDataManagerErrorJSONError
};


/*!
 Provides a list of current trips for a user, and can also be used to get detailed trajectory information and event information
 for a given drive. A `CMTTripListDataManager` instance is associated with any authenticated user.

 @since Available since 1.0.0
 */
@interface CMTTripListDataManager : NSObject

/** @name Properties */
/*!
 List of trips with most recent trips returned first. If the trip is unprocessed, the type of the trip will be `CMTBaseDriveInfo`.
 If the trip is processed, the type of the trip will be `CMTProcessedDriveInfo`.

 @discussion This will return a deep copy of the current list of trips.
 @since Available since 1.0.0
 */
@property (readonly,nonnull) NSArray <CMTBaseDriveInfo *> *tripList;

/*!
 List of trips with most recent trips returned first, the type of each trip will be
 an NSDictionary.
 @since Available since 1.0.0
 */
@property (readonly,nullable) NSArray <NSDictionary *> *rawTripList NS_UNAVAILABLE;

/*!
 Set this to an object to receive status changes for trips that have not finished processing
 @since Available since 1.0.0
 */
@property (weak,atomic,nullable) NSObject<CMTUnprocessedTripStatusDidChangeDelegate> *tripStatusDidChangeDelegate;

/**
 App developers should not attempt to create an instance of `CMTTripListDataManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/*!
 Fetch new trips from server and return all trips that have and have not been processed.
 Hidden trips will not be included. Trips that have a start time that precedes the effective date will
 not be included. Note that the server will limit the number of trips returned to the app.

 @param fetchTripError   Set if there is an error fetching trips, otherwise nil.
 @param fetchLabelError  Set if there is an error fetching labels, otherwise nil.
 @param updatedTripList  Set to a deep copy the list of trips that have been updated at the server. Could be empty.

 @return A deep copy list of the processed and unprocessed trips in reverse chronological order.

 @since Available since 1.0.0
 */
-(nonnull NSArray <CMTBaseDriveInfo *> *)loadTripsWithTripsFetchingError:(NSError * _Nullable __autoreleasing *_Nullable)fetchTripError
                                                      labelFetchingError:(NSError * _Nullable __autoreleasing *_Nullable)fetchLabelError
                                                         updatedTripList:(NSArray <CMTBaseDriveInfo *> * _Nonnull __autoreleasing *_Nullable)updatedTripList;


/*!
  Asynchronously fetch new trips from server and return all trips that have and have not been processed.
  Hidden trips will not be included. Trips that have a start time that precedes the effective date will
  not be included.

  Note that the server will limit the number of trips returned to the app.

  @param handler Block to be executed when trip fetching is complete.
                 `successFetchingTrips` is TRUE if trips are successfully fetched from the server, there may be no change
                 `successFetchingLabels` is TRUE if labels are successfully fetched from the server, there may be no change
                 `tripList` will return a deep copy list of the trips in reverse chronological order.
                            Hidden trips and trips with start time before the effective date will NOT be included.
                 `updatedTripList` will return a deep copy list of the trips that are newly fetched from the server, have been updated
                                   by the server, or have a new label since the last time this method was called OR since
                                   `loadTripsWithTripsFetchingError:labelFetchingError:updatedTripList:` was called.
                 `tripFetchingError` parameter will be set if there is an error fetching trips, otherwise it will be nil.
                `labelFetchingError` parameter will be set if there an an error fetching labels, otherwise it will be nil.
  @return The instance of the NSOperation reference that can be used to cancel the operation.
  @since Available since 1.0.4
  @deprecated Deprecated since 9.0.0, use loadTripsWithHandler:/loadTrips(handler:) instead.
 */
-(nonnull NSOperation *)loadTripsInBackgroundWithCompletionHandler:(void (^_Nullable)(BOOL successFetchingTrips, BOOL successFetchingLabels,
                                                                    NSArray <CMTBaseDriveInfo*> * _Nonnull tripList,
                                                                    NSArray <CMTBaseDriveInfo*> * _Nonnull updatedTripList,
                                                                    NSError * _Nullable tripFetchingError, NSError * _Nullable labelFetchingError))handler
__attribute__((deprecated("Deprecated since 9.0.0, use loadTripsWithHandler:/loadTrips(handler:) instead ")));

/*!
 Asynchronously fetch new trips from server and return all trips that have and have not been processed.
 Hidden trips will not be included. Trips that have a start time that precedes the effective date will
 not be included.
 
 Note that the server will limit the number of trips returned to the app.
 
 @param handler Block to be executed when trip fetching is complete.
                `successFetchingTrips` is TRUE if trips are successfully fetched from the server, there may be no change
                `successFetchingLabels` is TRUE if labels are successfully fetched from the server, there may be no change
                `tripList` will return a deep copy list of the trips in reverse chronological order.
                           Hidden trips and trips with start time before the effective date will NOT be included.
                `updatedTripList` will return a deep copy list of the trips that are newly fetched from the server, have been updated
                                  by the server, or have a new label since the last time this method was called OR since
                `loadTripsWithTripsFetchingError:labelFetchingError:updatedTripList:` was called.
                `tripFetchingError` parameter will be set if there is an error fetching trips, otherwise it will be nil.
                `labelFetchingError` parameter will be set if there an an error fetching labels, otherwise it will be nil.
 @since Available since 9.0.0
 */
-(void)loadTripsWithHandler:(void (^_Nullable)(BOOL successFetchingTrips, BOOL successFetchingLabels,
                                               NSArray <CMTBaseDriveInfo*> * _Nonnull tripList,
                                               NSArray <CMTBaseDriveInfo*> * _Nonnull updatedTripList,
                                               NSError * _Nullable tripFetchingError, NSError * _Nullable labelFetchingError))handler;

/*!
 @param driveId The drive id of a trip for which you want the start and end location names of the trip.
 @param handler Block to be executed when reverse geocoding is complete.
                `success` is TRUE if there are new trips and FALSE otherwise.
                `startLocationName` is the name of the start location of the trip
                `endLocationName` is the name of the end location of the trip
                `info` returns the full drive info for the drive.
                `error` parameter will be set if there is an error, otherwise it will be nil.
 @return The instance of the NSOperation reference that can be used to cancel the operation.
 @since Available since 1.0.4
 @deprecated Deprecated since 9.0.0, use reverseGeocodeTripForDriveId:handler:/reverseGeocodeTrip(forDriveId:handler:) instead.
 */
-(nonnull NSOperation *)reverseGeocodeTripInBackgroundForDriveId:(NSString *_Nonnull)driveId
                                               completionHandler:(void (^_Nullable)(BOOL success,
                                                                                    NSString * _Nullable startLocationName,
                                                                                    NSString * _Nullable endLocationName,
                                                                                    CMTBaseDriveInfo * _Nullable info,
                                                                                    NSError * _Nullable err))handler
__attribute__((deprecated("Deprecated since 9.0.0, use reverseGeocodeTripForDriveId:handler:/reverseGeocodeTrip(forDriveId:handler:) instead ")));

/*!
 @param driveId The drive id of a trip for which you want the start and end location names of the trip.
 @param handler Block to be executed when reverse geocoding is complete.
                `success` is TRUE if there are new trips and FALSE otherwise.
                `startLocationName` is the name of the start location of the trip
                `endLocationName` is the name of the end location of the trip
                `info` returns the full drive info for the drive.
                `error` parameter will be set if there is an error, otherwise it will be nil.
 @since Available since 9.0.0
 */
-(void)reverseGeocodeTripForDriveId:(NSString *_Nonnull)driveId
                            handler:(void (^_Nullable)(BOOL success,
                                                       NSString * _Nullable startLocationName,
                                                       NSString * _Nullable endLocationName,
                                                       CMTBaseDriveInfo * _Nullable info,
                                                       NSError * _Nullable err))handler;

/*!
   @param driveId The drive id of a trip for which you want the start and end location names of the trip.
   @param handler Block to be executed when reverse geocoding is complete.
                  `success` is TRUE if there are new trips and FALSE otherwise.
                  `startLocationName` is the name of the start location of the trip
                  `startLocationPlacemark` is the CLPlacemark of the start location of the trip
                  `endLocationName` is the name of the end location of the trip
                  `endLocationPlacemark` is the CLPlacemark of the end location of the trip
                  `info` returns the full drive info for the drive.
                  `error` parameter will be set if there is an error, otherwise it will be nil.
   @return The instance of the NSOperation reference that can be used to cancel the operation.
   @since Available since 6.0.0
   @deprecated Deprecated since 9.0.0, use reverseGeocodeTripWithPlacemarkForDriveId:handler:/reverseGeocodeTripWithPlacemark(forDriveId:handler:) instead.
 */
-(nonnull NSOperation *)reverseGeocodeTripWithPlacemarkInBackgroundForDriveId:(NSString *_Nonnull)driveId
                                                            completionHandler:(void (^_Nullable)(BOOL success,
                                                                                                 NSString * _Nullable startLocationName,
                                                                                                 CLPlacemark * _Nullable startLocationPlacemark,
                                                                                                 NSString * _Nullable endLocationName,
                                                                                                 CLPlacemark * _Nullable endLocationPlacemark,
                                                                                                 CMTBaseDriveInfo * _Nullable driveInfo, NSError * _Nullable error))handler
 __attribute__((deprecated("Deprecated since 9.0.0, use reverseGeocodeTripWithPlacemarkForDriveId:handler:/reverseGeocodeTripWithPlacemark(forDriveId:handler:) instead ")));

/*!
 @param driveId The drive id of a trip for which you want the start and end location names of the trip.
 @param handler Block to be executed when reverse geocoding is complete.
                `success` is TRUE if there are new trips and FALSE otherwise.
                `startLocationName` is the name of the start location of the trip
                `startLocationPlacemark` is the CLPlacemark of the start location of the trip
                `endLocationName` is the name of the end location of the trip
                `endLocationPlacemark` is the CLPlacemark of the end location of the trip
                `info` returns the full drive info for the drive.
                `error` parameter will be set if there is an error, otherwise it will be nil.
 @since Available since 9.0.0
 */
-(void)reverseGeocodeTripWithPlacemarkForDriveId:(NSString *_Nonnull)driveId
                                         handler:(void (^_Nullable)(BOOL success,
                                                                    NSString * _Nullable startLocationName,
                                                                    CLPlacemark * _Nullable startLocationPlacemark,
                                                                    NSString * _Nullable endLocationName,
                                                                    CLPlacemark * _Nullable endLocationPlacemark,
                                                                    CMTBaseDriveInfo * _Nullable driveInfo, NSError * _Nullable error))handler;


/*!
   Asynchronously fetch trip details waypoint and event information for a drive

   @param driveId The id of the drive you want detailed results for
   @param handler Block to be executed when loading the details completes.
                  `success` is TRUE if the fetch of the results is successful, FALSE otherwise
                  `result` returns returns a model of the trip results
                  `error` is set to an error code if success is FALSE
   If the trip has not been processed yet, `result` will be of type CMTBaseResultsModel. In this case,
   the trip details will not include any information about events. On the other hand, if the trip has been
   processed, `result` will be of type `CMTMobileResultsModel`, which includes an `events` property.

   @since Available since 1.1.0
 */
-(nonnull NSOperation *)loadTripDetailsInBackgroundForDriveId:(NSString *_Nonnull)driveId
                                            completionHandler:(void (^_Nullable)(BOOL success, CMTBaseResultsModel * _Nullable result, NSError * _Nullable error)) handler;


/*!
   Label a trip using the details provided in the model.

   @param label This object contains the driveId for which the label applies. If the driveId is missing, an error will occur.
                If the label.transportationMode is set to CMTTransportModeTypeNotLabeled, any previously set label
                will be deleted.
   @param error (Objective-C only) Set on failure.

   @return Updated model

   @throws (Swift only) NSError

   @since Available since 1.0.0
 */
-(nullable CMTTripLabelModel *) setTripLabel:(CMTTripLabelModel *_Nonnull)label error:(NSError * _Nullable __autoreleasing *_Nullable)error;

/*!
   Asynchronous version of `setTripLabel:error`

   @param label   This object contains the driveId for which the label applies
   @param handler Block to be executed when settings of the label is complete
   @return An NSOperation object that can be used to cancel the operation.
   @since Available since 1.0.4
   @deprecated Deprecated since 9.0.0, use setTripLabel:handler:/setTripLabel(_:handler:) instead.
 */
-(nonnull NSOperation *)setTripLabelInBackgroundWithLabel:(CMTTripLabelModel *_Nonnull)label
                                        completionHandler:(void (^_Nullable)(BOOL success,
                                                           CMTTripLabelModel *_Nullable,
                                                           NSError * _Nullable error)) handler
 __attribute__((deprecated("Deprecated since 9.0.0, use setTripLabel:handler:/setTripLabel(_:handler:) instead ")));

/*!
 Asynchronous version of `setTripLabel:error`
 
 @param label   This object contains the driveId for which the label applies
 @param handler Block to be executed when settings of the label is complete
 @since Available since 9.0.0
 */
-(void)setTripLabel:(CMTTripLabelModel *_Nonnull)label
            handler:(void (^_Nullable)(BOOL success,
                                       CMTTripLabelModel *_Nullable,
                                       NSError * _Nullable error)) handler;

/*!
   Label several trips at once

   @param tripLabelArray An array of CMTTripLabelModel objects, each object specifies a driveId
   @param error (Objective-C only) Set on failure.

   @return The updated array of CMTTripLabelModel objects

   @throws (Swift only) NSError

   @since Available since 1.0.0
 */
-(nullable NSArray <CMTTripLabelModel *> *) setTripLabelsWithArray:(NSArray <CMTTripLabelModel *> *_Nonnull)tripLabelArray error:(NSError *_Nullable __autoreleasing *_Nullable)error;

/*!
   Asynchronous version of `setTripLabelsWithArray:error`

   @param userTripLabelsList An array of CMTTripLabelModel objects, each object specifies a driveId
   @param handler            Block to be executed when settings of the label is complete
   @return An NSOperation object that can be used to cancel the operation.
   @since Available since 1.0.4
   @deprecated Deprecated since 9.0.0, use setTripLabels:handler:/setTripLabels(_:handler:) instead.
 */
-(nonnull NSOperation *)setTripLabelsInBackgroundWithLabels:(NSArray <CMTTripLabelModel *> *_Nonnull)userTripLabelsList
                                          completionHandler:(void (^_Nullable)(BOOL success,
                                                             NSArray <CMTTripLabelModel *> * _Nullable userTripLabelsList,
                                                             NSError * _Nullable error)) handler
  __attribute__((deprecated("Deprecated since 9.0.0, use setTripLabels:handler:/setTripLabels(_:handler:) instead ")));

/*!
 Asynchronous version of `setTripLabelsWithArray:error`
 
 @param userTripLabelsList An array of CMTTripLabelModel objects, each object specifies a driveId
 @param handler            Block to be executed when settings of the label is complete
 
 @since Available since 9.0.0
 */
-(void)setTripLabels:(NSArray <CMTTripLabelModel *> *_Nonnull)userTripLabelsList
             handler:(void (^_Nullable)(BOOL success,
                                        NSArray <CMTTripLabelModel *> * _Nullable userTripLabelsList,
                                        NSError * _Nullable error)) handler;

/*!
   Cancel any pending network operations

   @since Available since 1.0.0
 */
-(void) cancelAllOperations;

@end
