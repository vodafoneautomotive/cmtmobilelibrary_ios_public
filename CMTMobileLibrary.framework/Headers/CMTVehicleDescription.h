//
// CMTVehicleDescription.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/**
 Data model representing a user created vehicle object. Use this data model
 to create vehicle to add into the CMT backend
 */
@interface CMTVehicleDescription : CMTMTLModel<CMTMTLJSONSerializing>

/**
 Vehicle registration number, e.g. license plate number
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *registration;

/**
 Vehicle identification number
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *vin;

/**
 Car manufacturer
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *make;

/**
 Car model
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *model;

/**
 Nickname for vehicle
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *nickname;

/**
 Type of vehicle, the strings "car" and "motorcycle" are recognized, anything else is an error
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *vehicleType;

/**
 Power ratio (float)
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSNumber *powerRatio;

/**
 Policy number
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *policyNumber;

/**
 Group id 
 @since Available since 5.0.3
 */
@property (nonatomic, nullable, readonly) NSString *groupId;

/**
 Vehicle id
 @since Available since 8.0.0
 */
@property (nonatomic, nullable, readonly) NSString *vehicleId;

/**
 A description of the CMTVehicleDescription object
 @since Available since 8.0.0
 */
@property (readonly, nullable, copy) NSString *description;

/**
 App developers should not attempt to create an instance of `CMTVehicleDescription` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Create a CMTVehicleDescription object

 @discussion Any string parameter of length 0, will be ignored
    By default, powerRatio, policyNumber, and groupId fields cannot be set
    without backend support
 
 @param registration licence plate number / registration of vehicle
 @param vin vehicle identification number
 @param make vehicle manufacturer
 @param model vehicle model
 @param nickname nickname
 @param powerRatio power ratio (by default, this field cannot be set)
 @param vehicleType vehicleType, either "car" or "motorcycle"
 @param policyNumber policy number of vehicle (by default, this field cannot be set)
 @param groupId group id of vehicle (by default, this field cannot be set)
 @param vehicleId vehicle id of vehicle
 @return An instance of a CMTVehicleDescription object
 
 @since Available since 5.0.3
 */
-(nonnull instancetype) initWithRegistration:(NSString * _Nullable)registration
                                         vin:(NSString * _Nullable)vin
                                        make:(NSString * _Nullable)make
                                       model:(NSString * _Nullable)model
                                    nickname:(NSString * _Nullable)nickname
                                  powerRatio:(NSNumber * _Nullable)powerRatio
                                 vehicleType:(NSString * _Nullable)vehicleType
                                policyNumber:(NSString * _Nullable)policyNumber
                                     groupId:(NSString * _Nullable)groupId
                                   vehicleId:(NSString * _Nullable)vehicleId NS_DESIGNATED_INITIALIZER;

/**
 Create a CMTVehicleDescription object

 @discussion Any string parameter of length 0, will be ignored
 By default, powerRatio, policyNumber, and groupId fields cannot be set
 without backend support

 @param registration licence plate number / registration of vehicle
 @param vin vehicle identification number
 @param make vehicle manufacturer
 @param model vehicle model
 @param nickname nickname
 @param powerRatio power ratio (by default, this field cannot be set)
 @param vehicleType vehicleType, either "car" or "motorcycle"
 @param policyNumber policy number of vehicle (by default, this field cannot be set)
 @param groupId group id of vehicle (by default, this field cannot be set)
 @return An instance of a CMTVehicleDescription object

 @since Available since 5.0.3
 */
-(nonnull instancetype) initWithRegistration:(NSString * _Nullable)registration
                                         vin:(NSString * _Nullable)vin
                                        make:(NSString * _Nullable)make
                                       model:(NSString * _Nullable)model
                                    nickname:(NSString * _Nullable)nickname
                                  powerRatio:(NSNumber * _Nullable)powerRatio
                                 vehicleType:(NSString * _Nullable)vehicleType
                                policyNumber:(NSString * _Nullable)policyNumber
                                     groupId:(NSString * _Nullable)groupId;

/**
 An alternative initializer that takes a dictionary as a parameter

 @discussion Any string parameter of length 0, will be ignored
   By default, powerRatio, policyNumber, and groupId fields cannot be set
   without backend support

 @param vehicleDictionary the dictionary should have the following format
            vehicleDictionary[@"registration"] -> String
            vehicleDictionary[@"vin"] -> String
            vehicleDictionary[@"make"] -> String
            vehicleDictionary[@"model"] -> String
            vehicleDictionary[@"nickname"] -> String
            vehicleDictionary[@"power_ratio"] -> String
            vehicleDictionary[@"vehicle_type"] -> String
            vehicleDictionary[@"policy_number"] -> String
            vehicleDictionary[@"group_id"] -> String
            vehicleDictionary[@"vehicle_id"] -> String
 @return An instance of a CMTVehicleDescription object
 
 @since Available since 5.0.3
 */
-(nonnull instancetype) initWithVehicleDictionary:(NSDictionary * _Nonnull)vehicleDictionary;

@end
