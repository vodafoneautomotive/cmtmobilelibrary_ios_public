//
// CMTLeaderboardModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"
#import "CMTFriendModel.h"

/*!
 * Each category ranking contains three elements: the `label`, which describes what the ranking is and can
 * be used in the UI, the `value` or rank, and `valueText`, a description of the rank.
 *
 * @since Available since 1.0.0
 * @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTLeaderboardCategoryRankingModel : CMTMTLModel <CMTMTLJSONSerializing>

/*! Name of the ranking, e.g. USA
 *
 * @since Available since 1.0.0
 */
@property (nullable) NSString *label;

/*! Percentile of user's score, integer between 0 and 100
 *
 * @since Available since 1.0.0
 */
@property (nullable) NSNumber *value;

/*! Text describing user's ranking 
 *
 * @since Available since 1.0.0
 */
@property (nullable) NSString *valueText;

@end

/*!
 *  The leaderboard is split into arrays. The rankings in various categories can be found in the `categoryRankings` array.
 *  The list of friends is stored in the `friends` array and is ordered by a scoring metric, typically, user score.
 *
 * @since Available since 1.0.0
 * @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTLeaderboardModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! An ordered array of CMTLeaderboardCategoryRankingModels that should be displayed in order received
 *
 * @since Available since 1.0.0
 */
@property (nullable) NSArray <CMTLeaderboardCategoryRankingModel *> *categoryRankings;

/*! An array of CMTFriendModel objects, ordered by their scores 
 *
 * @since Available since 1.0.0
 */
@property (nullable) NSArray <CMTFriendModel *> *friends;

@end
