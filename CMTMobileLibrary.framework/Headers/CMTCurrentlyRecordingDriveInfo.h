//
// CMTCurrentlyRecordingDriveInfo.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTBaseDriveInfo.h"

/**
 *  Contains information about a currently recording trip.
 *  @since Available since 1.0.0
 */
@interface CMTCurrentlyRecordingDriveInfo : CMTBaseDriveInfo

/*! Boolean value indicating whether or not the drive has a valid GPS point.
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *hasValidGps;

/*! Boolean value indicating whether the drive is a legitimate drive.
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *isValidDrive;

/*! Integer indicating the number of total GPS points.
 *  @since Available since 1.0.0
 */
@property (nullable) NSNumber *numberGpsPoints;

@end
