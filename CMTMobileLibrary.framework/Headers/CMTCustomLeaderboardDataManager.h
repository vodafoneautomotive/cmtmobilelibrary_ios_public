//
// CMTCustomLeaderboardDataManager.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTUserBaseDataProvider.h"
#import "CMTCategoryRankingModel.h"
#import <Foundation/Foundation.h>

/*!
 * Name of errors that come from the `CMTLeaderboardDataManager`
 * @since Available since 0.2.28
 */
extern NSString * _Nonnull const CMTCustomLeaderboardDataManagerErrorDomain;

/**
 These constants indicate the type of error that resulted in an operation's failure.

 @discussion Deprecated since 9.0.0.
 @since Available since 0.2.28
 */
typedef NS_ENUM(NSInteger, CMTCustomLeaderboardDataManagerError) {
    ///  Indicates when the fetched data from the backend is unable to be serialized ( Available since 0.2.28)
    CMTCustomLeaderboardDataManagerErrorSerializationFailed = -1
};

/*!
 * The `CMTCustomLeaderboardDataManager` class includes a property called `leaderboardCategories`,
 * which contains all the information about the leaderboards that the app can display. These leaderboard categories
 * must be from the CMT backend.
 *
 * The types of leaderboards that can be displayed are dependent on which leaderboards you have enabled with CMT.
 *
 * The `leaderboardCategories` class method returns an array of objects of type `CMTCategoryRankingModel`.
 * Every CMTCategoryRankingModel contains the data to describe a specific leaderboard category. The userRank and
 * topUsers properties for `CMTUserRankModel` are key to creating category-specific leaderboards.
 *  @since Available since 0.2.28
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTCustomLeaderboardDataManager : CMTUserBaseDataProvider

/*! Returns array of CMTCategoryRankingModel objects
 * @since Available since 0.2.28
 */
@property (readonly,atomic,nullable) NSArray <CMTCategoryRankingModel *> *leaderboardCategories;

/**
 App developers should not attempt to create an instance of `CMTCustomLeaderboardDataManager` with default method.
 */
-(nonnull instancetype)init NS_UNAVAILABLE;
+(nonnull instancetype)new NS_UNAVAILABLE;

/**
 Loads custom leaderboard synchronously
 
 @method loadLeaderboardWithError:
 
 @since Available since 4.0.0
 @param error (Objective-C only) Set on failure.
 @return Array of CMTCategoryRankingModel objects (could be nil)
 @throws (Swift only) NSError
 */
-(nullable NSArray <CMTCategoryRankingModel *> *)loadLeaderboardWithError:(NSError * _Nullable __autoreleasing*_Nullable)error;

/**
 Forces an asynchronous fetch of the custom leaderboard
 
 @since Available since 4.0.0
 @param handler If not nil, at the completion of the asynchronous network operation, the handler will be called.
 The handler should return an `NSArray` object corresponding to the list of leaderboardCategories
 and whether the network call succeeded or failed. If the network call failed, an appropriate error will be indicated to
 the caller.
 */
-(void)loadLeaderboardInBackgroundWithCompletionHandler:(void (^_Nullable)(NSArray <CMTCategoryRankingModel *> * _Nullable leaderboardCategories, BOOL success, NSError * _Nullable error))handler;

/**
 Loads custom leaderboard synchronously with optional Facebook token
 
 @since Available since 1.3.17
 @param facebookToken If the `facebookToken` was just updated, you can pass it as a parameter to the server. If you provide the `facebookToken` as a parameter, then the                     server response will include the list of facebook friends that have authorized the app. If you are not using Facebook in your app, you can pass nil                    as the `facebookToken` parameter.
 @param error (Objective-C only) Set on failure.
 @return Array of CMTCategoryRankingModel objects (could be nil)
 @throws (Swift only) NSError
 */
-(nullable NSArray <CMTCategoryRankingModel *> *)loadLeaderboardWithFacebookToken:(NSString *_Nullable)facebookToken
                                                                            error:(NSError * _Nullable __autoreleasing*_Nullable)error;

/**
 Forces an asynchronous fetch of the custom leaderboard with optional Facebook token
 
 @since Available since 1.3.17
 @param facebookToken If the `facebookToken` was just updated, you can pass it as a parameter to the server. If you provide the `facebookToken` as a parameter, then the                     server response will include the list of facebook friends that have authorized the app. If you are not using Facebook in your app, you can pass nil                    as the `facebookToken` parameter.
 @param handler If not nil, at the completion of the asynchronous network operation, the handler will be called.
 The handler should return an `NSArray` object corresponding to the list of leaderboardCategories
 and whether the network call succeeded or failed. If the network call failed, an appropriate error
 will be indicated to the caller.
 @return The `NSOperation` assigned to the operation performing the fetch
 */
-(nonnull NSOperation *)loadLeaderboardInBackgroundWithFacebookToken:(NSString *_Nullable)facebookToken
                                                   completionHandler:(void (^_Nullable)(NSArray <CMTCategoryRankingModel *> * _Nullable leaderboardCategories, BOOL success, NSError * _Nullable error))handler;

@end
