//
// CMTFriendResponseModel.h
// CMTMobileLibrary
//
// Created by Cambridge Mobile Telematics, Inc. 
// Copyright © 2012-2021 Cambridge Mobile Telematics. All rights reserved.
//

#import "CMTMantle.h"

/*!
 *  Used to accept or ignore an incoming friend request
 *  @since Available since 0.1.0
 *  @discussion Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.
 */
__attribute__((deprecated("Deprecated since 9.0.0; for this use case, use the CMTURLSessionManager to communicate with the CMT backend.")))
@interface CMTFriendResponseModel : CMTMTLModel<CMTMTLJSONSerializing>

/*! Integer invitation identifier of the friend request you're responding to
 *  @since Available since 0.1.0
 */
@property (nullable, nonatomic) NSNumber *inviteId;

/*! an invite secret that can be used to find the friend request in the backend
 *  @since Available since 0.1.0
 */
@property (nullable, nonatomic) NSString *inviteSecret;

@end
